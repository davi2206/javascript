const LIVE = false;
const CANVAS_COLOR = '#333';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Multiple Pendulum"
const FPS = 255;

const COLORS = ['rgba(255, 0, 0, 0.01)', 'rgba(0, 255, 0, 0.03)', 'rgba(0, 0, 255, 0.06)'];
//const COLORS = ['rgba(255, 0, 0, 1)', 'rgba(0, 255, 0, 1)', 'rgba(0, 0, 255, 1)'];
const OBJECT_WIDTH = 3;
let colorIndex = 0;

let scripts = ["Inputs.js", "UI.js", "Pendulum.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let pendulumCount = 3;
let pendulums = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	registerEvents();
	registerUIEvents();
	runSim();
}

function runSim() {
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	pendulums.push(new Pendulum(new Point(CANVAS_WIDTH/2, CANVAS_HEIGHT/2), new Vector(CANVAS_HEIGHT/4, 0), 0.5, null, COLORS[colorIndex], OBJECT_WIDTH));

	let tempPendulum = pendulums[0];
	
	while(pendulums.length < pendulumCount) {
		colorIndex++;
		tempPendulum = new Pendulum(tempPendulum.getEndPoint(), new Vector(tempPendulum.getLength()/2, 0), tempPendulum.speed*2.1, tempPendulum, COLORS[colorIndex], tempPendulum.width*0.9);
		pendulums.push(tempPendulum);
	}

	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	pendulums.forEach(pendulum => {
		if(pendulum.connectedPendulum) {
			pendulum.setAnchorPoint(pendulum.connectedPendulum.getEndPoint());
		}
		pendulum.turn();
	});
}

function drawAll() {
	// Canvas
	//colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	
	pendulums.forEach(pendulum => {
		pendulum.draw();
	});
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}