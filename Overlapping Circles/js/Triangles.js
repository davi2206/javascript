

class Triangles {
    constructor() { }
    
    getPoint(circle) {
        let ang = random(0,2*Math.PI);

        let r = random() + random();
        if(r >= 1) r = 2-r;

        r *= circle.r

        let x = Math.cos(ang)*r;
        let y = Math.sin(ang)*r;

        return new Point(x + circle.x, y + circle.y);
    }
}