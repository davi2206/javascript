const LIVE = false;
const CANVAS_COLOR = 'black';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 900;
const TITLE = "Project Title"
const FPS = 1000;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "RandInSquare.js", "RandRadiusDegree.js", "InverseTransformSample.js", "Triangles.js", "Circle.js"];
let commonScripts = ["draw.js", "Point.js"];
var scriptsToLoad = 0;

// Sketch specific parameters
let r = (CANVAS_HEIGHT * 0.24);
let circles = [];

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, CANVAS_COLOR, true);
	// Canvas
	colorRect(0, 0, canvas.width, canvas.height, CANVAS_COLOR);
	let ang = 0;
	let x = 0;
	let y = 0;
	let nr = 3;
	let rFactor = r * 0.75;


	// Circles
	for (let i = 0; i <= nr; i++) {
		let red = random(100, 255).toString();
		let green = random(100, 255).toString();
		let blue = random(100, 255).toString();

		let color = `rgba(${red},${green},${blue},0.1)`;

		ang = ((2 * Math.PI) / nr * i) + (Math.PI / 2);
		x = Math.cos(ang) * rFactor + CANVAS_WIDTH / 2;
		y = Math.sin(ang) * rFactor + CANVAS_HEIGHT / 2;
		circles.push(new Circle(x, y, r, null, color));
	}

	circles.forEach(c => {
		c.draw();
	});

	loadImages();
	registerEvents();
}

function runSim() { }

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}
