const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Project Title"
const FPS = 1440;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let colorMin = 0;
let colorMax = 255;

let object_width = (CANVAS_WIDTH/(colorMax*6));
let object_height = CANVAS_HEIGHT;
let xPos = 1;
let yPos = 0;

let r = 255;
let g = b = colorMin;
let swtch = 1;

let running = true;

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);
	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function () {
		if (running) {
			moveAll();
			drawAll();
		}
	}, 1000 / FPS);
}

function moveAll() {
	if(r >= colorMax && g < colorMax && b <= colorMin) g++;
	if(r > colorMin && g >= colorMax && b <= colorMin) r--;
	if(r <= colorMin && g >= colorMax && b < colorMax) b++;
	if(r <= colorMin && g > colorMin && b >= colorMax) g --;
	if(r < colorMax && g <= colorMin && b >= colorMax) r++;
	if(r >= colorMax && g <= colorMin && b > colorMin) b--;

	xPos += object_width * swtch;
}

function drawAll() {
	// Canvas
	//colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	if (xPos >= CANVAS_WIDTH || xPos <= 0) {
		swtch *= -1;
		yPos = 0; //+= (object_height);
		running = true;
		object_height *= 0.9;
		console.log(object_height);
	}
	if(object_height < 5) running = false;

	// color = generateColor(r, g, b);
	color = `rgba(${r},${g},${b},1)`;
	colorRect(xPos, yPos, object_width, object_height, color);
}

function generateColor(r, g, b) {
	rs = (r).toString(16);
	gs = (g).toString(16);
	bs = (b).toString(16);

	if (rs.length == 1) rs = "0" + rs;
	if (gs.length == 1) gs = "0" + gs;
	if (bs.length == 1) bs = "0" + bs;
	let a = 1;

	return `#${r}${g}${b}${a}`;
}

function HEXToVBColor(rrggbb) {
    var bbggrr = rrggbb.substr(4, 2) + rrggbb.substr(2, 2) + rrggbb.substr(0, 2);
    return parseInt(bbggrr, 16);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}
