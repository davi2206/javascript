const LIVE = false;
const CANVAS_COLOR = 'gray';
const TITLE = "Fractal Tree"
const FPS = 4;

// A Ball that we can put on the canvas
const BRANCH_COLOR = 'black';

let CANVAS_WIDTH = 1800;
let CANVAS_HEIGHT = 800;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let target_depth = 0;
let ang = 5;
let ang_noise = 0;
let width = 5;
let width_noise = 0;
let length = 5;
let length_reduce = 20;
let length_noise = 0;

let red_pr_branch;

let total_depth = 0;

window.onload = function() {
	CANVAS_WIDTH = screen.width * 0.70;
	CANVAS_HEIGHT = screen.height * 0.70;
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	// registerUIEvents();
	resetValues();

	runSim();
}

function resetValues() {
	target_depth = 0;
	ang = 5;
	ang_noise = 0;
	width = 5;
	width_noise = 0;
	length = 5;
	length_reduce = 20;
	length_noise = 0;
}

function runSim() {
	setInterval(function() {
		if(target_depth < 20) {
			moveAll();
			drawAll();
		}
	}, 1000/FPS);
}

function moveAll() {}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	drawBranches();
}

function drawBranches() {
	getInputValues();

	let start = new Point(CANVAS_WIDTH/2, CANVAS_HEIGHT);
	let branchVector = new Vector(0, -length);
	branch(start, branchVector, 0);
}

function branch(start, vector, depth) {
	let my_depth = depth + 1;

	if(my_depth > target_depth) return true;
	if(my_depth > total_depth) total_depth = my_depth;

	let color = getRandColor(["#3D2B1F", "#4B3621", "#6F4E37", "#654321", "#483C32"]);
	
	let newStart = new Point(start.x + vector.x, start.y + vector.y);

	let leftVector = new Vector(vector.x,vector.y);
	leftVector.turn_deg(noise(-ang, ang_noise));

	let multiplier = noise(length_reduce, length_noise)/100;
	leftVector.mult(multiplier);

	let rightVector = new Vector(vector.x,vector.y);
	rightVector.turn_deg(noise(ang, ang_noise));
	
	multiplier = noise(length_reduce, length_noise)/100;
	rightVector.mult(multiplier);

	branch(newStart, leftVector, my_depth);
	let last = branch(newStart, rightVector, my_depth);

	if(last) {
		color = "green";
	}

	let width_current = noise(width/my_depth, width_noise);
	drawVector(start, vector, color, width_current);
}

function noise(value, noise) {
	let pct = value / 100 * noise;
	let rand_noice = random(0, pct);
	return (value + rand_noice);
}

function getInputValues() {
	// Manipulated Values
	target_depth += 1;
	ang += 1;
	width += 1;
	length += 7;
	length_reduce += 3;

	// Noise fields
	ang_noise = parseInt(input_ang_noise.value); 
	width_noise = parseInt(input_width_noise.value);
	length_noise = parseInt(input_length_noise.value);
}



function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}