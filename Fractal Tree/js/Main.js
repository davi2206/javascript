const LIVE = false;
const CANVAS_COLOR = 'gray';
const TITLE = "Fractal Tree"
const FPS = 30;

// A Ball that we can put on the canvas
const BRANCH_COLOR = 'black';

let CANVAS_WIDTH = 1800;
let CANVAS_HEIGHT = 800;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let target_depth;
let ang;
let ang_noise;
let width;
let width_noise;
let length;
let length_reduce;
let length_noise;

let red_pr_branch;

let total_depth;

window.onload = function() {
	CANVAS_WIDTH = screen.width * 0.70;
	CANVAS_HEIGHT = screen.height * 0.70;
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;

	// Set values in input fields
	input_depth.value = 15;
	input_ang.value = 25;
	input_ang_noise.value = 0;
	input_width.value = 20;
	input_width_noise.value = 0;
	input_length.value = 150;
	input_length_reduce.value = 80;
	input_length_noise.value = 0;
	total_depth = 0;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	// registerUIEvents();

	drawBranches();
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	// Nothing to move
}

function drawAll() {
	// Canvas
	// colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
}

function drawBranches() {
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	
	getInputValues();

	let start = new Point(CANVAS_WIDTH/2, CANVAS_HEIGHT);
	let branchVector = new Vector(0, -length);
	branch(start, branchVector, 0);

	console.log(total_depth);
}

function branch(start, vector, depth) {
	let my_depth = depth + 1;

	if(my_depth > target_depth) return true;
	if(my_depth > total_depth) total_depth = my_depth;

	let color = getRandColor(["#3D2B1F", "#4B3621", "#6F4E37", "#654321", "#483C32"]);
	
	let newStart = new Point(start.x + vector.x, start.y + vector.y);

	let leftVector = new Vector(vector.x,vector.y);
	leftVector.turn_deg(noise(-ang, ang_noise));

	let multiplier = noise(length_reduce, length_noise)/100;
	leftVector.mult(multiplier);

	let rightVector = new Vector(vector.x,vector.y);
	rightVector.turn_deg(noise(ang, ang_noise));
	
	multiplier = noise(length_reduce, length_noise)/100;
	rightVector.mult(multiplier);

	branch(newStart, leftVector, my_depth);
	let last = branch(newStart, rightVector, my_depth);

	if(last) {
		color = "green";
	}

	let width_current = noise(width/my_depth, width_noise);
	drawVector(start, vector, color, width_current);
}

function noise(value, noise) {
	let pct = value / 100 * noise;
	let rand_noice = random(0, pct);
	return (value + rand_noice);
}

function getInputValues() {
	target_depth = Math.min(parseInt(input_depth.value), 22);
	ang = parseInt(input_ang.value);
	ang_noise = parseInt(input_ang_noise.value);
	width = parseInt(input_width.value);
	width_noise = parseInt(input_width_noise.value); 
	length = parseInt(input_length.value);
	length_reduce = parseInt(input_length_reduce.value);
	length_noise = parseInt(input_length_noise.value);
}



function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}