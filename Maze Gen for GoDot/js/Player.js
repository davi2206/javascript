function movePlayer(playerDir) {
	let playerDirs = getPlayerDirs(player);
	if (!playerDirs.includes(playerDir)) return;
	let playerIndex = player.y * maze_width + player.x;
	let newPlayerIndex = getNewIndex(playerDir, playerIndex);
	let isRelated = cells[playerIndex].isRelated(cells[newPlayerIndex]);

	if (isRelated) {
		player = cells[newPlayerIndex];
	}

	if(player == cells[cells.length-1]) {
		win();
	}
}

function getPlayerDirs(entity) {
	let dirs = [];

	if (entity.y > 0) {
		dirs.push("N");
	}
	if (entity.x > 0) {
		dirs.push("W");
	}
	if (entity.x < maze_width - 1) {
		dirs.push("E");
	}
	if (entity.y < maze_height - 1) {
		dirs.push("S");
	}
	return dirs;
}