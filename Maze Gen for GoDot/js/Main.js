const LIVE = false;
const CANVAS_COLOR = 'GREEN';
const CANVAS_WIDTH = 800; // HEIGHT is forced to half of WIDTH
const TITLE = "A Mazing Provocation";
const FPS = 15;

const MAX_LEVEL = 5;

let intervalDraw;
let intervalMove;
let level = 1;
let skill = 1;
let initial_maze_width = 100;
let maze_width = 0;
let maze_height = 0;
let gold_saturation = 2;
let collectible_count = 1;

const CELL_COLOR = 'BLACK';
const PATH_COLOR = 'BLACK';
const SCRAMBLE_ITERATIONS = 0;

let scripts = ["Inputs.js", "UI.js", "Cell.js", "Player.js", "MazeManagement.js", "Gold.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let cell_size = 0;
let path_size = 0;
let cells = [];
let gold = [];
let collectibles = [];
let root = null;
let oldRoot = null;
let player = null;
let dir = "";

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_WIDTH, color = CANVAS_COLOR, showError = true);

	registerEvents();
	registerUIEvents();
	clear();
	titleH1.innerHTML = `${TITLE}! Level: ${level} at Skill: ${skill}`;
	
	maze_width = (initial_maze_width);
	maze_height = maze_width;
	cell_size = Math.max(Math.min((CANVAS_WIDTH / maze_width)/2*0.75, ((CANVAS_WIDTH/2) / maze_height)/2*0.75), 1);
	path_size = Math.max(cell_size*2, 1);
	
	generateCells();
	
	runSim();
}

function win() {
	level++;
	if(level > MAX_LEVEL) {
		level = 1;
		skill++;
	}
	play();
}

function clear() {
	clearInterval(intervalDraw);
	cells = [];
	gold = [];
	collectibles = [];
	root = null;
	player = null;
}

function runSim() {
	scrambleMaze(1000);
	generateLoops(100);
	drawAll();
}

function moveAll() {
	let rootIndex = root.y * maze_width + root.x;
	let dirs = root.getDirs();

	dir = dirs[random_floor(0, dirs.length)];
	oldRoot = cells[rootIndex];

	rootIndex = getNewIndex(dir, rootIndex);

	try {
		root = cells[rootIndex];
		root.parent = null;
		oldRoot.parent = root;
	}
	catch (e) {
		oldRoot.draw(cell_size, "RED", true);
		console.log("Old root: ", oldRoot);
		console.log("Dir: ", dir);
		console.log("Root index: ", rootIndex);
		throw e;
	}
}

function getNewIndex(dir, index) {
	switch (dir) {
		case "N":
			index -= maze_width;
			break;
		case "S":
			index += maze_width;
			break;
		case "W":
			index -= 1;
			break;
		case "E":
			index += 1;
			break;
	}

	return index;
}

function drawAll() {
	// Canvas
	colorRect(0, 0, canvas.width, canvas.height, CANVAS_COLOR);
	cells.forEach(c => {
		c.draw(cell_size, CELL_COLOR, true, path_size);
	});

	gold.forEach(g => g.draw());
	collectibles.forEach(c => c.draw());

	// Start and End
	let finish = cells[cells.length - 1];
	cells[0].drawExtra(cell_size / 1.5, "BLUE", true);
	colorX(finish.canvasPos.x, finish.canvasPos.y, cell_size/1.5, "RED");
	root.drawExtra(cell_size/1.2, "RED", true, path_size);
	player.drawExtra(cell_size / 2, "PINK", true, path_size, true);
}

function generateTileMap() {
	let str = "{\"level\":{";
	let val = 0;
	let tempIndex = 0;

	cells.forEach(cell => {
		val = 0;

		if(cell.x > 0) {
			// Check cell left
			tempIndex = cell.index-1;
			if(cell.isRelated(cells[tempIndex])) val += 8;
		}
		if(cell.x < maze_width-1) {
			// Check cell right
			tempIndex = cell.index+1;
			if(cell.isRelated(cells[tempIndex])) val += 2;
		}
		if(cell.y > 0) {
			// Check cell top
			tempIndex = cell.index-maze_width;
			if(cell.isRelated(cells[tempIndex])) val += 1;
		}
		if(cell.y < maze_width-1) {
			// Check cell bottom
			tempIndex = cell.index+maze_width;
			if(cell.isRelated(cells[tempIndex])) val += 4;
		}

		str += `Vector2(${cell.x},${cell.y}): ${val},`;
	});

	str += `},"start":Vector2(1,1),"end":Vector2(${root.x},${root.y}),},`;
	console.log(str);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}