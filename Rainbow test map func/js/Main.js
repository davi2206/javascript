const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 900;
const TITLE = "Project Title"
const FPS = 250;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js", "Rainbow.js"];
var scriptsToLoad = 0;

let xPos = 0;
let slices = 6;
let frame = 1;

let colorArray;
let sliceWidth = 0;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	colorArray = getRainbowArray();
	sliceWidth = CANVAS_WIDTH/slices;

	setInterval(function() {
		if(frame > slices) return;
		moveAll();
	}, 1000/FPS);
}

function moveAll() {
	colorIndex = map(frame, 1, slices, 1, (colorArray.length));
	colorIndex = Math.round(colorIndex)-1;
	color = colorArray[colorIndex];
	console.log("Frame: " + frame);
	console.log("Index: " + colorIndex);
	console.log("Colors: " + colorArray.length);
	console.log("Color: " + color);

	colorRect(xPos,0,sliceWidth,canvas.height, color);
	
	xPos += sliceWidth;
	frame++;
}