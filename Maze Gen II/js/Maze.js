
class Maze {

    constructor(width, height, rows, cols, posVector) {
        this.width = width;
        this.height = height;
        this.rows = rows;
        this.cols = cols;

        this.pos = posVector;

        this.cells = [];
        this.generateCells();
    }

    generateCells() {
        for (let y = 0; y < this.cols; y++) {
            for (let x = 0; x < this.rows; x++) {
                if (x == 0 && y > 0) {
                    this.cells.push(new Cell(x, y, this.cells[(y - 1) * this.cols], null, this));
                }
                else if (x > 0) {
                    let index = y * this.cols + x;
                    this.cells.push(new Cell(x, y, this.cells[index - 1], null, this));
                }
                else {
                    this.cells.push(new Cell(x, y, null, null, this));
                }
            }
        }
    
        root = this.cells[0];
        player = this.cells[0];
    }

    draw() {
        this.cells.forEach(cell => cell.draw());
    }
}