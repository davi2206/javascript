function generateCells() {
    for (let y = 0; y < maze_height; y++) {
        for (let x = 0; x < maze_width; x++) {
            if (x == 0 && y > 0) {
                cells.push(new Cell(x, y, cells[(y - 1) * maze_width]));
            }
            else if (x > 0) {
                let index = y * maze_width + x;
                cells.push(new Cell(x, y, cells[index - 1]));
            }
            else {
                cells.push(new Cell(x, y));
            }
        }
    }

    root = cells[0];
    player = cells[0];
}

function scrambleMaze(SCRAMBLE_ITERATIONS) {
    for (let i = 0; i < cells.length * SCRAMBLE_ITERATIONS; i++) {
        moveAll();
    }
}

function generateLoops(min_length) {
    let bridges = 0;
    let potentials = [];

    cells.forEach(loopCell => {    
        if(bridges > level) return;
        let dirs = getPlayerDirs(loopCell);

        dirs.forEach(dir => {
            switch (dir) {
                case "N":
                    index = loopCell.index - maze_width;
                    break;
                case "S":
                    index = loopCell.index + maze_width;
                    break;
                case "W":
                    index = loopCell.index - 1;
                    break;
                case "E":
                    index = loopCell.index + 1;
                    break;
            }
            if(loopCell.isValidSibling(cells[index], min_length)) {
                loopCell.validSiblings.push(cells[index]);
            }
        });
        if(loopCell.validSiblings.length > 0) potentials.push(loopCell);
    });

    while(bridges <= level) {
        let cell = potentials[random_floor(0, potentials.length-1)];
        if(!cell) return;
        let sibling = cell.validSiblings[random_floor(0, cell.validSiblings.length-1)];
        cell.sibling = sibling;
        sibling.sibling = cell;
        
        removeFromSet(cell, potentials);
        removeFromSet(sibling, potentials);
        bridges++;
    }
}
