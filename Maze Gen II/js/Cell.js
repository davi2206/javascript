/*
	Cell Class
*/
class Cell {
	constructor(x, y, parent, sibling, maze) {
		this.maze = maze;
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.sibling = sibling;
		this.canvasPos = this.getCanvasPos();
		this.index = this.y * maze.cols + this.x;
		this.treeSize = 0;
		this.validSiblings = [];
	}

	isRelated(cell) {
		if (this.parent && this.parent == cell) return true;
		if (cell.parent && cell.parent == this) return true;
		if (this.sibling && this.sibling == cell) return true;
		if (cell.sibling && cell.sibling == this) return true;
	
		return false;
	}

	draw(radius = 5, color = "black", fill = true, lineWidth = 5) {
		if(this.parent) drawLineTo([this.canvasPos.x, this.canvasPos.y], [this.parent.canvasPos.x, this.parent.canvasPos.y], color, path_size);
		if(this.sibling) drawLineTo([this.canvasPos.x, this.canvasPos.y], [this.sibling.canvasPos.x, this.sibling.canvasPos.y], color, path_size);
		
		colorCircle(this.canvasPos.x, this.canvasPos.y, radius, color, fill, lineWidth);
	}

	drawExtra(radius = 3, color = "pink", fill = true, lineWidth = 5) {
		colorCircle(this.canvasPos.x, this.canvasPos.y, radius, color, fill, lineWidth);
	}

	getCanvasPos() {
		let cell_width = CANVAS_WIDTH / this.maze.cols;
		let cell_x = (cell_width * this.x) + (cell_width / 2);
	
		let cell_height = (CANVAS_WIDTH/2) / this.maze.rows;
		let cell_y = (cell_height * this.y) + (cell_height / 2);

		let p = new Point(cell_x, cell_y);
		p.move_vector(this.maze.pos)
	
		return p;
	}

	getDirs() {
		let dirs = [];
	
		if (this.y > 0 && dir != "S") {
			dirs.push("N");
		}
		if (this.x > 0 && dir != "E") {
			dirs.push("W");
		}
		if (this.x < this.maze.cols - 1 && dir != "W") {
			dirs.push("E");
		}
		if (this.y < this.maze.rows - 1 && dir != "N") {
			dirs.push("S");
		}
		return dirs;
	}

	isValidSibling(cell, min_dist) {
		if(this.isRelated(cell)) return false;
		if(this.getDistance(cell) < min_dist) return false;

		return true;
	}

	getDistance(cell) {
		let dist = 0;
		let loopCellTree = this.getTree();
	
		let tmp = cell.parent;
	
		while (tmp) {
			dist++;
			if (loopCellTree.includes(tmp)) return dist;
			tmp = tmp.parent;
		}
	
		return dist;
	}

	getTree() {
		let tree = [];
		let index = this.index;
	
		while (cells[index].parent) {
			tree.push(cells[index]);
			index = cells[index].parent.index;
		}
	
		return tree;
	}
}