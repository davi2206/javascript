const LIVE = false;
const CANVAS_COLOR = 'GREEN';
const CANVAS_WIDTH = 1600; // HEIGHT is forced to half of WIDTH
const TITLE = "A Mazing Provocation";
const FPS = 15;

const MAX_LEVEL = 5;

let intervalDraw;
let intervalMove;
let level = 1;
let skill = 1;
let initial_maze_width = 10;
let maze_width = 0;
let maze_height = 0;

const CELL_COLOR = 'BLACK';
const PATH_COLOR = 'BLACK';
const SCRAMBLE_ITERATIONS = 0;

let scripts = ["Inputs.js", "UI.js", "Cell.js", "Player.js", "MazeManagement.js", "Maze.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let cell_size = 0;
let path_size = 0;
let cells = [];
let root = null;
let oldRoot = null;
let player = null;
let dir = "";

let m1;
let m2;
let m3;
let m4;

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_WIDTH/2, color = CANVAS_COLOR, showError = true);

	registerEvents();
	registerUIEvents();
	clear();
	titleH1.innerHTML = `${TITLE}! Level: ${level} at Skill: ${skill}`;

	m1 = new Maze(CANVAS_WIDTH/2, CANVAS_WIDTH/4, 5, 10, new Vector(0,0));
	m2 = new Maze(CANVAS_WIDTH/2, CANVAS_WIDTH/4, 5, 10, new Vector(CANVAS_WIDTH/2,0));
	m3 = new Maze(CANVAS_WIDTH/2, CANVAS_WIDTH/4, 5, 10, new Vector(0,CANVAS_WIDTH/4));
	m4 = new Maze(CANVAS_WIDTH/2, CANVAS_WIDTH/4, 5, 10, new Vector(CANVAS_WIDTH/2, CANVAS_WIDTH/4));
	
	// maze_width = (initial_maze_width*level);
	// maze_height = maze_width/2;
	// cell_size = Math.max(Math.min((CANVAS_WIDTH / maze_width)/2*0.75, ((CANVAS_WIDTH/2) / maze_height)/2*0.75), 1);
	// path_size = Math.max(cell_size*2, 1);
	
	// generateCells();
	// scrambleMaze(SCRAMBLE_ITERATIONS);
	// generateLoops(cells.length/50);

	// runSim();
}

function win() {
	level++;
	if(level > MAX_LEVEL) {
		level = 1;
		skill++;
	}
	play();
}

function clear() {
	clearInterval(intervalDraw);
	cells = [];
	root = null;
	player = null;
}

function runSim() {
	drawAll();

	for (let i = 0; i < (5000 * level); i++) {
		moveAll();
	}
	
	intervalDraw = setInterval(function () {
		drawAll();
	}, 1000 / FPS);

	intervalMove = setInterval(function () {
		moveAll();
	}, 1000 / ((level+skill)/10));
}

function moveAll() {
	let rootIndex = root.y * maze_width + root.x;
	let dirs = root.getDirs();

	dir = dirs[random_floor(0, dirs.length)];
	oldRoot = cells[rootIndex];

	rootIndex = getNewIndex(dir, rootIndex);

	try {
		root = cells[rootIndex];
		root.parent = null;
		oldRoot.parent = root;
	}
	catch (e) {
		oldRoot.draw(cell_size, "RED", true);
		console.log("Old root: ", oldRoot);
		console.log("Dir: ", dir);
		console.log("Root index: ", rootIndex);
		throw e;
	}
}

function getNewIndex(dir, index) {
	switch (dir) {
		case "N":
			index -= maze_width;
			break;
		case "S":
			index += maze_width;
			break;
		case "W":
			index -= 1;
			break;
		case "E":
			index += 1;
			break;
	}

	return index;
}

function drawAll() {
	// Canvas
	colorRect(0, 0, canvas.width, canvas.height, CANVAS_COLOR);
	cells.forEach(c => {
		c.draw(cell_size, CELL_COLOR, true, path_size);
	});

	// Start and End
	let finish = cells[cells.length - 1];
	cells[0].drawExtra(cell_size / 1.5, "BLUE", true);
	colorX(finish.canvasPos.x, finish.canvasPos.y, cell_size/1.5, "RED");
	root.drawExtra(cell_size/1.2, "RED", true, path_size);
	player.drawExtra(cell_size / 2, "PINK", true, path_size, true);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}