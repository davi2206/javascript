let btn_ignore_path = document.getElementById("btn_ignore_path");
let btn_reset = document.getElementById("btn_reset");

function registerUIEvents() {
	btn_ignore_path.onclick = function() {
		ignore_path();
	}
	
	btn_reset.onclick = function() { 
		gameReset();
	}
}