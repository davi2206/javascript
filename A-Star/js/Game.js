const LIVE = false;
var canvas;
var canvasContext;

const WALK_SPEED = 5;
const FPS = 144;
const LOCATE_MOUSE = false;

var searching = false;
let ignore_path_weight = false;

var scripts = [
	"Inputs.js", 
	"Level.js", 
	"Tile.js",
	"sim.js",
	"UI.js"];
let commonScripts = ["VectorMath.js", "VectorClass.js", "draw.js"];
var scriptsToLoad = 0;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	// Common Load Scripts
	let baseDir = 'js/';
	commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/A-star/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	setInterval(function() {fpsLoop();}, 1000/FPS);
}

function play() {
	registerUIEvents();
	gameReset();
	registerEvents();
}

function gameReset() {
	openSet = [];
	closedSet = [];
	var levelInitialized = initLevel();
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('Loading...', canvas.width/10, canvas.height/3, 'blue', 80);
	
	while(!levelInitialized) {
		console.log("Initializing level");
	}
	
	initSim();
	searching = true;
	drawLevel();
}

function ignore_path() {
	ignore_path_weight = !ignore_path_weight;
	
	if(ignore_path_weight) {
		btn_ignore_path.value = "Consider past path";
	} else {
		btn_ignore_path.value = "Ignore past path";
	}
	gameReset();
}

function fpsLoop() {
	if(searching) {
		getCurent();
	
		if(currentNode == goalNode || currentNode == null) {
			console.log("Found it!");
			searching = false;
			return;
		}
		currentNode.getNeighbours();
		
		for(var neighbour of currentNode.neighbours) {
			if(!neighbour.isOpen || neighbour.g > (currentNode.g+STEP_VALUE)) { // New node
				neighbour.g = (currentNode.g+neighbour.stepValue);
				neighbour.setValuesHandF();
				neighbour.parentTile = currentNode;
				
				if(!neighbour.isOpen) {
					neighbour.isOpen = true;
					openSet.push(neighbour);
				}
			}
		}
		// Level
		drawCurrent();
	}
	
	findMouseTile();
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		colorText(mouseTileX+","+mouseTileY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

