const LIVE = false;
const CANVAS_COLOR = 'black';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 900;
const TITLE = "Collatz Conjecture"
const FPS = 10;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js", "Node.js"];
let scriptsToLoad = 0;

let init_vector;
let vector;

let startPoint;
let number = 1;
let angle = 4;
let vectorLength = 8;
let lineCount = 2000;

let counter = 0;
let nodes = [];
let max = 0;

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	number = reset();
	init_vector = new Vector(vectorLength, -vectorLength);
	vector = init_vector.copy();

	let all = []

	// For making many lines
	for(i = 0; i <= lineCount; i++) {
		all.push(generateNodes(generateColazNumbers(i)));
	}

	// For making ONE line
	// all.push(generateNodes(generateColazNumbers(871)));

	all.forEach((l) => {
		l.forEach((n) => {
			n.draw();
		})
	});
	// console.log(max);


	// let line = generateColazLine(500);
	// let nodes = generateNodes(line);

	// nodes.forEach((node) => {
	// 	node.draw();
	// })

	// setInterval(function () {
	// 	moveAll();
	// 	drawAll();
	// }, 1000 / FPS);
}

function drawAll() {
	// Canvas
	colorRect(0, 0, canvas.width, canvas.height, CANVAS_COLOR);

	nodes.forEach((node) => {
		node.draw(1.5);
	});
}

function generateColazNumbers(startNr) {
	let debug = startNr % 20000 == 0;
	if(debug) {
		// console.log(startNr);
	}
	let numbers = [];
	numbers.push(startNr);
	while(startNr > 1) {
		counter++;
		startNr = collatz(startNr);
		if(startNr > max) max = startNr;
		numbers.push(startNr);
	}
	
	counter = 0;
	numbers.reverse();
	return numbers;
}

// For generating sea-weed pattern
function generateNodes(line) {
	let nodes = [];
	let startPoint = new Point(0, CANVAS_HEIGHT);
	endPoint = startPoint.copy();

	let color = getRandColorHex();
	
	line.forEach((nr) => {
		let colorNr = 16777215;
		if (nr % 2 == 0) {
			vector.turn_deg(-angle);
		} else {
			vector.turn_deg(angle);
		}
		// vector.set_magnitude(nr);
		endPoint.move_vector(vector);
		
		colorNr /= nr;
		// let n = new Node(startPoint.copy(), endPoint.copy(), "#"+Math.floor(colorNr).toString(16), 5);
		let n = new Node(startPoint.copy(), endPoint.copy(), color, 1);
		nodes.push(n);
		startPoint = endPoint.copy();
	});

	// Resetting the vector, after use
	vector = init_vector.copy();
	return nodes;
}

// For generating graph
function generateNodesGraph(line) {
	let x = 0;
	let nodes = [];
	let startPoint = new Point(0, CANVAS_HEIGHT-line[0]);
	endPoint = startPoint.copy();

	let color = getRandColorHex();

	line.forEach((nr) => {
		endPoint.x = x;
		endPoint.y = CANVAS_HEIGHT-(CANVAS_HEIGHT/max*(nr-1));
		
		let n = new Node(startPoint.copy(), endPoint.copy(), color, 1);
		nodes.push(n);
		startPoint = endPoint.copy();
		x += (CANVAS_WIDTH/(line.length));
	});

	// Resetting the vector, after use
	vector = init_vector.copy();
	return nodes;
}





function collatz(number) {
	if (number == 1) {
		return number;
	}
	if (number % 2 == 0) return (number / 2);
	return (number * 3 + 1)/2;
}

function reset() {
	counter = 0;
	let n = random_floor(0, 5000);
	startPoint = new Point(CANVAS_WIDTH/5000*n, CANVAS_HEIGHT);
	return n;
}



function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}