
function cFlock(boids) {
	this.flock = []
	this.count = 0;
	
	while(this.count < boids) {
		this.deg = random_floor(0, 360);
		//this.deg = this.count;
		
		this.x = random_floor(0, CANVAS_WIDTH);
		this.y = random_floor(0, CANVAS_HEIGHT);
		
		this.vx = random(-1, 1);
		this.vy = random(-1, 1);
		
		this.vector = [this.vx,this.vy];
		
		// Color dem fishies
		
		this.r = random_floor(0,255);
		this.g = random_floor(0,255);
		this.b = random_floor(0,255);
		this.a = 0.1;
		this.color = 'rgba('+this.r+', '+this.g+', '+this.b+', '+this.a+')';
		
		this.flock.push(new cBoid(this.x, this.y, this.deg, this.vector, this.color));
		this.count++;
	}
	
	this.h = CANVAS_HEIGHT/2;
	this.h2 = this.h+25;
	this.w = CANVAS_WIDTH/2;
	this.w2 = this.w+25;
	
	
	// this.flock.push(new cBoid(0, 0, 170, [1,0]));
	// this.flock.push(new cBoid(0, 0, 190, [0,1]));

	// this.flock.push(new cBoid(this.w-BOID_PERCEPTION+10, this.h, 225, [1,-1]));
	// this.flock.push(new cBoid(this.w+BOID_PERCEPTION-10, this.h, 315, [-1,-1]));
	
	// this.flock.push(new cBoid(this.w*2, this.h, 170, [1,0]));
	// this.flock.push(new cBoid(this.w2/2, this.h2, 190, [-1,0]));
	
	// this.flock.push(new cBoid(CANVAS_WIDTH/2-100, CANVAS_HEIGHT/12*10-100, 270));
	// //this.flock.push(new cBoid(300, CANVAS_HEIGHT/5*2, 160));
	this.flock[0].isMarker = true;
	
	this.move = function() {
		this.flock.forEach((boid) => boid.updated = false);
		this.flock.forEach((boid) => boid.move());
	}
	
	this.draw = function() {
		this.flock.forEach((boid) => boid.draw());
	}
}