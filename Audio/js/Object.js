function cObject(x, y, color = OBJECT_COLOR) {
	this.isMarker = false;
	this.steering = "none";
	
	this.speed = 0;
	this.r = slider.value;
	this.x = x;
	this.y  = y;
	this.color = color;
	
	this.move = function() {
		// Update 'this.x' and 'this.y' and 'this.speed'
	}
	
	this.draw = function() {
		// Common drawCircle
		colorCircle(this.x, this.y, this.r, this.color);
	}
}