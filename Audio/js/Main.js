const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "All the Tunes!"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

var scripts = ["ImageLoader.js", "Object.js", "Inputs.js", "UI.js"];
var commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let object_size = 50;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	var baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

function soundMe() {
	console.log("Sound?");
	// AI generated Audio Code
	let audioContext = new (window.AudioContext || window.webkitAudioContext)();
	let tempo = 120; // Beats per minute
	let noteLength = 0.5; // Length of each note in beats
	//let notes = [261.63, 329.63, 392, 523.25, 392, 329.63, 261.63]; // Frequencies of the notes in Hz
	let notes = [261.63, 293.66, 311.13, 349.23, 392, 415.3, 466.16]; // Frequencies of the notes in Hz
	let currentTime;
	let oscillator;
	let oscillator2;
	
	// Schedule the notes to play at the correct time
	for (let i = 0; i < notes.length; i++) {
	  currentTime = audioContext.currentTime + i * (noteLength / tempo) * 60;
	  
	  // 1
	  oscillator = audioContext.createOscillator();
	  oscillator.frequency.value = notes[i];
	  oscillator.connect(audioContext.destination);
	  oscillator.start(currentTime);
	  oscillator.stop(currentTime + (noteLength / tempo) * 60);
	}
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	slider.value = object_size;
	check_box.checked = false;
	loadImages();
	registerEvents();
	registerUIEvents();
	this.object = new cObject(CANVAS_WIDTH/2, CANVAS_HEIGHT/2)
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	this.object.move();
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	this.object.draw();
	
	if(check_box.checked) {
		colorCircle(mousePos.x, mousePos.y, slider.value, "green");
	}
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
	this.object = new cObject(mousePos.x, mousePos.y);
	soundMe();
}

function runTests() {
	// Have tests here
}