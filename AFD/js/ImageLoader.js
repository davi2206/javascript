// Create elements for images
var cityImg = document.createElement("img");
var spaceshipImg = document.createElement("img");
var alienImg = document.createElement("img");
var alienWHumanImg = document.createElement("img");
var misileImg = document.createElement("img");

var pics = [];

var picsToLoad = 0;

function loadImages() {
	const imageMap = new Map();
	imageMap.set(cityImg, "city.png");
	imageMap.set(spaceshipImg, "spaceship.png");
	imageMap.set(alienImg, "alien.png");
	imageMap.set(alienWHumanImg, "alien_human.png");
	imageMap.set(misileImg, "misile.png");
	
	picsToLoad = imageMap.size;
	
	for(var [k,v] of imageMap) {
		beginLoadImg(k, v);
	}
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;
		
		if(picsToLoad == 0) {
			startGame();
		}
	}
	/* Change for env. */
	imgVar.src = "graphics/"+fileName;
	//imgVar.src = "https://theslowloris.com/wp-includes/js/games/AFD/graphics/"+fileName;
}