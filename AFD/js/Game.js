var canvas;
var canvasContext;

var player;
var misiles = [];
var aliens = [];

var liveAliens = 1;
var liveHumans = 100;

var showWinSrcn = false;
var showLoseSrcn = false;

const SHIP_SPEED = 5;
const FLY_ALTITUDE = 55;
const PLAYER_LIVES = 3;
const MISILE_SPEED = 10;
const ALIENS_SEC = 2;
const ALIEN_SPEED = 1;

const FPS = 150;
function play() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorText('Just waiting for images to load...', canvas.width/10, canvas.height/3, 'blue', 80);
	colorText('Sit back and relax', canvas.width/10*2, canvas.height/3*2, 'blue', 70);
	
	player = new Spaceship();
	registerEvents()
	
	gameReset();
	loadImages();
}

function startGame() {
	setInterval(function() {
		checkWinLose();
		moveAll();
		drawAll();
	}, 1000/FPS);
	
	spawnAlien();
}

function spawnAlien() {
	if(aliens.length < liveAliens) {
		aliens.push(new Alien());
	}
	setTimeout(spawnAlien, aliens.length*100);
}

function gameReset() {
	player.reset();
	misiles = [];
	aliens = [];
}

function checkWinLose() {
	if(liveHumans == 0) {
		showLoseSrcn = true;
	}
	
	if(liveAliens == 0) {
		showWinSrcn = true;
	}
}

function drawAll() {
	// Canvas
	//colorRect(0,0,canvas.width,canvas.height,'black');
	drawImgCentRot(cityImg, canvas.width/2, canvas.height/2, 0);

	if(showWinSrcn) {
		colorText('You Won!', 100, 100, 'green', 80);
		colorText('You saved the last ' + liveHumans + ' people', 100, 200, 'green', 60);
		colorText('from anal probing!', 100, 300, 'green', 60);
		colorText('Click the mouse to continue...', 100, 500, 'green', 70);
		return;
	}
	
	if(showLoseSrcn) {
		colorText('YOU SUCK!', 100, 100, 'red', 80);
		colorText('There were ' + liveAliens + " aliens left still!", 100, 200, 'red', 60);
		colorText('Don´t be sad all the humans are dead', 100, 400, 'red', 70);
		colorText('I know you tried your best', 100, 500, 'red', 70);
		colorText('Click the mouse to continue...', 100, 700, 'red', 70);
		return;
	}

	// SpaceShip
	player.draw();
	if(player.misile != undefined) {
		player.misile.draw();
	}
	
	// Aliens
	for(var i = 0; i < aliens.length; i++) {
		//console.log(aliens[i].x);
		aliens[i].draw();
	}
	
	colorText('Aliens left: ' +liveAliens, 25, canvas.height-60, 'Red', 30);
	colorText('Un-probed humans: ' +liveHumans, 25, canvas.height-25, 'Blue', 30);
}

function moveAll() {
	player.move();
	
	for(var i = 0; i < aliens.length; i++) {
		//console.log(aliens[i].x);
		aliens[i].move();
	}
}
