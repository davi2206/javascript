const DEBUG = true;

var tries = 3;
var scriptsToLoad = 0;
var levelMap = new Map();

var aliensDescent = ["js/Inputs.js", "js/Misile.js", "js/Alien.js", "js/Spaceship.js", "js/ImageLoader.js", "js/Game.js"];

window.onload = function() {
	levelMap.set(0, "AFD");
	levelMap.set(1, "BS");
	
	window.addEventListener("keydown", function(e) {
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	canvas = document.getElementById('gameCanvas');
	titleH1 = document.getElementById('gameTitle');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('If you see this, something broke!', canvas.width/10, canvas.height/3, 'blue', 80);
	
	playGames();
}

function playGames() {
	if(tries > 0){
		loadGame(aliensDescent);
		titleH1.innerHTML = "Alien Forces Descent";
	} else {
		colorRect(0, 0, canvas.width, canvas.height, "black");
		colorText('GAME OVER!', canvas.width/2, canvas.height/2, 'red', 80);
		titleH1.innerHTML = "You Lost";
	}
}

function loadGame(scripts) {
	scriptsToLoad = scripts.length;
	if(DEBUG) {
		for(var i = 0; i < scripts.length; i++) {
			var path = scripts[i];
			loadScript(path);
		}
	} else {
		for(var i = 0; i < scripts.length; i++) {
			var path = "https://theslowloris.com/wp-includes/js/games/"+scripts[i];
			loadScript(path);
		}
	}
}