// Level parameters
var levelSize = 80;
const LEVEL_GRID = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 5, 0, 1,
					1, 0, 5, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 0, 0, 0, 0, 5, 0, 1, 0, 0, 0, 0, 2, 0, 0, 1, 0, 5, 0, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 1, 1, 4, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 4, 1, 1,
					1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 5, 0, 0, 0, 1, 0, 5, 0, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 1, 1, 4, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 5, 0, 1,
					1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 4, 1, 1,
					1, 0, 0, 4, 0, 1, 0, 4, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 1,
					1, 3, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
					1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
var levelGrid = [];

// Track tile options
const LEVEL_GROUND = 0;
const LEVEL_WALL = 1;
const LEVEL_START = 2;
const LEVEL_GOAL = 3;
const LEVEL_DOOR = 4;
const LEVEL_KEY = 5;


const LEVEL_COLS = 20;
const LEVEL_ROWS = 15;


function initLevel() {
	levelSize = canvas.width / LEVEL_COLS;
	levelGrid = LEVEL_GRID.slice();
}

function drawLevel() {
	
	var arrayIndex = 0;
	var drawTileX = levelSize/2;
	var drawTileY = levelSize/2;
	
	for(row = 0; row < LEVEL_ROWS; row++) {
		for(col = 0; col < LEVEL_COLS; col++) {
			levelIndex = getArrayIndex(col, row);
			
			var levelType = levelGrid[levelIndex];
			var useImg = levelPics[levelType];
			
			if(checkTransparency(levelType)) {
				//canvasContext.drawImage(levelPics[LEVEL_GROUND], drawTileX, drawTileY);
				drawImgCentRot(levelPics[LEVEL_GROUND], drawTileX, drawTileY, 0, TILE_SIZE, TILE_SIZE);
			}
			
			if(levelType == LEVEL_START) {
				useImg = levelPics[LEVEL_GROUND];
			}
			
			//canvasContext.drawImage(useImg, drawTileX, drawTileY);
			drawImgCentRot(useImg, drawTileX, drawTileY, 0, TILE_SIZE, TILE_SIZE);
			arrayIndex++;
			drawTileX += levelSize;
		}
		drawTileX = 0;
		drawTileY += levelSize;
	}
	drawTileY = 0;
}

function checkTransparency(levelType) {
	 if(levelType == LEVEL_DOOR ||
		levelType == LEVEL_GOAL ||
		levelType == LEVEL_KEY) {
			return true;
		}
	return false;
}