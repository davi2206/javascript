// Class Player
function cPlayer() {
	this.name;
	this.x = 500;
	this.y = 500;
	
	var nextX = 0;
	var nextY = 0;
	
	this.keys = 0;
	this.playerPic;
	
	// Key press vars
	this.keyUp = false;
	this.keyDown = false;
	this.keyLeft = false;
	this.keyRight = false;
	
	this.up;
	this.right;
	this.down;
	this.left;
	
	this.setupInput = function (upKey, rightKey, downKey, leftKey) {
		this.up = upKey;
		this.right = rightKey;
		this.down = downKey;
		this.left = leftKey;
	}

	this.reset = function(playerPic, name) {
		this.playerPic = playerPic;
		this.name = name;
		// Player starting possition
		this.keys = 0;
		for(row = 0; row < LEVEL_ROWS; row++) {
			for(col = 0; col < LEVEL_COLS; col++) {
				levelIndex = getArrayIndex(col, row);
				
				if(levelGrid[levelIndex] == LEVEL_START) {
					this.x = col*levelSize;
					this.y = row*levelSize;
					levelGrid[levelIndex] = LEVEL_GROUND;
					nextX = this.x;
					nextY = this.y;
					return;
				} // End if
			} // End for col
		} // End for row
		console.log("NO START POINT FOUND!!");
	} //End reset funct

	this.move = function() {
		nextX = this.x;
		nextY = this.y;
		
		if(this.keyUp) {
			nextY -= WALK_SPEED;
		}
		if(this.keyDown) {
			nextY += WALK_SPEED;
		}
		if(this.keyLeft) {
			nextX -= WALK_SPEED;
		}
		if(this.keyRight) {
			nextX += WALK_SPEED;
		}
		
		var index = this.getIndex(nextX, nextY);
		var nextTile = this.getTileAtIndex(index);

		switch(nextTile) {
			case LEVEL_DOOR:				// Maybe unlock
				if(this.keys > 0) {
					levelGrid[index] = LEVEL_GROUND;
					this.keys--;
				}
				break;
			case LEVEL_GOAL:				// Win the game
				showWinSrcn = true;
				break;
			case LEVEL_KEY:					// Pickup key, then walk on
				levelGrid[index] = LEVEL_GROUND;
				this.keys++;
			case LEVEL_GROUND:				// Walk on
				this.x = nextX;
				this.y = nextY;
				break;
			case LEVEL_WALL:				// Stop at walls
			default: // Freeze
				break;
		}
	}

	this.getIndex = function (x, y) {
		var col = Math.floor(x / levelSize);
		var row = Math.floor(y / levelSize);
		return getArrayIndex(col, row);
	}

	this.getTileAtIndex = function(index) {
		return levelGrid[index];
	}
}