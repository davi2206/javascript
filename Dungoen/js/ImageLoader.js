// Create elements for images
var playerPic = document.createElement("img");

var levelPics = [];

var wallPic = document.createElement("img");
var roadPic = document.createElement("img");
var goalPic = document.createElement("img");

var picsToLoad = 0;

function loadImages() {
	var imageList = [
		{varName: playerPic, fileName: "hero.png"},
		{levelType: LEVEL_GROUND, fileName: "ground.png"},
		{levelType: LEVEL_WALL, fileName: "wall.png"},
		{levelType: LEVEL_DOOR, fileName: "door.png"},
		{levelType: LEVEL_GOAL, fileName: "goal.png"},
		{levelType: LEVEL_KEY, fileName: "key.png"}
	];
	
	picsToLoad = imageList.length;
	
	for(var i = 0; i < imageList.length; i++) {
		if(imageList[i].varName != undefined) {
			beginLoadImg(imageList[i].varName, imageList[i].fileName);
		} else {
			loadLevelImage(imageList[i].levelType, imageList[i].fileName);
		}
	}
}

function loadLevelImage(levelCode, fileName) {
	levelPics[levelCode] = document.createElement("img");
	beginLoadImg(levelPics[levelCode], fileName);
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;	
		if(picsToLoad == 0) {
			return true;
		}
	}
	
	var baseDir = 'Graphics/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/dungeon/Graphics/';
	}
	imgVar.src = baseDir+fileName;
}