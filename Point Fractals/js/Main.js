const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 3000;
const CANVAS_HEIGHT = 3000;
const TITLE = "Point Fractals"
const FPS = 1000;

let scripts = ["Inputs.js", "UI.js"];
let commonScripts = ["draw.js", "Vector.js", "Point.js"];
var scriptsToLoad = 0;

let point_size = 3;
let ratio = 0.7;
let sides = 7;
//let colors = ["#FF0000","#80FF00","#00FFFF","#8000FF","#0000FF","#FF0080","#FFFF00","#00FF7F"];
let colors = ["red","green","blue"];

let multi_color = true;

let cornerPoints;
let cursor;
let p, index, color;

let total_points = 0;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	registerEvents();
	registerUIEvents();

	cornerPoints = getPolygonPoints(CANVAS_WIDTH/2, CANVAS_HEIGHT/2, CANVAS_HEIGHT/2, sides, inDirection = new Vector(0,-1));
	cursor = new Point(CANVAS_WIDTH/2, CANVAS_HEIGHT/2);

	setInterval(function() {
		setPoint();
	}, 1000/FPS);
}

function setPoint() {
	for(let i = 0; i < 10; i++)
	{
		index = random_floor(0, cornerPoints.length-1);
		p = cornerPoints[index];
		color = multi_color ? colors[index] : "black";

		let vector = new Vector(p.x - cursor.x, p.y - cursor.y);

		vector.mult(ratio);
		cursor.move_vector(vector);

		cursor.draw(point_size, color, false, 0.5);
		total_points++;
		titleH1.innerHTML = TITLE + ": " + total_points;
	}
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}