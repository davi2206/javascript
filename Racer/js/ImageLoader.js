// Create elements for images
var blueCarPic = document.createElement("img");
var greenCarPic = document.createElement("img");

var trackPics = [];

var wallPic = document.createElement("img");
var roadPic = document.createElement("img");
var goalPic = document.createElement("img");

var picsToLoad = 0;

function loadImages() {
	var imageList = [
		{varName: blueCarPic, fileName: "blue.png"},
		{varName: greenCarPic, fileName: "green.png"},
		{trackType: TRACK_ROAD, fileName: "road.png"},
		{trackType: TRACK_WALL, fileName: "wall.png"},
		{trackType: TRACK_GOAL, fileName: "goal.png"}
	];
	
	picsToLoad = imageList.length;
	
	for(var i = 0; i < imageList.length; i++) {
		if(imageList[i].varName != undefined) {
			beginLoadImg(imageList[i].varName, imageList[i].fileName);
		} else {
			loadTrackImage(imageList[i].trackType, imageList[i].fileName);
		}
	}
}

function loadTrackImage(trackCode, fileName) {
	trackPics[trackCode] = document.createElement("img");
	beginLoadImg(trackPics[trackCode], fileName);
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;	
		if(picsToLoad == 0) {
			startGame();
		}
	}
	imgVar.src = "Graphics/"+fileName;
}