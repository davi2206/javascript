// Track parameters
var trackSize = 80;
const TRACK_GRID = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1,
					1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,
					1, 0, 2, 2, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1,
					1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1,
					1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
					1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
					1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
var trackGrid = [];

// Track tile options
const TRACK_ROAD = 0;
const TRACK_WALL = 1;
const TRACK_START = 2;
const TRACK_GOAL = 3;
const TRACK_COLS = 20;
const TRACK_ROWS = 15;


function initTrack() {
	trackSize = canvas.width / TRACK_COLS;
	trackGrid = TRACK_GRID.slice();
}

function drawTrack() {
	
	var arrayIndex = 0;
	var drawTileX = 0;
	var drawTileY = 0;
	
	for(row = 0; row < TRACK_ROWS; row++) {
		for(col = 0; col < TRACK_COLS; col++) {
			trackIndex = getArrayIndex(col, row);
			
			var trackType = trackGrid[trackIndex];
			var useImg = trackPics[trackType];
			
			if(trackType == TRACK_START) {
				useImg = trackPics[TRACK_ROAD];
			}
			
			canvasContext.drawImage(useImg, drawTileX, drawTileY);
			arrayIndex++;
			drawTileX += trackSize;
		}
		drawTileX = 0;
		drawTileY += trackSize;
	}
	drawTileY = 0;
}