const CAR_GAS_POWER = 0.03;
const CAR_BREAK_POWER = 0.02;
const CAR_TURN_RATE = 0.02;
const CAR_MOMENTUM = 0.99;

function carClass() {
	this.name;
	this.x = 500;
	this.y = 500;
	this.ang = 0;
	this.speed = 0;
	this.currentLap = 0;
	this.carPic;
	
	// Key press vars
	this.keyGas = false;
	this.keyReverse = false;
	this.keyLeft = false;
	this.keyRight = false;
	
	this.up;
	this.right;
	this.down;
	this.left;
	
	this.setupInput = function (upKey, rightKey, downKey, leftKey) {
		this.up = upKey;
		this.right = rightKey;
		this.down = downKey;
		this.left = leftKey;
	}

	this.reset = function(carPic, name) {
		this.carPic = carPic;
		this.name = name;
		// Car starting possition
		this.speed = 0;
		this.currentLap = 0;
		for(row = 0; row < TRACK_ROWS; row++) {
			for(col = 0; col < TRACK_COLS; col++) {
				trackIndex = getArrayIndex(col, row);
				
				if(trackGrid[trackIndex] == TRACK_START) {
					this.ang = -Math.PI/2; // Turn the car 90* left to face up. Math because angles are in rad
					this.x = col*trackSize + trackSize/2;
					this.y = row*trackSize + trackSize/2;
					trackGrid[trackIndex] = TRACK_ROAD;
					return;
				} // End if
			} // End for col
		} // End for row
		console.log("NO START POINT FOUND!!");
	} //End reset funct

	this.move = function() {
		this.speed *= CAR_MOMENTUM;
		if(this.keyGas) {
			this.speed += CAR_GAS_POWER;
		}
		if(this.keyReverse) {
			this.speed -= CAR_BREAK_POWER;
		}
		if(this.keyLeft) {
			this.ang -= this.speed/100; // CAR_TURN_RATE;
		}
		if(this.keyRight) {
			this.ang += this.speed/100; // CAR_TURN_RATE;
		}
		
		this.x += Math.cos(this.ang)*this.speed;
		this.y += Math.sin(this.ang)*this.speed;
		
		this.bounceWalls();
	}

	this.bounceWalls = function() {
		var carCol = Math.floor(this.x / trackSize);
		var carRow = Math.floor(this.y / trackSize);
		var carIndex = getArrayIndex(carCol, carRow);
		var carGrid = trackGrid[carIndex];
		var armpitTest = true;
		
		if((carCol >= 0 && carCol < TRACK_COLS) &&	// Inside game sides
		   (carRow >= 0 || carRow < TRACK_ROWS)) {	// Inside game top/botom
				switch(carGrid) {
					case TRACK_WALL:				// Gollision
						this.collide();
						break;
					case TRACK_GOAL:				// Passing finish line
						this.passGoal();
						break;
				}
		}
		else { // Off the map
			this.collide();
		}
	}

	this.collide = function() {
		this.x -= Math.cos(this.ang) * this.speed;
		this.y -= Math.sin(this.ang) * this.speed;
		this.speed *= -0.5;
	}

	this.passGoal = function() {
		var prevX = this.x - Math.cos(this.ang) * this.speed;
		var prevY = this.y - Math.sin(this.ang) * this.speed;
		
		var x = Math.floor(prevX / trackSize);
		var y = Math.floor(prevY / trackSize);
		var index = getArrayIndex(x, y);
		
		if(trackGrid[index] != TRACK_GOAL) {
			if(this.currentLap == LAPS) {
				winner = this.name;
				showWinSrcn = true;
			}
			this.currentLap++;
		}
	}
}