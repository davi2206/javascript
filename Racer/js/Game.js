const LIVE = false;

var canvas;
var canvasContext;

var showWinSrcn = false;
var winner = "n/a";

var blueCar = new carClass();
var greenCar = new carClass();

const LAPS = 1;

const FPS = 120;
const LOCATE_MOUSE = false;

let scripts = [];
let commonScripts = ["draw.js"];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
}

function play() {
	loadImages();
}

function startGame() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('Just waiting for images to load...', canvas.width/10, canvas.height/3, 'blue', 80);
	colorText('Sit back and relax', canvas.width/10*2, canvas.height/3*2, 'blue', 70);

	initTrack();
	gameReset();
	registerEvents();
	
	setInterval(function() {
		drawAll();
		blueCar.move();
		greenCar.move();
	}, 1000/FPS);
}

function gameReset() {
	initTrack();
	blueCar.reset(blueCarPic, "Blue Car");
	greenCar.reset(greenCarPic, "Green Looser");
}

function drawAll() {	
	// End Game screens
	if(showWinSrcn) {
		colorRect(0, 0, canvas.width, canvas.height, "black");
		colorText(winner+' won the race!', canvas.width/10, canvas.height/3, 'green', 80);
		colorText('Click the mouse to continue...', canvas.width/10*2, canvas.height/3*2, 'blue', 70);
		return;
	}
	
	// Tracks
	drawTrack();
	
	// Car
	drawImgCentRot(blueCar.carPic, blueCar.x, blueCar.y, blueCar.ang);
	drawImgCentRot(greenCar.carPic, greenCar.x, greenCar.y, greenCar.ang);
	
	colorText('Lap: ' + blueCar.currentLap + '/' + LAPS, canvas.width/10*8, canvas.height/10*8, 'green', 30);
	colorText('Lap: ' + greenCar.currentLap + '/' + LAPS, canvas.width/10*8, canvas.height/10*9, 'green', 30);
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		var mouseTrackX = Math.floor(mouseX / trackSize);
		var mouseTrackY = Math.floor(mouseY / trackSize);
		var mouseIndex = getArrayIndex(mouseTrackX, mouseTrackY);
		colorText(mouseTrackX+","+mouseTrackY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = TRACK_COLS * row + col;
	return index;
}