var canvas;
var canvasContext;

// Paddle parameters
const PADDLE_WIDTH = 150;
const PADDLE_HEIGHT = 15;
var paddleX = 0;
var paddleY = 0;

// Ball parameters
var ballWidth = 15;
var ballX = ballY = 0;
var ballSpeedY = 0;
var ballSpeedX = 0;
var deltaX = 0;
var speedCtrl = 10; // Controll angular speed

// Brick parameters
const BRICK_COLOR = 'blue';
const BRICK_SPACING_PCT = 2;
const BRICK_COLS = 5;
const BRICK_ROWS = 21;
const EMPTY_ROWS = 1;
const BRICK_HEIGHT_PCT = 10;
var brickHeight = 50;
var brickWidth = 100;
var brickGep = 1;
var brickCount = 0;
var bricksLeft = 0;
var brickGrid;

var showWinSrcn = false;
var showLoseSrcn = false;
var playerLives;
var playerScore = 0;

var locateMouse = true;
var mouseX;
var mouseY;

const BALL_SPEED = 4;
const DIST_FROM_BOTTOM = 50;
const FPS = 120;

window.onload = function() {
	locateMouse = false;
	
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	gameReset();
	
	setInterval(function() {
		drawAll();
		moveAll();
	}, 1000/FPS);
	
	canvas.addEventListener('mousedown', handleMouseClick);
	
	canvas.addEventListener('mousemove', movePaddle);
}

function initBricks() {
	brickWidth = canvas.width / BRICK_COLS;
	brickHeight = brickWidth / 100 * BRICK_HEIGHT_PCT;
	brickGep = brickWidth / 100 * BRICK_SPACING_PCT
	
	var i = 0;
	for(; i < BRICK_COLS * EMPTY_ROWS; i++) {
		brickGrid[i] = false;
	}
	for(; i < brickCount; i++) {
		brickGrid[i] = true;
		bricksLeft++;
	}
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height,'black');
	
    if(showWinSrcn) {
        ballSpeedY = 0;
        ballSpeedX = 0;
		colorText('You Won! Final score was: ' + playerScore, canvas.width/10, canvas.height/3, 'green', 40);
        colorText('And you had ' + playerLives + ' balls left!', canvas.width/9, canvas.height/2, 'green', 35);
		colorText('Click the mouse to continue...', canvas.width/10*2, canvas.height/3*2, 'green', 35);
		return;
	}
	
	if(showLoseSrcn) {
		colorText('YOU SUCK! Final score was: ' + playerScore, canvas.width/10, canvas.height/3, 'red', 40);
		colorText('Click the mouse to continue...', canvas.width/10*2, canvas.height/3*2, 'green', 35);
		return;
	}
	
	// Paddle
	colorRect(paddleX,paddleY,PADDLE_WIDTH,PADDLE_HEIGHT,'white');
	
	// Bricks
	drawRemainingBricks();
	
	// Ball
	colorCircle(ballX, ballY, ballWidth/2, 'white');
	
	colorText('Balls left: ' + playerLives, canvas.width/10, canvas.height/10*9, 'green', 30);
	colorText('Score: ' + playerScore, canvas.width/10*8, canvas.height/10*9, 'green', 30);
	
	// Print Mouse Possition
	if(locateMouse) {
		var mouseBrickX = Math.floor(mouseX / brickWidth);
		var mouseBrickY = Math.floor(mouseY / brickHeight);
		var mouseIndex = getArrayIndex(mouseBrickX, mouseBrickY);
		colorText(mouseBrickX+","+mouseBrickY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
}

function drawRemainingBricks() {
	for(row = 0; row < BRICK_ROWS; row++) {
		for(col = 0; col < BRICK_COLS; col++) {
			brickIndex = getArrayIndex(col, row);
			
			if(brickGrid[brickIndex]) {
				colorRect(brickWidth*col, brickHeight*row, brickWidth-brickGep, brickHeight-brickGep, BRICK_COLOR);
			}
		}
	}
}

function moveAll() {
	moveBall();
	killBall();
	ballBounceBricks();
	
	ballX += ballSpeedX;
	ballY += ballSpeedY;
}

function moveBall() {
	// Horizontal movement
	// Bounce off walls
	if((ballX-ballWidth/2 < 0) && (ballSpeedX < 0)) {
		ballSpeedX = -ballSpeedX;
	}
	if((ballX+ballWidth/2 > canvas.width) && (ballSpeedX > 0)) {
		ballSpeedX = -ballSpeedX;
	}

	// Vertical movement
	if(ballY + ballWidth / 2 >  paddleY && ballY + ballWidth / 2 < paddleY + PADDLE_HEIGHT) { // Below paddle
		if(ballX+ballWidth/2 > paddleX && ballX-ballWidth/2 < paddleX+PADDLE_WIDTH) { // Ball inside paddle
			ballSpeedY = -ballSpeedY;
			deltaX = ballX - (paddleX + PADDLE_WIDTH / 2);
			ballSpeedX = deltaX / speedCtrl;
		}
	} else if (ballY-ballWidth/2 < 0) { // Bounce of ceiling
		ballSpeedY = -ballSpeedY;
	}
}

function killBall() {
	if(ballY > canvas.height) { // Ball outside game
		removeLife();
		ballReset();
	}
}

function ballBounceBricks() {
	var ballCol = Math.floor(ballX / brickWidth);
	var ballRow = Math.floor(ballY / brickHeight);
	var ballIndex = getArrayIndex(ballCol, ballRow);
	
	var armpitTest = true;
	
	if((ballCol >= 0 && ballCol < BRICK_COLS) &&	// Determening collision
	   (ballRow >= 0 || ballRow < BRICK_ROWS) &&	// Determening collision
	   (ballIndex < BRICK_COLS*BRICK_ROWS) &&		// Determening collision
	   (brickGrid[ballIndex])) {					// Determening collision
		
		brickGrid[ballIndex] = false;
		bricksLeft--;
        if(bricksLeft == 0) showWinSrcn = true;
        
		playerScore++;
		
		// Getting ball position in last frame
		var prevBallX = ballX-ballSpeedX;
		var prevBallY = ballY-ballSpeedY;
		var prevBallCol = Math.floor(prevBallX / brickWidth);
		var prevBallRow = Math.floor(prevBallY / brickHeight);
		
		if(prevBallCol != ballCol) {
			var adjBrickSide = getArrayIndex(prevBallCol, ballRow); // Brick next to the one hit
			
			if(!brickGrid[adjBrickSide]) {
				ballSpeedX *= -1;
				armpitTest = false;
			}
		}
		if(prevBallRow != ballRow) {
			var adjBrickTopBtm = getArrayIndex(ballCol, prevBallRow); // Brick above/below the one hit
			
			if(!brickGep[adjBrickTopBtm]) {
				ballSpeedY *= -1;
				armpitTest = false;
			}
		}
		if(armpitTest) {
			ballSpeedX *= -1;
			ballSpeedY *= -1;
		}
	}
}

function movePaddle(evt) {
	var mousePos = getMousePos(evt);
	paddleX = mousePos.x-PADDLE_WIDTH/2;
	
	if(ballSpeedY == 0) {
		ballX = mousePos.x;
	}
}

function removeLife(){
	playerLives--;
	if(playerLives == 0) {
		showLoseSrcn = true;
	}
}

function gameReset() {
	playerLives = 3;
	playerScore = 0;
	
	// Paddle starting possition
	paddleX = canvas.width/2-PADDLE_WIDTH/2;
	paddleY = canvas.height-PADDLE_HEIGHT-DIST_FROM_BOTTOM;
	
	brickCount = BRICK_COLS*BRICK_ROWS;
	brickGrid = new Array(brickCount);
	ballReset();
	initBricks();
}

// Support functions
function colorRect(x, y, width, height, color){
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function colorCircle(x, y, r, color){
	canvasContext.fillStyle = color;
	canvasContext.beginPath();
	canvasContext.arc(x, y, r, 0, Math.PI*2, true);
	canvasContext.fill();
}

function colorText(words, posX, posY, color, size) {
	canvasContext.font = 'normal '+size+'px Arial';
	canvasContext.fillStyle = color;
	canvasContext.fillText(words, posX, posY);
}

function getMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouseX = evt.clientX - rect.left - root.scrollLeft;
	mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function getArrayIndex(col, row) {
	var index = BRICK_COLS * row + col;
	return index;
}

function handleMouseClick(evt) {
	if(showWinSrcn || showLoseSrcn) {
		showWinSrcn = false;
		showLoseSrcn = false;
		gameReset();
	} else if (ballSpeedY == 0) {
		ballSpeedY = BALL_SPEED;
	}
}

function ballReset() {
	// Ball starting possition
	ballX = (canvas.width/2)+1;
	ballY = (paddleY-ballWidth/2);
	ballSpeedX = 0;
	ballSpeedY = 0;
}