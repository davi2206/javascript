const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Test Project"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_SIZE = 15;
const OBJECT_COLOR = 'white';

var scripts = ["Inputs.js"]; //["Script_1.js", "Script_2.js"];
var scriptsToLoad = 0;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, CANVAS_COLOR, showError = false)
	
	// Common Load Scripts
	var baseDir = 'js/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
	runTests();
}

// Automatically called when all scripts are loaded
function play() {
	registerEvents()
}

function runSim() {
	setInterval(function() {}, 1000/FPS);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
	console.log(mousePos);
}

function runTests() {
	let test_name;
	let expectation;
	let result;
	let vector_one = new Vector(1,1);
	console.log("Vector: " + vector_one);
	
	// Add
	let vector_two = new Vector(2,3);
	vector_one.add(vector_two);
	test_name = "Add";
	expectation = [3,4];
	result = [vector_one.x, vector_one.y];
	test(test_name, expectation, result);
		
	// Sub
	let vector_three = new Vector(2,2);
	vector_one.subtract(vector_three);
	test_name = "Subtract";
	expectation = [1,2];
	result = [vector_one.x, vector_one.y];
	test(test_name, expectation, result);
	
	// Mult
	let four = 4;
	vector_one.mult(four);
	test_name = "Multiply";
	expectation = [4,8];
	result = [vector_one.x, vector_one.y];
	test(test_name, expectation, result);
	
	// Div
	let five = 2;
	vector_one.div(five);
	test_name = "Divide";
	expectation = [2,4];
	result = [vector_one.x, vector_one.y];
	test(test_name, expectation, result);
	
	// Get Mag
	let mag = vector_one.get_magnitude();
	test_name = "Get Magnitude";
	expectation = 4.47213595499958;
	result = mag;
	test(test_name, expectation, result);
	
	// Get Ang Rad
	let vector_six = new Vector(5,5);
	let rad = vector_six.get_ang_rad();
	test_name = "Get Radian Angle";
	expectation = 0.7853981633974483;
	result = rad;
	test(test_name, expectation, result);
	
	// Get And Deg
	let deg = vector_six.get_ang_deg();
	test_name = "Get Degree Angle";
	expectation = 45;
	result = deg;
	test(test_name, expectation, result);
	
	// Dot Product
	let vector_seven = new Vector(2,7);
	let vector_eight = new Vector(9,13);
	let dot = vector_seven.dot(vector_eight);
	test_name = "Get DOT product";
	expectation = 109;
	result = dot;
	test(test_name, expectation, result);
	
	// Extend pct
	let vector_nine = new Vector(3,4);
	let ten = 15;
	vector_nine.extend_pct(ten);
	test_name = "Extend Percent";
	expectation = [3.45, 4.60];
	result = [round(vector_nine.x,2), round(vector_nine.y,2)];
	test(test_name, expectation, result);
	
	// Extend dist
	let vector_eleven = new Vector(3,4);
	let twelve = 15;
	vector_eleven.extend_dist(twelve);
	test_name = "Extend Distance";
	expectation = [12, 16];
	result = [vector_eleven.x, vector_eleven.y];
	test(test_name, expectation, result);
	
	// Set Mag
	let vector_twelve = new Vector(3,4);
	let thirteen = 10;
	vector_twelve.set_magnitude(thirteen);
	test_name = "Set Magnitude";
	expectation = [6, 8];
	result = [vector_twelve.x, vector_twelve.y];
	test(test_name, expectation, result);
	
	// Turn Rad
	let vector_fourteen = new Vector(3,4);
	let fifteen = 0.610865;
	let sixteen = vector_fourteen.get_ang_rad();
	vector_fourteen.turn_rad(fifteen);
	let seventeen = vector_fourteen.get_ang_rad();
	test_name = "Turn Radian";
	expectation = round(fifteen+sixteen,1);
	result = round(seventeen, 1);
	test(test_name, expectation, result);
	
	// Turn Deg
	let vector_eighteen = new Vector(3,4);
	let nineteen = 90;
	let twenty = vector_eighteen.get_ang_deg();
	vector_eighteen.turn_deg(nineteen);
	let twentyone = vector_eighteen.get_ang_deg();
	
	console.log("DEBUG:: ");
	console.log(twenty);
	console.log(twentyone);
	
	test_name = "Turn Degree";
	expectation = (nineteen+twenty);
	result = twentyone;
	test(test_name, expectation, result);
	
	// Draw
}

function test(test_name, expectation, result) {
	let evaluation;
	let color;
	let info;
	
	if(JSON.stringify(expectation) != JSON.stringify(result)) {
		evaluation = "Failed";
		color = "color: rgb(255,0,0)";
		info = `: ${expectation} -> ${result}`
	} else {
		evaluation = "Passed";
		color = "color: rgb(100,200,100)"
		info = "";
	}
	
	console.log(`%c ${test_name} ${evaluation} ${info}`, color);
}






















