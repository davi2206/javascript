const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Project Title"
const FPS = 3;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let step = 1;
let curent_nr = 0;
let visited_nrs = [];

let notes = [261.63, 293.66, 329.63, 349.23, 392, 440, 493.88, 523.25];
//				C	  D	   		E		F	  G	  A		B		C		+20


window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	let o = setupAudio();
	setInterval(function() {
		moveAll(o);
	}, 1000/FPS);
}

function moveAll(o) {
	visited_nrs[curent_nr] = true;
	if(curent_nr - step < 0 || visited_nrs[curent_nr-step]) {
		curent_nr += step;
	}
	else {
		curent_nr -= step;
	}
	step++;

	// playNotes(curent_nr);
	playNote(curent_nr, o);

	if(curent_nr > 200) {
		o.stop();
	}
}

function setupAudio() {
	var context = new AudioContext();
	var  g = context.createGain();

	let o  = context.createOscillator();
	o.frequency.value = 0;
	o.connect(g);
	g.connect(context.destination);
	o.start(0);

	return o;
}

function playNote(nr, o) {
	let mod_nr = nr % 88;

	let freq = Math.pow(2, (mod_nr+50-49)/12) * 440;
	o.frequency.value = freq;

	console.log("Nr: " + nr);
	console.log("Mod Nr: " + mod_nr);
	console.log("Freq: " + freq);
}

function playNotes(nr) {
	let digits = getDigitsOfNr(nr);

	var context = new AudioContext();
	var  g = context.createGain();

	let tones = [];

	for(let i = 0; i < digits.length; i++) {
		tones[i] = context.createOscillator();

		let multiplier = i * 2;

		let freq = notes[i] + (notes[i]*multiplier);
		tones[i].frequency.value = freq;
		tones[i].connect(g);
		g.connect(context.destination);

		console.log(freq);
	}

	tones.forEach(t => t.start(0));

	g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 1);
}

function getDigitsOfNr(nr) {
	let digits = nr.toString().split('');
	let realDigits = digits.map(Number)
	return realDigits;
}

function onMouseClick(){}