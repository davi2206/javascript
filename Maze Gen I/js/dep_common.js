function colorRect(x, y, width, height, color){
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function colorCircle(x, y, r, color){
	canvasContext.fillStyle = color;
	canvasContext.beginPath();
	canvasContext.arc(x, y, r, 0, Math.PI*2, true);
	canvasContext.fill();
}

function colorText(words, posX, posY, color, size) {
	canvasContext.font = 'normal '+size+'px Arial';
	canvasContext.fillStyle = color;
	canvasContext.fillText(words, posX, posY);
}

function drawImgCentRot(img, x, y, ang) {
	canvasContext.save();
	canvasContext.translate(x, y);
	canvasContext.rotate(ang);
	canvasContext.drawImage(img, -img.width/2, -img.height/2);
	canvasContext.restore();
}

function getMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouseX = evt.clientX - rect.left - root.scrollLeft;
	mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function removeFromSet(object, set) {
	for(var i = set.length-1; i>=0; i--) {
		if(set[i] == object) {
			set.splice(i, 1);
		}
	}
}