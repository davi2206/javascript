// Level parameters
var tileSize;

var levelGrid = [];
const tileMap = new Map();
var startTileIndex;
var goalTileIndex;
var startNode;
var goalNode;

// Track tile options
const LEVEL_GROUND = 0;
const LEVEL_WALL = 1;
const LEVEL_START = 2;
const LEVEL_UNDEFINED = 3;

const LEVEL_COLS = 75;
const LEVEL_ROWS = 35;

function initLevel() {
	tileSize = canvas.width / LEVEL_COLS;
	levelGrid = [];
	generateLevel();
	drawLevel();
	
	startNode = tileMap.get(startTileIndex);
	
	return true;
}

function generateLevel() {
	var arrayIndex = 0;
	var drawTileX = 0;
	var drawTileY = 0;
	
	nodes = LEVEL_COLS*LEVEL_ROWS;
	var startCol = Math.floor(Math.random() * (LEVEL_COLS/8))+1;
	var startRow = Math.floor(Math.random() * (LEVEL_ROWS/8))+1;
	
	startTileIndex = getArrayIndex(startCol, startRow);
	
	for(row = 0; row < LEVEL_ROWS; row++) {
		for(col = 0; col < LEVEL_COLS; col++) {
			levelIndex = getArrayIndex(col, row);
			
			var levelType = LEVEL_UNDEFINED;
			
			if(startTileIndex == levelIndex) {
				levelType = LEVEL_START;
			}
			
			// Set Walls
			if(col == 0 || col == (LEVEL_COLS-1) || row == 0 || row == (LEVEL_ROWS-1)) {
				levelType = LEVEL_WALL;
			}
			
			tileMap.set(levelIndex, new cTile(col, row, levelIndex, levelType));
			levelGrid[levelIndex] = levelType;
			
			arrayIndex++;
			drawTileX += tileSize;
		}
		drawTileX = 0;
		drawTileY += tileSize;
	}
	drawTileY = 0;
}

function drawLevel() {
	for(const [k, v] of tileMap) {
		var color = "pink";
		switch(v.tileType) {
			case LEVEL_GROUND:
				color = "white";
				break;
			case LEVEL_WALL:
				color = "black";
				break;
			case LEVEL_START:
				color = "green";
				break;
			case LEVEL_UNDEFINED:
				color = "gray";
				break;
			default:
				color = "black";
				break;
		}
		
		if(v.isOpen) {
			color = "green";
		}
		
		v.draw(color);
		//colorText("G: " + v.g, v.col*tileSize+2, v.row*tileSize+2, 'yellow', 7);
		//colorText("H: " + v.h, v.col*tileSize+2, v.row*tileSize+10, 'yellow', 7);
	}
}

function drawCurrent() {
	drawSet(openSet);
}

function drawSet(set) {
	for(var v of set) {
		var color = "pink";
		switch(v.tileType) {
			case LEVEL_GROUND:
				color = "white";
				break;
			case LEVEL_WALL:
				color = "black";
				break;
			case LEVEL_START:
				color = "green";
				break;
			case LEVEL_UNDEFINED:
				color = "gray";
				break;
			default:
				color = "black";
				break;
		}
		
		if(v.isOpen) {
			color = "green";
		}
		if(v.isClosed) {
			color = "blue";
		}
		
		v.draw(color);
		//colorText("G: " + v.g, v.col*tileSize+2, v.row*tileSize+2, 'yellow', 7);
		//colorText("H: " + v.h, v.col*tileSize+2, v.row*tileSize+10, 'yellow', 7);
	}
}

function setGoal() {
	var goal = false;
	while(!goal) {
		var goalCol = LEVEL_COLS-Math.floor(Math.random() * (LEVEL_COLS/8))-1;
		var goalRow = LEVEL_ROWS-Math.floor(Math.random() * (LEVEL_ROWS/8))-1;
		
		goalTileIndex = getArrayIndex(goalCol, goalRow);
		goalNode = tileMap.get(goalTileIndex);
		
		if(goalNode.tileType == LEVEL_GROUND) {
			goal = true;
		}
	}
}