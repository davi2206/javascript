let btn_reset = document.getElementById("btn_reset");

function registerUIEvents() {
	try {
		btn_reset.onclick = function() {
			gameReset();
		}
	}
	catch {
		console.log("No btns for you");
	}
}