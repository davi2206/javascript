// Generating a maze, I hope

var openSet = []; // Known nodes to be evaluated
var startNode;
var currentNode;

function initSim() {
	startNode = tileMap.get(startTileIndex);

	startNode.isOpen = false;
	startNode.isClosed = true;
	openSet = [];
	closedSet = [];
	
	openSet.push(startNode);
}

function getCurent() {
	currentNode = null;
	
	var pick = Math.random() * openSet.length;
	var i = Math.floor(pick);
	currentNode = openSet[i];
	
	if(currentNode != null) {
		removeFromSet(currentNode, openSet);
		currentNode.isOpen = false;
	} else {
		// Debug only
		//console.log("currentNode: "+currentNode);
	}
}

function step() {
	if(searching) {
		getCurent();
		
		if(currentNode == null) { // We finished the Maze!
			finish();
			return;
		}
		var walls = currentNode.getNeighbours();
		var col = 'pink';
		if(walls > 2) {
			currentNode.tileType = LEVEL_GROUND;
			currentNode.draw('white');
		} else {
			currentNode.tileType = LEVEL_WALL;
			currentNode.draw('black');
			return;
		}
		
		levelGrid[currentNode.index] = currentNode.tileType;
		
		for(var neighbour of currentNode.neighbours) {
			neighbour.parentTile = currentNode;
			if(neighbour.tileType == LEVEL_UNDEFINED && currentNode.tileType == LEVEL_GROUND) {
				neighbour.isOpen = true;
				openSet.push(neighbour);
			}
		}
		// Level
		drawCurrent();
	}
}

function finish() {
	searching = false;
	setGoal();
	drawLevel();
	startNode.draw('blue');
	goalNode.draw('red');
	
	levelGrid[goalNode.index] = LEVEL_UNDEFINED; // Same as End type in A*
	console.log("Done!");
}
/*
G: Score from start to this node. Increse by 10 for horizontal/vertical moves, increse 14 for diagonal (If included)
H: Score for remaining distanceance in straight line (Ignoring obstacles) Go diagonal until you hit row/column of target, then go straight
F: G + H. This is the score we look at to know which note to check next. Always check lowest. If equal, check lowest H
*/