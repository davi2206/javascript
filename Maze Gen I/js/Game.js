const LIVE = false;
const FPS = 144;
const LOCATE_MOUSE = false;

let canvas;
let canvasContext;

let searching = false;

let scripts = [
	"Inputs.js",
	"Level.js", 
	"Tile.js", 
	"sim.js",
	"UI.js"];
let commonScripts = ["draw.js"];
let scriptsToLoad = 0;

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	canvas.width = (window.innerWidth*0.8);
	canvas.height = (window.innerHeight*0.7);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/Mazes/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
}

function play() {
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('This is some sort of loading screen', canvas.width/10, canvas.height/3, 'blue', 80);
	
	gameReset();
	registerEvents();
	registerUIEvents();
	setInterval(function() {fpsLoop();}, 1000/FPS);
}

function gameReset() {
	var levelInitialized = initLevel();
	
	while(!levelInitialized) {
		console.log("Initializing level");
	}
	
	initSim();
	searching = true;
	drawLevel();
}

function fpsLoop() {
	step();
	
	findMouseTile();
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		colorText(mouseTileX+","+mouseTileY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
	
	//for(var i=0; i < openSet.length; i++) {
	//	var node = openSet[i];
	//	colorText(node.g, node.col*tileSize+5, node.row*tileSize+20, 'yellow', 15);
	//	colorText(node.h, node.col*tileSize+5, node.row*tileSize+40, 'yellow', 15);
	//}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

