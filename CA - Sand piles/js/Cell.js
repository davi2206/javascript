/*
 * Cell Class
*/

class Cell {
    constructor(pos, width, height, val) {
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.val = val;

        this.delta_val = 0;
        this.adjacent = [];
    }

    shed() {
        if(this.val >= 4) {
            this.delta_val -= 4;

            this.getAdjacent();
            for (let i = 0; i < this.adjacent.length; i++) {
                this.adjacent[i].delta_val++;
            }
        }
    }

    update() {
        this.val += this.delta_val;
        this.delta_val = 0;
    }

    getAdjacent() {
        this.adjacent = [];
        if(this.pos[0] > 0) {
            this.left_index = getIndex(this.pos[0]-1, this.pos[1]);
            this.adjacent.push(cells[this.left_index]);
        }

        if(this.pos[0] < cols-1) {
            this.right_index = getIndex(this.pos[0]+1, this.pos[1]);
            this.adjacent.push(cells[this.right_index]);
        }

        if(this.pos[1] > 0) {
            this.up_index = getIndex(this.pos[0], this.pos[1]-1);
            this.adjacent.push(cells[this.up_index]);
        }

        if(this.pos[1] < rows-1) {
            this.down_index = getIndex(this.pos[0], this.pos[1]+1);
            this.adjacent.push(cells[this.down_index]);
        }
    }

    draw() {
		colorRect(this.pos[0]*this.width, this.pos[1]*this.height, this.width, this.height, colors[this.val], 'black');
    }
}