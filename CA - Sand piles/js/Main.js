const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 2000;
const CANVAS_HEIGHT = 900;
const TITLE = "Sand Piles"
const FPS = 144;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js", "Cell.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let cols = 51;
let rows = 51;

let cells = [];

let width;
let height;

let colors = ['rgb(0,0,0)', 'rgb(255,0,0)', 'rgb(0,255,0)', 'rgb(0,0,255)', 'rgb(255,255,0)', 'rgb(255,0,255)', 'rgb(0,255,255)', 'rgb(255,255,255)'];

let center;

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	init();

	setInterval(function () {
		move();
		drawAll();
	}, 1000 / FPS);
}

function init() {
	cells = [];
	height = CANVAS_HEIGHT / rows;
	width = height;
	for (let y = 0; y < rows; y++) {
		for (let x = 0; x < cols; x++) {
			cells.push(new Cell([x, y], width, height, 0));
		}
	}

	center = cells[getIndex(Math.floor(cols/2), Math.floor(rows/2))];
	center.val = 10000;
}

function move() {
	result = cells.filter((cell) => cell.val >= 4);
	for (let i = 0; i < result.length; i++) {
		result[i].shed();
	}
	for (let i = 0; i < cells.length; i++) {
		cells[i].update();
	}

	titleH1.innerHTML = TITLE + ' ' + center.val;
}

function drawAll() {
	for (let i = 0; i < cells.length; i++) {
		cells[i].draw();
	}
}

function getValue(x, y) {
	let index;
	if (y > 0) {
		let sum = 0;
		index = getIndex(x, y - 1);
		let plusCols = (x == 0) ? cols : 0;
		let minusCols = (x == cols - 1) ? cols : 0;

		sum += (cells[index - 1 + plusCols].val * 4);
		sum += (cells[index].val * 2);
		sum += (cells[index + 1 - minusCols].val);

		return new Cell([x, y], width, height, key[sum], colors[sum]);
	}
}

function getIndex(x, y) {
	return (y * cols + x);
}
function getYfromIndex(i) {
	return Math.floor(i / cols);
}
function getXfromIndex(i) {
	return Math.floor(i % cols);
}


function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}