/*
	Cell Class
*/
class Cell {
	constructor(x, y, parent, siblings) {
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.siblings = siblings || [];
		this.canvasPos = this.getCanvasPos();
		this.index = this.y * maze_width + this.x;
		this.visited = false;
		this.validSiblings = [];
	}

	isRelated(cell) {
		if (this.parent && this.parent == cell) return true;
		if (cell.parent && cell.parent == this) return true;
		if (this.siblings && this.siblings.push(cell)) return true;
		if (cell.siblings && cell.siblings.push(this)) return true;
	
		return false;
	}

	draw(radius = 5, color = "black", fill = true, lineWidth = 5) {
		if(this.parent) drawLineTo([this.canvasPos.x, this.canvasPos.y], [this.parent.canvasPos.x, this.parent.canvasPos.y], "RED", path_size);
		this.siblings.forEach(element => {
			drawLineTo([this.canvasPos.x, this.canvasPos.y], [element.canvasPos.x, element.canvasPos.y], color, path_size);
		});

		colorCircle(this.canvasPos.x, this.canvasPos.y, radius, color, fill, lineWidth);
	}

	drawExtra(radius = 3, color = "pink", fill = true, lineWidth = 5) {
		colorCircle(this.canvasPos.x, this.canvasPos.y, radius, color, fill, lineWidth);
	}

	getCanvasPos() {
		let cell_width = CANVAS_WIDTH / maze_width;
		let cell_x = (cell_width * this.x) + (cell_width / 2);
	
		let cell_height = (CANVAS_WIDTH) / maze_height;
		let cell_y = (cell_height * this.y) + (cell_height / 2);
	
		return new Point(cell_x, cell_y);
	}

	getDirs() {
		let dirs = [];
	
		if (this.y > 0 && dir != "S") {
			dirs.push("N");
		}
		if (this.x > 0 && dir != "E") {
			dirs.push("W");
		}
		if (this.x < maze_width - 1 && dir != "W") {
			dirs.push("E");
		}
		if (this.y < maze_height - 1 && dir != "N") {
			dirs.push("S");
		}
		return dirs;
	}

	isValidSibling(cell, min_dist) {
		if(this.isRelated(cell)) return false;
		if(this.getDistance(cell) < min_dist) return false;

		return true;
	}

	getDistance(cell) {
		let parentDist = Number.MAX_SAFE_INTEGER;
		let siblingDist = Number.MAX_SAFE_INTEGER;
		if(this.isRelated(cell)) return 1;
		else {
			if(this.parent) parentDist = this.parent.getDistance(cell);
			// if(this.sibling) siblingDist = this.sibling.getDistance(cell);
			if(!this.parent && !this.sibling) return -1;
			return parentDist + 1;
		}
	}

	getTree() {
		let tree = [];
		let index = this.index;
	
		while (cells[index].parent) {
			tree.push(cells[index]);
			index = cells[index].parent.index;
		}
	
		return tree;
	}
}