let regen_button = document.getElementById("regen_button");
let gold_button = document.getElementById("gold_button");
let collectible_button = document.getElementById("collectible_button");
let json_button = document.getElementById("json_button");

function registerUIEvents() {
	regen_button.onclick = function() { 
		play();
	}

	gold_button.onclick = function() { 
		populate_gold();
	}

	collectible_button.onclick = function() { 
		populate_collectible();
	}

	json_button.onclick = function() { 
		generateTileMap();
	}
}

// function btn_reset_values() {
// //	slider.value = object_size;
// 	play();
// }