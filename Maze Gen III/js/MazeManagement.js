function generateCells() {
    for (let y = 0; y < maze_height; y++) {
        for (let x = 0; x < maze_width; x++) {
            let c = new Cell(x, y);
            cells.push(c);
            if((x == 0 || x == maze_width-1) && y > 0) c.siblings.push(cells[c.index-maze_width]);
            if(x > 0 && (y == 0 || y == maze_height-1)) c.siblings.push(cells[c.index-1]);
        }
    }
}

function scrambleMaze() {
    walkable = [...cells];
    let rand = random_floor(1,maze_width-2);
    c = cells[rand];
    dir = maze_width;

    buildWall(c, dir);
}

function buildWall(c, dir) {
    let next_dir = 0;
    if(Math.abs(dir) == maze_width) next_dir = 1;
    else next_dir = maze_width;

    let wall = [];
    wall.push(c);
    let next;
    let previus = c;

    while(true) {
        next = cells[previus.index+dir];
        wall.push(next);

        next.siblings.push(previus);
        if(next.visited || isEdge(next)) {
            wall.forEach(element => element.visited = true);

            let i = random_floor(1,wall.length-1);
            i = random_floor(1, wall.length-1);
            console.log(i);
            
            removeFromSet(wall[i-1], wall[i].siblings);

            
            if(wall.length > 2) {
                buildWall(wall[i], (next_dir*-1));
                buildWall(wall[i], next_dir);
            }
            return;
        }
        else {
            // next.parent = previus;
            next.visited = true;
            previus = next;
        }
    }
}

function isEdge(cell) {
    return (cell.x <= 0 || cell.x >= maze_width-1 || cell.y <= 0 || cell.y >= maze_height-1);
}



function generateLoops(min_length) {
    let bridges = 0;
    let potentials = [];

    cells.forEach(loopCell => {    
        // if(bridges > level) return;
        let dirs = getPlayerDirs(loopCell);

        dirs.forEach(dir => {
            switch (dir) {
                case "N":
                    index = loopCell.index - maze_width;
                    break;
                case "S":
                    index = loopCell.index + maze_width;
                    break;
                case "W":
                    index = loopCell.index - 1;
                    break;
                case "E":
                    index = loopCell.index + 1;
                    break;
            }
            if(loopCell.isValidSibling(cells[index], min_length)) {
                loopCell.validSiblings.push(cells[index]);
            }
        });
        if(loopCell.validSiblings.length > 0) potentials.push(loopCell);
    });

    while(bridges <= level) {
        let cell = potentials[random_floor(0, potentials.length-1)];
        if(!cell) return;
        let sibling = cell.validSiblings[random_floor(0, cell.validSiblings.length-1)];
        cell.sibling = sibling;
        sibling.sibling = cell;
        
        removeFromSet(cell, potentials);
        removeFromSet(sibling, potentials);
        bridges++;
    }
}

function populate_gold() {
    gold = [];
    let gold_count = cells.length/100 * gold_saturation;

    let potentials = [...cells];

    for (let index = 0; index < gold_count; index++) {
        let c = potentials[random_floor(0,potentials.length-1)];
        removeFromSet(c, potentials);
        gold.push(new Collectible(c.x, c.y, 4, 7, "#FFD700", "#FFBF00"));
    }

    drawAll();
}

function populate_collectible() {
    collectibles = [];

    let potentials = [...cells];

    for (let index = 0; index < collectible_count; index++) {
        let c = potentials[random_floor(0,potentials.length-1)];
        removeFromSet(c, potentials);
        collectibles.push(new Collectible(c.x, c.y, 2, 5, "RED", "PURPLE"));
    }

    drawAll();
}
