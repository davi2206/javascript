/*
	Collectible Class
*/
class Collectible {
	constructor(x, y, r, edges, edge_color, color) {
		this.x = x;
		this.y = y;
        this.r = r;
        this.edges = edges;
        this.edge_color = edge_color;
        this.color = color;
        this.canvasPos = this.getCanvasPos();
        this.cell_width = CANVAS_WIDTH / maze_width;
	}

    getCanvasPos() {
        this.cell_width = CANVAS_WIDTH / maze_width;
		let gold_x = (this.cell_width * this.x) + (this.cell_width / 2);
	
		let cell_height = (CANVAS_WIDTH) / maze_height;
		let gold_y = (cell_height * this.y) + (cell_height / 2);
	
		return new Point(gold_x, gold_y);
	}

    draw() {
        drawPolygon(this.canvasPos.x, this.canvasPos.y, this.cell_width/this.r, this.edges, new Vector(0,1), this.edge_color, this.color, 0.1);
	}
}