/*
    Node Class
*/
class Node {
	constructor(point_a, point_b, color = "black", width = 5) {
		this.point_a = point_a;
		this.point_b = point_b;
        this.color = color;
        this.width = width;
	}

    move(vector) {
        this.point_a.move_vector(vector);
        this.point_b.move_vector(vector);
    }

    get_length() {
        let a = this.point_b.x - this.point_a.x;
        let b = this.point_b.y - this.point_a.y;
        return Math.sqrt((a*a) + (b*b));
    }

    draw() {
        // console.log(this.point_a);
        // console.log(this.point_b);
        drawLineTo([this.point_a.x, this.point_a.y], [this.point_b.x, this.point_b.y], this.color, this.width);
        // drawLineTo([0,0], [500,500], 'white', 5);
	}

	copy() {
		return new Node(this.point_a, this.point_b);
	}
}