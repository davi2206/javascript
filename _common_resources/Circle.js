class Circle {
    constructor(x, y, r, color="RED") {
		this.x = x;
		this.y = y;
		this.r = r;
		this.color = color;
	}

	getRandomPoint() {
        let ang = random(0,2*Math.PI);

        let r = (Math.sqrt(random())*this.r);

        let x = Math.cos(ang)*r;
        let y = Math.sin(ang)*r;

        return new Point(x + this.x, y + this.y);
    }

    draw() {
        colorCircle(this.x, this.y, this.r, this.color, false, 2);
    }
}