/*
	Vector Class
*/
class Vector {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
	
	// Add (vector)
	add(vector){
		this.x += vector.x;
		this.y += vector.y;
	}
	
	// Subtract (vector)
	subtract(vector) {
		this.x -= vector.x;
		this.y -= vector.y;
	}
	
	// Mltiply (nr)
	mult(nr) {
		this.x *= nr;
		this.y *= nr;
	}
	
	// Divide (nr)
	div(nr) {
		this.x /= nr;
		this.y /= nr;
	}
	
	// Get magnitude ()
	get_magnitude() {
		var a2 = this.x * this.x;
		var b2 = this.y * this.y;
		var c2 = a2 + b2;
		
		return Math.sqrt(c2);
	}
	
	// Get Radian angle ()
	get_ang_rad() {
		return Math.atan2(this.y, this.x);
	}
	
	// Get Degree angle ()
	get_ang_deg() {
		let deg = (this.get_ang_rad() * 180 / Math.PI);

		if(deg < 0) deg = deg + 360;

		return deg;
	}
	
	// Dot product (vector)
	dot(vector) {
		return this.x * vector.x + this.y * vector.y;
	}
	
	// ExtendPct (%)
	extend_pct(pct) {
		let new_mag = this.get_magnitude() / 100 * (100 + pct);
		this.set_magnitude(new_mag);
	}
	
	// ExtendDist (distance)
	extend_dist(dist) {
		let new_mag = this.get_magnitude() + dist;
		this.set_magnitude(new_mag);
	}
	
	// Set magnitude (distance)
	set_magnitude(new_mag) {
		let mag = this.get_magnitude();
		this.div(mag); // Divide by current magnitude for Unit vector
		this.mult(new_mag); // Multiply Unit vector by new magnitude
	}
	
	// Turn Radian (radian)
	turn_rad(rad) {
		let cos = round(Math.cos(rad),25);
		let sin = round(Math.sin(rad),25);
		this.x = (this.x * cos) - (this.y * sin);
		this.y = (this.x * sin) + (this.y * cos);
	}
	
	// Turn Degree (degree)
	turn_deg(deg) {
		if(deg == 0) {
			return;
		}
		// let rad = (deg * Math.PI / 180);
		// console.log(rad);
		// this.turn_rad(rad);

		let curDeg = this.get_ang_deg();
		let newDeg = curDeg + deg;
		this.set_ang_deg(newDeg);
	}

	// Set angle (radian)
	set_ang_rad(rad) {
		let mag = this.get_magnitude();
		
		this.x = Math.cos(rad);
		this.y = Math.sin(rad);

		this.set_magnitude(mag);
	}

	// Set angle (degree)
	set_ang_deg(deg) {
		let rad = deg / 180 * Math.PI;
		this.set_ang_rad(rad);
	}

	// Add vector. but keep magnitude
	turn_vector(vector) {
		let mag = this.get_magnitude();
		this.add(vector);
		this.set_magnitude(mag);
	}

	limit(magnitudeLimit) {
		if(this.get_magnitude() > magnitudeLimit) this.set_magnitude(magnitudeLimit);
	}

	draw(start_point, color = "black", width = 5) {
		drawVector(start_point, this, color, width);
	}

	copy() {
		return new Vector(this.x, this.y);
	}
}
