/*
	Particle Class
*/
class Particle {
	constructor(x, y, r = 50, vector = new Vector(0, 0), color = OBJECT_COLOR, fadePct = 0, gravity = new Vector(0, 0), dieOutsideCanvasNSEW = [true, true, true, true], minSize = 0.1) {
		this.position = new Vector(x, y);
		this.r = r;
		this.vector = vector;
		this.color = color;
		this.fadePct = fadePct;
		this.gravity = gravity;
		this.dieOutsideCanvas = dieOutsideCanvasNSEW;
		this.minSize = minSize;

		this.live = true;
	}

	move() {
		// Update position from vector
		this.position.add(this.vector);

		// Update Vector with gravity
		this.vector.add(this.gravity);

		if (this.dieOutsideCanvas.includes(true)) {
			this.live = this.checkParticleLife();
		}

		// Fade, if needed
		if (this.fadePct != 0) this.fade(this.fadePct);
	}

	draw() {
		// Common drawCircle
		colorCircle(this.position.x, this.position.y, this.r, this.color);
	}

	fade(rate = 1) {
		let multiplier = 100 - rate;
		let size = this.r * multiplier / 100;
		this.r = size;

		if (this.r < this.minSize)
			this.live = false;
	}

	drawVectorLine(length, color = "black", width = 5) {
		let start = [this.position.x, this.position.y];
		let end = [this.position.x + this.vector.x * length, this.position.y + this.vector.y * length];
		drawLineTo(start, end, color, width)
	}

	checkParticleLife() {
		// N
		if (this.dieOutsideCanvas[0] && this.position.y < 0) return false;
		// S
		if (this.dieOutsideCanvas[1] && this.position.y > CANVAS_HEIGHT) return false;
		// E
		if (this.dieOutsideCanvas[2] && this.position.x > CANVAS_WIDTH) return false;
		// W
		if (this.dieOutsideCanvas[3] && this.position.x < 0) return false;

		return true;
	}

	getDistFromPoint(point) {
		let tmpX = this.position.x - point[0];
		let tmpY = this.position.y - point[1];
		let tmpVector = new Vector(tmpX, tmpY);

		return tmpVector.get_magnitude();
	}

	isOverlapping(particle) {
		let dist = this.getDistFromPoint([particle.position.x, particle.position.y]);
		return (dist < (this.r + particle.r));
	}

	isOverlappingAny(particles) {
		console.log(particles);
		for (let i = 0; i < particles.length; i++) {
			console.log(this);
			if (particles[i] == this) return;
			if (this.isOverlapping(particles[i])) return true;
		}
		return false;
	}

	copy() {
		return new Particle(
			this.position.x,
			this.position.y,
			this.r,
			this.vector.copy(),
			this.color,
			this.fadePct,
			this.gravity.copy(),
			this.dieOutsideCanvasNSEW,
			this.minSize);
	}
}
