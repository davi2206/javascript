
class Vehicle {
	constructor(pos, vector, wrapEdges, color, maxSpeed, maxForce, size, nodes) {
		canvas = document.getElementById('gameCanvas');
		this.pos = pos || new Point(random(0, canvas.width), random(0, canvas.height));
		this.velocity = vector || new Vector(random(0, 10), random(0, 10));
		this.wrapEdges = wrapEdges == null ? true : wrapEdges;
		this.color = color || getRandColor();
		this.maxSpeed = maxSpeed || random(1, 10);
		this.maxForce = maxForce || random(1, 10);
		this.size = size || random(1, 25);
		this.nodes = nodes || random(3, 25);

		this.desiredVector = new Vector(0, 0);
		this.drawDesiredVelocity = new Vector(0, 0);

		this.debugMultiplier = 5;

		this.targetVehicles = [];
		this.pursuingVehicles = [];
		this.fleePoints = [];
		this.targetPos = null;
		this.doWander = false;

		// Wander Values
		/*
		v = Velocity * x
		tp = Target point (At end of Vel*x)
		wp = Wanter point. 
		s = Vector from TP to WP. Added to v as steering?
		sa = angle of s, relative to v
		dl = magnitude of displacement vector
		d = Random vector, of magnitude dl
		dp = point


		ang = angle of s, starts as 0
		v = vel * x
		s = v.copy()
		s.turn_deg(ang)
		 */
	}

	move() {
		// Seek
		if (this.targetPos) {
			this.velocity.add(this.seek(this.targetPos));
		}
		
		// Pursue
		this.targetVehicles.forEach(vehicle => 
			this.velocity.add(this.pursue(vehicle)));

		// Flee
		this.pursuingVehicles.forEach(vehicle => 
			this.velocity.add(this.flee(vehicle)));
		
		// Flee Points
		this.fleePoints.forEach(point => 
			this.velocity.add(this.fleePoint(point)));

		// Wander
		if (this.doWander) {
			this.wander();
		}

		this.drawDesiredVelocity = this.velocity.copy();

		this.velocity.limit(this.maxSpeed);
		this.pos.move_vector(this.velocity);
		if (this.wrapEdges) this.edges();
	}

	seek(targetPos) {
		// Vector from Vehicle to target
		let desiredVector = new Vector(targetPos.x - this.pos.x, targetPos.y - this.pos.y);
		
		// Limit to max speed
		desiredVector.limit(this.maxSpeed);
		desiredVector.subtract(this.velocity);

		desiredVector.limit(this.maxForce);

		return desiredVector;
	}

	pursue(vehicle) {
		let prediction = vehicle.pos.copy();
		let predictionVel = vehicle.velocity.copy();
		predictionVel.mult(10);
		prediction.move_vector(predictionVel);
		return this.seek(prediction);
	}

	flee(targetVehicle) {
		return this.fleePoint(targetVehicle.pos);
	}

	fleePoint(pos) {
		let force = this.seek(pos);
		let newMagnitude = this.maxSpeed - force.get_magnitude();
		force.set_magnitude(newMagnitude);
		force.mult(-1);
		return force;
	}

	wander() {
		let velocityMultiplier = 10;
		let sRadius = 50;
		let dRadius = 10;
		
		let v = this.velocity.copy();
		v.mult(velocityMultiplier);
		let tp = this.pos.copy();
		tp.move_vector(v);
		
		let s = v.copy();
		s.set_magnitude(sRadius);
		
		let wp = new Point(tp.x + s.x, tp.y + s.y);

		let d = new Vector(random(-dRadius, dRadius), random(-dRadius, dRadius));
		d.set_magnitude(d.dRadius);

		let dp = new Point(wp.x+d.x, wp.y+d.y);

		let newWPvec = new Vector(dp.x - tp.x, dp.y - tp.y);
		newWPvec.set_magnitude(sRadius);

		wp = new Point(tp.x + newWPvec.x, tp.y + newWPvec.y);

		this.targetPos = new Point(0, 0);
	}

	edges() {
		if (this.pos.x > canvas.width) {
			this.pos.x = 0;
		}
		if (this.pos.y > canvas.height) {
			this.pos.y = 0;
		}
		if (this.pos.x < 0) {
			this.pos.x = canvas.width;
		}
		if (this.pos.y < 0) {
			this.pos.y = canvas.height;
		}
	}

	draw() {
		drawPolygon(this.pos.x, this.pos.y, this.size, this.nodes, this.velocity, this.color, this.color);

		// Debug lines
		if (this.color == "green") {
		    let drawVelocity = this.velocity.copy();
		    drawVelocity.mult(this.debugMultiplier);
		    drawVelocity.draw(this.pos, "green", 2);
		    this.drawDesiredVelocity.draw(this.pos, "red", 2);
		}
		if (this.targetPos) {
			this.targetPos.draw(5, "green", true);
		}
	}
}