/*
	Point Class
*/
class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

    get_dist_to(point) {
		let xDif = (point.x - this.x);
		let yDif = (point.y - this.y);
		return Math.sqrt((xDif * xDif) + (yDif * yDif));
    }

	move_vector(vector) {
		this.move_x(vector.x);
		this.move_y(vector.y);
	}

	move_x(x) {
		this.x += x;
	}

	move_y(y) {
		this.y += y;
	}

	draw(radius = 5, color = "black", fill = true, lineWidth = 5) {
		colorCircle(this.x, this.y, radius, color, fill, lineWidth);
	}
	copy() {
		return new Point(this.x, this.y);
	}
}