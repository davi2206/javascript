function drawLineTo(start, end, stroke = 'black', width = 5) {
	canvasContext.strokeStyle = stroke;
	canvasContext.lineWidth = width;

	canvasContext.beginPath();
	canvasContext.moveTo(...start);
	canvasContext.lineTo(...end);
	canvasContext.stroke();
}

function drawVector(start_point, vector, color = "black", width = 5) {
	let start = [start_point.x, start_point.y];
	let end = [start_point.x + vector.x, start_point.y + vector.y];

	drawLineTo(start, end, color, width);
}

function drawLineDirection(start, angDeg, length, stroke = 'black', width = 5, drawStartPoint = true) {
	if (stroke) {
		canvasContext.strokeStyle = stroke;
	}

	var angRad = getRadOfDeg(angDeg);

	var x = Math.cos(angRad) * length + start[0];
	var y = Math.sin(angRad) * length + start[1];

	canvasContext.beginPath();
	canvasContext.moveTo(...start);
	canvasContext.lineTo(x, y);
	canvasContext.stroke();

	if (drawStartPoint) {
		colorCircle(start[0], start[1], width, stroke);
	}
}

function colorRect(x, y, width, height, fillColor, strokeColor = '') {
	canvasContext.fillStyle = fillColor;

	if (strokeColor == '') canvasContext.strokeStyle = fillColor;
	else canvasContext.strokeStyle = strokeColor;

	canvasContext.beginPath();
	canvasContext.rect(x, y, width, height);
	canvasContext.stroke();
	canvasContext.fill();
}

function colorCircle(x, y, r, color, fill = true, lineWidth = 5) {
	canvasContext.fillStyle = color;
	canvasContext.strokeStyle = color;
	canvasContext.lineWidth = lineWidth;
	canvasContext.beginPath();
	canvasContext.arc(x, y, r, 0, Math.PI * 2, true);

	if (fill) {
		canvasContext.fill();
	}
	else {
		canvasContext.stroke();
	}
}

function colorX(x, y, size, color = 'black') {
	canvasContext.strokeStyle = color;
	canvasContext.beginPath();

	canvasContext.moveTo(x - size, y - size);
	canvasContext.lineTo(x + size, y + size);

	canvasContext.moveTo(x + size, y - size);
	canvasContext.lineTo(x - size, y + size);
	canvasContext.stroke();
}

function colorText(words, posX, posY, color = 'black', size = 25) {
	canvasContext.font = 'normal ' + size + 'px Arial';
	canvasContext.fillStyle = color;
	canvasContext.fillText(words, posX, posY);
}

function drawImgCentRot(img, x, y, ang = 0, w = 0, h = 0) {
	canvasContext.save();
	canvasContext.translate(x, y);
	canvasContext.rotate(ang);

	if (w == 0) {
		w = img.width;
	}
	if (h == 0) {
		h = img.height;
	}

	canvasContext.drawImage(img, -w / 2, -h / 2, w, h);
	canvasContext.restore();
}

/* x, y, radius, nr of sides, direction vector */
function drawPolygon(inX, inY, r, nodes, inDirection = new Vector(0,1), strokeColor = "black", fillColor = "", lineWidth = .1) {
	canvasContext.strokeStyle = strokeColor;
	canvasContext.fillStyle = fillColor;
	canvasContext.lineWidth = lineWidth;
	
	// You can't draw a shape with less than 3 lines
	if(nodes < 3) {
		nodes = 3;
		console.error("Polygons need 3 or more sides");
	}

	canvasContext.beginPath();

	let angle = ((180 * (nodes - 2)) / nodes); // Interior angle
	let nodeLen = 2 * r * Math.sin(Math.PI / nodes); // Length of nodes
	let direction = inDirection.copy();
	if(direction.x == 0 && direction.y == 0) direction.x = 1;

	// Move pen r in direction
	direction.set_magnitude(r);
	let x = inX + direction.x;
	let y = inY + direction.y;
	canvasContext.moveTo(x, y);

	// Turn to first node heading
	direction.turn_deg(180 - angle / 2);
	direction.set_magnitude(nodeLen);

	for (let i = 0; i < nodes; i++) {
		// Draw first node
		x += direction.x;
		y += direction.y;
		canvasContext.lineTo(x, y);

		// Turn angle
		direction.turn_deg(180-angle);
	}

	// Draw outline
	canvasContext.stroke();
	// If fillColor, fill in
	fillColor != "" ? canvasContext.fill() : null;
}


/* x, y, radius, nr of sides, direction vector */
function getPolygonPoints(inX, inY, r, nodes, inDirection = new Vector(0,1)) {
	// You can't have a shape with less than 3 lines
	if(nodes < 3) {
		nodes = 3;
		console.error("Polygons need 3 or more sides");
	}

	let angle = ((180 * (nodes - 2)) / nodes); // Interior angle
	let nodeLen = 2 * r * Math.sin(Math.PI / nodes); // Length of nodes
	let direction = inDirection.copy();
	if(direction.x == 0 && direction.y == 0) direction.x = 1;

	// Move pen r in direction
	direction.set_magnitude(r);
	let x = inX + direction.x;
	let y = inY + direction.y;

	// Turn to first node heading
	direction.turn_deg(180 - angle / 2);
	direction.set_magnitude(nodeLen);

	let points = [];
	// First point
	points.push(new Point(x, y));

	for (let i = 0; i < nodes; i++) {
		// Draw first node
		x += direction.x;
		y += direction.y;

		points.push(new Point(x, y));

		// Turn angle
		direction.turn_deg(180-angle);
	}
	return points;
}