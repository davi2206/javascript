/*
	FUNCTIONS FOR VECTOR MATH
*/

/// TODO: Change to calculate with actual Vactor objects

var rad = 0;

var x = 0;
var y = 0;
var xn = 0;
var yn = 0;

// !!Deprecated
function vector_add(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]+v2[0];
	vector[1] = v1[1]+v2[1];
	
	return vector;
}

// !!Deprecated
function vector_sub(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]-v2[0];
	vector[1] = v1[1]-v2[1];
	
	return vector;
}

// !!Deprecated
function vector_mult(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]*nr;
	vector[1] = v1[1]*nr;
	
	return vector;
}

// !!Deprecated
function vector_div(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]/nr;
	vector[1] = v1[1]/nr;
	
	return vector;
}

// Get Average Vector
function vector_avg(vectors) {
	var vector = [0,0];
	var magnitude = 0;
	aSq = 0;
	bSq = 0;


	for (var i = 0; i < vectors.length; i++) {
		vector = vector_add(vector, vectors[i]);
		aSq = Math.pow(vectors[i][0], 2);
		bSq = Math.pow(vectors[i][1], 2);

		magnitude += Math.sqrt(aSq + bSq)
	}
	
	vector = vector_div(vector, vectors.length);
	magnitude = magnitude / vectors.length;

	return vector_set_magnitude(vector, magnitude);
}

// Get Average Magnitude
function vector_avg_mag(vectors) {
	// TODO
}

// !!Deprecated
function vector_ang(v) {
	var ang = Math.atan2(v[1],v[0]);
	return ang;
}

// !!Deprecated
function vactor_extend(v, pct) {
	var magnitude = vector_magnitude(v);
	var extension = magnitude / 100 * pce;
	var new_mag = magnitude + extension;
}

// !!Deprecated
// Add the direction of vector 2 to vector 1, but keep vector 1's velocity
function vector_ajust(v1, v2) {
	var new_vector = vector_add(v1, v2);
	var vector_velocity = Math.sqrt(v1[0]^2+v1[1]^2);
	var new_vector_velocity = Math.sqrt(new_vector[0]^2+new_vector[1]^2);
	var magnification = new_vector_velocity / vector_velocity;
	
	var final_vector = [new_vector_velocity / magnification, new_vector_velocity / magnification];
	return final_vector;
}

// !!Deprecated
function vector_set_magnitude(vector, magnitude) {
	var v_magnitude = vector_magnitude(vector);
	var vector_one = vector_div(vector, v_magnitude);
	var vector_ajusted = vector_mult(vector_one, magnitude);
	return vector_ajusted;
}

// !!Deprecated
function vector_turn(v, deg) {
	if(deg == 0) {
		return v;
	}
	
	var rad = deg * Math.PI / 180;
	
	var x = v[0];
	var y = v[1];
	var cos = round(Math.cos(rad),10);
	var sin = round(Math.sin(rad),10)
	var xn = (x * cos) - (y * sin);
	var yn = (x * sin) + (y * cos);
	
	var nv = [xn,yn];
	
	return nv;
}

// !!Deprecated
function vector_magnitude(vector) {
	var a = vector[0];
	var b = vector[1];
	var a2 = a*a;
	var b2 = b*b;
	var c2 = a2 + b2;
	
	var magnitude = Math.sqrt(c2);
	return magnitude;
}

function vector_from_ang(ang) {
	var rad = ang / 180 * Math.PI;
	var vector = [0,0];
	vector[0] = Math.cos(rad);
	vector[1] = Math.sin(rad);
	
	return vector;
}