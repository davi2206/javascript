/**
 * Loads of color functions
 */

/** NOT WORKING */
function rgbaToHex(r, g, b, a) {
    rStr = (r).toString(16);
	gStr = (g).toString(16);
	bStr = (b).toString(16);
	aStr = (a).toString(16);

	if (rStr.length == 1) rStr = "0" + rStr;
	if (gStr.length == 1) gStr = "0" + gStr;
	if (bStr.length == 1) bStr = "0" + bStr;
	if (aStr.length == 1) bStr = "0" + bStr;

    return `#${rStr}${gStr}${bStr}${aStr}`;
}

function hexToRGBA(hex) {
    return {
        r: 0,
        g: 0,
        b: 0,
        a: 1,
    }
}

function getNextRainbowColor(r, g, b, rgbMin = 0, rgbMax = 255) {
    // Only accept "rainbow colors"
    if(r > rgbMin && g > rgbMin && b > rgbMin) throw new Error("Cannot get next from non-rainbow color");
    if(r < rgbMax && g < rgbMax && b < rgbMax) throw new Error("Cannot get next from non-rainbow color");
    
    // Ajust values
	if(r >= rgbMax && g <= rgbMin && b > rgbMin) b--;
	if(r < rgbMax && g <= rgbMin && b >= rgbMax) r++;
	if(r <= rgbMin && g > rgbMin && b >= rgbMax) g --;
	if(r <= rgbMin && g >= rgbMax && b < rgbMax) b++;
	if(r > rgbMin && g >= rgbMax && b <= rgbMin) r--;
    if(r >= rgbMax && g < rgbMax && b <= rgbMin) g++;

    // Constrain values to min and max (Move this to common function)
    r = r < rgbMin ? rgbMin : r;
    g = g < rgbMin ? rgbMin : g;
    b = b < rgbMin ? rgbMin : b;
    r = r > rgbMax ? rgbMax : r;
    g = g > rgbMax ? rgbMax : g;
    b = b > rgbMax ? rgbMax : b;

    // Return RGB values
    return {
        r: r,
        g: g,
        b: b,
    }
}

function getRainbowArray(rgbMin = 0, rgbMax = 255) {
    let r = rgbMax;
    let g = b = rgbMin;
    let colors = [];

    let count = 0;
    colors.push(`rgba(${r},${g},${b},1)`);

    while(count < ((rgbMax)*6-1)) {
        let rgb = getNextRainbowColor(r, g, b, rgbMin, rgbMax);
        r = rgb.r;
        g = rgb.g;
        b = rgb.b;
        colors.push(`rgba(${r},${g},${b},1)`);
        count++;
    }

    return colors;
}