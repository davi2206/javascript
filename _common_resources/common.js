function getMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouseX = evt.clientX - rect.left - root.scrollLeft;
	mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function removeFromSet(object, set) {
	for(var i = set.length-1; i>=0; i--) {
		if(set[i] == object) {
			set.splice(i, 1);
		}
	}
}

function getRadOfDeg(deg) {
	while(deg > 360) {
		deg -= 360;
	}
	while(deg < 0) {
		deg += 360;
	}
	return (Math.PI * deg / 180)
}

function random(low=0, high=1) {
	var dif = high-low;
	var rand = Math.random()*dif;
	var adjusted = rand+low;
	return adjusted;
}

function random_floor(min, max) {
	return Math.floor(random(min, max));
}

function getRandColor(colorArray = []) {
	if(colorArray.length == 0) {
		colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D', '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC','#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC','#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399','#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680','#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933','#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
	}
	
	var rand = Math.round(random(0, colorArray.length-1));
	return colorArray[rand];
}

function getRandColorHex() {
	let randColor = random_floor(0, 16777215);
	return "#"+randColor.toString(16);
}

function round(nr, dec) {
	dec = Math.pow(10, dec);
	return Math.floor(nr*dec)/dec;
}

function map(nr, in_min, in_max, out_min = 0, out_max = 100) {
	return (nr - in_min) / (in_max - in_min) * (out_max - out_min) + out_min;
}

function getRandPointInCircle(xPos, yPos, radius) {
	let ang = random(0,2*Math.PI);

	let r = (Math.sqrt(random())*radius);

	let x = Math.cos(ang)*r;
	let y = Math.sin(ang)*r;

	return [x + xPos, y + yPos];
}






// Loop over and load scripts
function loadScripts(scripts = [], basePath = '', commonScripts = [], commonBasePath = '') {
	scriptsToLoad = scripts.length + commonScripts.length;
	
	for(var i = 0; i < commonScripts.length; i++) {
		var path = commonBasePath+commonScripts[i];
		loadScript(path);
	}

	for(var i = 0; i < scripts.length; i++) {
		var path = basePath+scripts[i];
		loadScript(path);
	}
}

// Load External JS
function loadScript(url){
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Other browsers
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function callback() {
	scriptsToLoad--;
	
	if(scriptsToLoad == 0) {
		play();
	}
}

function loadCanvas(width, height, color = 'white', showError = true) {
	parentCanvas = document.getElementById('parentCanvas');
	canvas = document.getElementById('gameCanvas');
	canvas.addEventListener("click", getMousePos);
	canvasContext = canvas.getContext('2d');
	
	if(showError) {
		colorText('If you see this, something broke!', canvas.width/10, canvas.height/3, 'red', 80);
	}

	parentCanvas.width = width;
	parentCanvas.height = height;
	canvas.width = width;
	canvas.height = height;
}