/*
	Pendulum Class
*/
class Pendulum {
	constructor(anchorPoint, vector, speed = 1, connectedPendulum = null, color = "black", width = 2) {
		this.anchorPoint = anchorPoint;
		this.vector = vector;
		this.speed = speed;
        this.connectedPendulum = connectedPendulum;
        this.color = color;
        this.width = width;
	}
	
	getAnchorPoint() {
		return this.anchorPoint;
	}

    setAnchorPoint(point) {
        this.anchorPoint = point;
    }

    getEndPoint() {
        let endX = this.anchorPoint.x + this.vector.x;
        let endY = this.anchorPoint.y + this.vector.y;
        return new Point(endX, endY);
    }

    getLength() {
        return this.vector.get_magnitude();
    }

    move(vector) {
        let newX = this.anchorPoint.x + vector.x;
        let newY = this.anchorPoint.y + vector.y;
        this.setAnchorPoint(new Point(newX, newY));
    }

    turn() {
        this.vector.turn_deg(this.speed);
    }

	draw() {
		drawVector(this.anchorPoint, this.vector, this.color, this.width);
	}

	copy() {
		return new Pendulum(this.anchorPoint, this.vector);
	}
}
