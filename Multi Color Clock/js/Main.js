const LIVE = false;
const CANVAS_COLOR = '#333';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Multiple Pendulum"
const FPS = 60;

const COLORS = ['rgba(255, 0, 0, 0.01)', 'rgba(0, 255, 0, 0.03)', 'rgba(0, 0, 255, 0.06)'];
//const COLORS = ['rgba(255, 0, 0, 1)', 'rgba(0, 255, 0, 1)', 'rgba(0, 0, 255, 1)'];
const OBJECT_WIDTH = 4;
let colorIndex = 0;

let scripts = ["Inputs.js", "UI.js", "Pendulum.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let frames = 1;
let pendulumMS;
let pendulumS;
let pendulumM;
let pendulumH;
let pendulumD;
let pendulumW;

let pendulums = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	registerEvents();
	registerUIEvents();
	runSim();
}

function runSim() {
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
//								anchorPoint, vector, speed = 1, connectedPendulum = null, color = "black", width = 2
	let center = new Point(CANVAS_WIDTH/2, CANVAS_HEIGHT/2);
	pendulumMS = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT*0.49), ((360/FPS)+0.3), null, 'rgba(255, 0, 0, 0.03)', OBJECT_WIDTH/4);
	pendulumS = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT*0.45), ((360/60)/FPS), null, 'rgba(0, 0, 255, 0.02)', OBJECT_WIDTH/3);
	pendulumM = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT*0.4), ((360/60/60)/FPS), null, 'rgba(0, 255, 0, 0.01)', OBJECT_WIDTH/2);
	// pendulumH = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT/5), (360/60), null, 'YELLOW', OBJECT_WIDTH);
	// pendulumD = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT/6), (360/60), null, 'PINK', OBJECT_WIDTH*2);
	// pendulumW = new Pendulum(center, new Vector(0, -CANVAS_HEIGHT/7), (360/60), null, 'PURPLE', OBJECT_WIDTH*3);
	
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	// if(frames % 1 == 0) pendulumMS.turn();
	// if(frames % 2 == 0) pendulumS.turn();
	// if(frames % 4 == 0) pendulumM.turn();
	// if(frames % 8 == 0) pendulumH.turn();
	// if(frames % 16 == 0) pendulumD.turn();
	pendulumMS.turn();
	pendulumS.turn();
	pendulumM.turn();
	// pendulumH.turn();
	// pendulumD.turn();

	titleH1.innerHTML = TITLE + ' f: ' + (Math.floor(frames/FPS));
	frames++;
}

function drawAll() {
	// Canvas
	//colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);

	pendulumMS.draw();
	pendulumS.draw();
	pendulumM.draw();
	// pendulumH.draw();
	// pendulumD.draw();
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}