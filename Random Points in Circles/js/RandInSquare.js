

class RandomInSquare {
    constructor() { }
    
    getPoint(circle) {
        let center = new Point(circle.x, circle.y);
        let point = null;
        let dist = CANVAS_WIDTH;
        let posX = 0;
        let posY = 0;
    
        while(true) {
            posX = random(circle.x-circle.r, circle.x+circle.r);
            posY = random(circle.y-circle.r, circle.y+circle.r);
            point = new Point(posX, posY);
            dist = point.get_dist_to(center);
            if(dist && circle.r && dist < circle.r) return point;
        }
    }
}