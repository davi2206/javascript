


class Circle {
    constructor(x, y, r, algorithm, color="RED") {
		this.x = x;
		this.y = y;
		this.r = r;
		this.algorithm = algorithm;
		this.color = color;
	}

    draw() {
        colorCircle(this.x, this.y, this.r, this.color, false, 2);
    }
}