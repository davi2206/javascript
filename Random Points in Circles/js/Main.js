const LIVE = false;
const CANVAS_COLOR = 'black';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 900;
const TITLE = "Project Title"
const FPS = 1000;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "RandInSquare.js", "RandRadiusDegree.js", "InverseTransformSample.js", "Triangles.js", "Circle.js"];
let commonScripts = ["draw.js", "Point.js"];
var scriptsToLoad = 0;

// Sketch specific parameters
let r = (CANVAS_HEIGHT * 0.49);
let circles = [];

let time = 0;
let benchmark = 1000000000;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);

	// Circles
	circles.push(new Circle(CANVAS_WIDTH/2, CANVAS_HEIGHT/2, r+5, new RandomInSquare(), "RED"));
	circles.push(new Circle(CANVAS_WIDTH/2, CANVAS_HEIGHT/2, r, new RandRadiusDegree(), "GREEN"));
	circles.push(new Circle(CANVAS_WIDTH/2, CANVAS_HEIGHT/2, r-5, new InverseTransformSample(), "BLUE"));
	circles.push(new Circle(CANVAS_WIDTH/2, CANVAS_HEIGHT/2, r-10, new Triangles(), "YELLOW"));

	circles.forEach(c => {
		c.draw();
	});

	loadImages();
	registerEvents();
}

function runSim() {
	setInterval(function() {
		generatePoints();
	}, 1000/FPS);
}

function generatePoints() {
	circles.forEach(c => {
		c.algorithm.getPoint(c).draw(1, c.color, true, 1);
	});
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}
