const LIVE = false;
const CANVAS_COLOR = 'rgb(75, 75, 75)';
const CANVAS_WIDTH = 1000;
const CANVAS_HEIGHT = 1000;
const TITLE = "Brownian Tree Snowflake"
const FPS = 100;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js", "Vector.js", "Particle.js", "Point.js", "Rainbow.js"];
var scriptsToLoad = 0;

let active = true;

let object_size = 2;
let max_turn = 2;
let speed = 1;

let branches = 12;

let particle;
let particles = [];
let rainbow;

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);
	canvasContext.translate(CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2);

	particle = createParticle();
	rainbow = getRainbowArray(220, 255);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function () {
		if (active) {
			moveAll();
			drawAll();
		}
	}, 1000 / FPS);
}

function moveAll() {
	while (!overlap(particle, particles) && particle.position.x > object_size * 2) {
		let turn = random(-max_turn, max_turn);
		particle.position.y += turn;
		particle.move();

		// console.log(particle.vector.get_ang_rad());
		let divider = 0.3;
		let offset = 0;

		if (particle.position.y < (-particle.position.x / divider) - offset) {
			particle.position.y = (-particle.position.x / divider) - offset;
			particle.vector.y *= -1;
		}
		if (particle.position.y > (particle.position.x / divider) + offset) {
			particle.position.y = (particle.position.x / divider) + offset;
			particle.vector.y *= -1;
		}
		if(particle.vector.y != 0) console.log("ERROR");

		// Randomly turn a bit
		// If pos.y < 0 || pos.y > (some y val), ajust
	}  {

		particles.push(particle.copy());
		particle = createParticle();
	}
}

function createParticle() {
	return new Particle((CANVAS_WIDTH / 2 + object_size * 2), 0, object_size, new Vector(-speed, 0), OBJECT_COLOR);
}

function overlap(particle, _particles) {
	for (let i = 0; i < _particles.length; i++) {
		if (particle.getDistFromPoint([_particles[i].position.x, _particles[i].position.y]) < (object_size * 2)) {
			if (particle.position.x >= CANVAS_WIDTH / 2 || particle.position.y >= CANVAS_HEIGHT / 2) active = false;
			return true;
		}
	}
	return false;
}

function drawAll() {
	// Canvas
	colorRect(-CANVAS_WIDTH / 2, -CANVAS_HEIGHT / 2, CANVAS_WIDTH, CANVAS_HEIGHT, CANVAS_COLOR);

	console.log(rainbow.length);
	for (let i = 0; i < branches; i++) {
		canvasContext.rotate(2 * Math.PI / branches);
		particle.draw();

		for(let i = 0; i < particles.length; i++) {
			let colIndx = Math.round(map(i, 0, particles.length-1, 0, rainbow.length-1));
			
			particles[i].color = rainbow[colIndx];
			particles[i].draw();
			new Particle(particles[i].position.x, -particles[i].position.y, object_size, particles[i].vector, rainbow[colIndx]).draw();
		}
	}
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}
