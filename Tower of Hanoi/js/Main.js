const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Project Title"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let object_size = 50;
let done = 0;

let noteNrs = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	// setInterval(function() {
	// 	moveAll();
	// 	drawAll();
	// }, 1000/FPS);
}

function moveAll() {
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	
}

function solveHanoi(n, source, destination, auxiliary) {
	if (n == 1) {
		console.log(`Move disk 1 from rod ${source} to rod ${destination}`);
		noteNrs.push(1);
		return;
	}
	solveHanoi(n-1, source, auxiliary, destination);
	console.log(`Move disk ${n} from rod ${source} to rod ${destination}`);
	noteNrs.push(n);
	solveHanoi(n-1, auxiliary, destination, source);
}

function playNotes() {
	// AI generated Audio Code
	let audioContext = new (window.AudioContext || window.webkitAudioContext)();
	let tempo = 120; // Beats per minute
	let noteLength = 0.15; // Length of each note in beats
	//let notes = [261.63, 329.63, 392, 523.25, 392, 329.63, 261.63]; // Frequencies of the notes in Hz
	let notes = [261.63, 293.66, 311.13, 349.23, 392, 415.3, 466.16]; // Frequencies of the notes in Hz
	let currentTime;
	let oscillator;
	
	// Schedule the notes to play at the correct time
	noteNrs.forEach((element, index) => {
		console.log(index, element, notes[element]);
		currentTime = audioContext.currentTime + index * (noteLength / tempo) * 60;
		oscillator = audioContext.createOscillator();
		oscillator.frequency.value = 440 * Math.pow(2, ((element-5)/12)); //notes[element];
		oscillator.connect(audioContext.destination);
		oscillator.start(currentTime);
		oscillator.stop(currentTime + (noteLength / tempo) * 60);
	});
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
	solveHanoi(10, 'A', 'C', 'B');
	playNotes();
}

function runTests() {
	// Have tests here
}