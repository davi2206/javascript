const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Project Title"
const FPS = 250;

let AUDIO_CONTEXT;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let object_size = 50;
let done = 0;

let noteNrs = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);
	AUDIO_CONTEXT = new (window.AudioContext || window.webkitAudioContext)();
	
	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	// setInterval(function() {
	// 	moveAll();
	// 	drawAll();
	// }, 1000/FPS);
}

function moveAll() {
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	
}

function solveHanoi(n, source, destination, auxiliary) {
	if (n == 1) {
		console.log(`Move disk 1 from rod ${source} to rod ${destination}`);
		noteNrs.push(1);
		return;
	}
	solveHanoi(n-1, source, auxiliary, destination);
	console.log(`Move disk ${n} from rod ${source} to rod ${destination}`);
	noteNrs.push(n);
	solveHanoi(n-1, auxiliary, destination, source);
}

function playNote(freequency = 261.63, tempo = 120, noteLength = 0.5) {
	let currentTime;
	let oscillator;

	currentTime = AUDIO_CONTEXT.currentTime
	oscillator = AUDIO_CONTEXT.createOscillator();
	console.log(oscillator);
	oscillator.frequency.value = freequency;
	oscillator.connect(AUDIO_CONTEXT.destination);
	oscillator.start(currentTime);
	oscillator.stop(currentTime + (noteLength / tempo) * 60);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
	playNote(random_floor(900, 1000), 100, random(0.2, 0.5));
}

function runTests() {
	// Have tests here
}