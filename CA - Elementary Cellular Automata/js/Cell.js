/*
 * Cell Class
*/

class Cell {
    constructor(pos, width, height, val, color) {
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.val = val;
        this.color = color;
    }
}