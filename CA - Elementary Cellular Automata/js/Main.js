const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 2000;
const CANVAS_HEIGHT = 1000;
const TITLE = "Project Title"
const FPS = 1;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js", "Cell.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let scale = 10;
let cols = Math.floor(CANVAS_WIDTH / scale);
let rows = Math.floor(CANVAS_HEIGHT / scale);

let cells = [];

let width;
let height;

let keyNr = 0;
let key = [0, 0, 0, 0, 0, 0, 0, 0];
let colors = ['rgb(0,0,0)','rgb(255,0,0)','rgb(0,255,0)','rgb(0,0,255)','rgb(255,255,0)','rgb(255,0,255)','rgb(0,255,255)','rgb(255,255,255)'];

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function() {
		init();
		drawAll();
	}, 1000/FPS);
}

function init() {
	keyNr++;
	cells = [];
	titleH1.innerHTML = "Rule: " + keyNr;
	for (let i = 0; i < cols; i++) {
		cells.push(new Cell([i,0], width, height, 0, colors[7]));
	}
	let mid = Math.floor(cols / 2) - 1;
	cells[mid].val = 1;
	cells[mid].color = colors[0];
	height = CANVAS_HEIGHT / rows;
	width = height;

	if (keyNr > -1) {
		let k = keyNr.toString(2);
		while (k.length < 8) { k = '0' + k; }
		key = k.split('').map(Number);
		key = key.reverse();
	}

	for (let y = 1; y < rows; y++) {
		for (let x = 0; x < cols; x++) {
			cells.push(getValue(x, y));
		}
	}
}

function drawAll() {
	for (let i = 0; i < cells.length; i++) {
		let x = (i % cols) * width;
		let y = getYfromIndex(i) * height;
		colorRect(x, y, width, height, cells[i].color);
	}
}

function getValue(x, y) {
	let index;
	if (y > 0) {
		let sum = 0;
		index = getIndex(x, y - 1);
		let plusCols = (x == 0) ? cols : 0;
		let minusCols = (x == cols - 1) ? cols : 0;

		sum += (cells[index - 1 + plusCols].val * 4);
		sum += (cells[index].val * 2);
		sum += (cells[index + 1 - minusCols].val);

		return new Cell([x, y], width, height, key[sum], colors[sum]);
	}
}

function getIndex(x, y) {
	return (y * cols + x);
}
function getYfromIndex(i) {
	return Math.floor(i / cols);
}
function getXfromIndex(i) {
	return Math.floor(i % cols);
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}