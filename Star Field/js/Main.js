const LIVE = false;
const CANVAS_COLOR = 'rgba(0,0,0)';//'darkblue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 1200;
const TITLE = "Rockets"
const FPS = 100;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';
const MAX_STARS = 2000;
const STAR_MAX_SIZE = 2;

let scripts = ["Inputs.js"];
let commonScripts = ["draw.js", "Vector.js", "Particle.js"];
var scriptsToLoad = 0;

let object_size = 50;

let particles = [];
let mxDst = 0;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	//loadImages();
	runSim();
	registerEvents();
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	for(let i = 0; i < particles.length; i++) {
		if(particles[i].live) {
			particles[i].vector.mult(1.02);

			let dstFrCenter = particles[i].getDistFromPoint([CANVAS_WIDTH/2,CANVAS_HEIGHT/2]);
			if(dstFrCenter > mxDst) {
				mxDst = dstFrCenter;
				console.log(dstFrCenter);
			}
			particles[i].r = STAR_MAX_SIZE / 100 * (dstFrCenter/10);
			particles[i].move();
		}
		else {
			removeFromSet(particles[i], particles);
		}
	}

	if(particles.length < MAX_STARS) {
		for(let i = 0; i < MAX_STARS/FPS; i++) {
			particles.push(createStar());
		}
	}
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	for(let i = 0; i < particles.length; i++) {
		particles[i].draw();
		particles[i].drawVectorLine(-5, "white", particles[i].r);
	}
}

function onMouseClick(evt) {}

function createStar() {
	let r = 0; // Size should be set, based on distance from center
	let vectorX = random(-5,5);
	let vectorY = random(-5,5);
	let vector = new Vector(vectorX, vectorY);
	vector.set_magnitude(0.2);
	let x = CANVAS_WIDTH/2 + vector.x;
	let y = CANVAS_HEIGHT/2 + vector.y;
	let color = OBJECT_COLOR;
	let fadePct = -0.5;
	let gravity = new Vector(0,0);
	let dieOutsideCanvasNSEW = [true, true, true, true];
	let minSize = 0;
	return new Particle(x, y, r, vector, color, fadePct, gravity, dieOutsideCanvasNSEW, minSize);
}