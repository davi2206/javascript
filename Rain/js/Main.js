const LIVE = false;
const CANVAS_COLOR = 'rgba(0,0,0,0.07)';//'darkblue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 1200;
const TITLE = "Rockets"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'blue';
const MAX_RAIN = 1000;
const RAIN_MORE = 2;

let scripts = ["Inputs.js"];
let commonScripts = ["draw.js", "Vector.js", "Particle.js"];
var scriptsToLoad = 0;

let object_size = 50;

let particles = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	//loadImages();
	runSim();
	registerEvents();
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	for(let i = 0; i < particles.length; i++) {
		if(particles[i].live) {
			particles[i].move();
		}
		else {
			removeFromSet(particles[i], particles);

			if(particles.length < MAX_RAIN) {
				for(let i = 0; i < RAIN_MORE; i++) {
					particles.push(getDrop());
				}
			}
		}
	}
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	for(let i = 0; i < particles.length; i++) {
		particles[i].draw();
	}
}

function onMouseClick(evt) {
	particles.push(getDrop());
}

function getDrop() {
	let x = random(0, CANVAS_WIDTH);
	let y = random(-CANVAS_HEIGHT, 0);
	let r = random(0.75,1.5);
	let vector = new Vector(0.1,0);
	let color = OBJECT_COLOR;
	let fadePct = 0;
	let gravity = new Vector(0,0.005);
	let dieOutsideCanvasNSEW = [false, true, true, true];
	let minSize = 0.1;
	return new Particle(x, y, r, vector, color, fadePct, gravity, dieOutsideCanvasNSEW, minSize);
}