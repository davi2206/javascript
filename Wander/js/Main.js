// CANVAS PARAMETERS
const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Wandering Shapes"
const FPS = 60;

// VEHICLE PARAMETERS
const VEHICLE_WRAP_EDGES = true;
const VEHICLE_COLOR = "white";
const VEHICLE_MAX_SPEED = 1;
const VEHICLE_MAX_FORCE = 0.1;
const VEHICLE_SIZE = 15;
const VEHICLE_NODES = 4;


// SCRIPTS TO LOAD
let scripts = ["ImageLoader.js", "Inputs.js", "UI.js", "Vehicle.js"]; // Project Specific
let commonScripts = ["draw.js", "Point.js", "Vector.js", "VectorMath.js"]; // Common Resources
var scriptsToLoad = 0;

let object_size = 50;

let vehicle;
let pursuer;
let fleer;
let rando;
let wandere;

let vehicles = [];

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	// Generate Wanderers
	for (let i = 5; i < 10; i++) {
		vehicles.push(new Vehicle(
			new Point(random(0,CANVAS_WIDTH), random(0,CANVAS_HEIGHT)),
			new Vector(random(-5,5), random(-5,5)),
			wrapEdges = true,
			color = getRandColorHex(),
			maxSpeed = VEHICLE_MAX_SPEED,
			maxForce = VEHICLE_MAX_FORCE/i,
			size = VEHICLE_SIZE,
			nodes = (i+3)));
	}

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function () {
		moveAll();
		drawAll();
	}, 1000 / FPS);
}

function moveAll() {
	//let mousePos = new Point(0,CANVAS_HEIGHT);
	vehicles.forEach(v => v.move());
}

function drawAll() {
	// Canvas
	colorRect(0, 0, canvas.width, canvas.height, CANVAS_COLOR);

	vehicles.forEach(v => v.draw());
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}