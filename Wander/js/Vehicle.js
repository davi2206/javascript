
class Vehicle {
    constructor(pos, vector, wrapEdges, color, maxSpeed, maxForce, size, nodes) {
        canvas = document.getElementById('gameCanvas');
        this.pos = pos || new Point(random(0, canvas.width), random(0, canvas.height));
        this.velocity = vector || new Vector(random(0, 10), random(0, 10));
        this.wrapEdges = wrapEdges == null ? true : wrapEdges;
        this.color = color || getRandColor();
        this.maxSpeed = maxSpeed || random(1, 10);
        this.maxForce = maxForce || random(1, 10);
        this.size = size || random(1, 25);
        this.nodes = nodes || random(3, 25);

        this.desiredVector = new Vector(0, 0);
        this.drawDesiredVelocity = new Vector(0, 0);

        this.debugMultiplier = 5;

        // Wander Values
        this.tp = new Point(0, 0);
        this.tp_dist = 100;
        this.s_r = 55;
        this.deg = 0;
        this.max_deg = 25;

        // Draw line
        this.path = [];
        this.spacing = FPS/10;
        this.frames = 0;
        this.point_limit = 1500;
    }

    move() {
        this.frames++;
        if (this.frames == this.spacing) {
            this.path.push(this.pos.copy());
            if (this.path.length > this.point_limit) {
                this.path.shift();
            }
            this.frames = 0;
        }

        this.drawDesiredVelocity = this.velocity.copy();

        this.velocity.limit(this.maxSpeed);
        this.pos.move_vector(this.velocity);
        if (this.wrapEdges) this.edges();

        this.wp = this.wander();
        let desire = this.seek(this.wp);
        this.velocity.add(desire);
    }

    seek(targetPos) {
        // Vector from Vehicle to target
        let desiredVector = new Vector(targetPos.x - this.pos.x, targetPos.y - this.pos.y);

        // Limit to max speed
        desiredVector.limit(this.maxSpeed);
        desiredVector.subtract(this.velocity);

        desiredVector.limit(this.maxForce);

        return desiredVector;
    }

    wander() {
        this.tp = this.pos.copy();
        let v = this.velocity.copy();
        v.extend_dist(this.tp_dist);
        this.tp.move_vector(v);

        let s = this.velocity.copy();
        s.set_magnitude(this.s_r);

        // Randomize deg
        this.deg = this.deg + random((this.max_deg * -1), this.max_deg);

        s.turn_deg(this.deg);
        this.debug_s = s.copy();

        let wp = this.tp.copy();
        wp.move_vector(s);
        return wp;
    }

    edges() {
        if (this.pos.x > canvas.width) this.pos.x = 0;
        if (this.pos.y > canvas.height) this.pos.y = 0;
        if (this.pos.x < 0) this.pos.x = canvas.width;
        if (this.pos.y < 0) this.pos.y = canvas.height;
    }

    draw() {
        drawPolygon(this.pos.x, this.pos.y, this.size, this.nodes, this.velocity, this.color, this.color);

        // Debug lines
        if (false) {
            let drawVelocity = this.velocity.copy();
            drawVelocity.mult(this.debugMultiplier);
            drawVelocity.draw(this.pos, "green", 2);
            this.drawDesiredVelocity.draw(this.pos, "red", 2);
            this.tp.draw();
            colorCircle(this.tp.x, this.tp.y, this.s_r, 'red', false, 3);
            this.debug_s.draw(this.tp, 'black', 3);
        }

        this.path.forEach((p) => p.draw(1, this.color));
    }
}