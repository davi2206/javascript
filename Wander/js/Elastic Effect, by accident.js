
class Elastic {
    constructor(pos, vector) {
        this.pos = pos;
        this.velocity = vector;
    }

    move() {
        this.pos.move_vector(this.velocity);
        this.wrapCoord();
    }

    draw() {
        colorCircle(this.pos.x, this.pos.y, 5, 'white', true);
    }

    seek(target) {
        if(target) {
            // Vector from Vehicle to target
            let targetVector = new Vector(target.x - this.pos.x, target.y - this.pos.y);
            // Limit to max speed
            // Steer force = desired vel - cur vel
            // Limit to max forvce

            this.addForce(targetVector);
        }




        // let force = 0.01;
        // targetVector.mult(force);
        // this.velocity.add(targetVector);
        // this.velocity.div(50);

        // if(this.velocity.get_magnitude() > 2) this.velocity.set_magnitude(2);
    }

    addForce(vector) {
        let dotNeg = this.velocity.x*-vector.y + this.velocity.y*vector.x;
        let dot = this.velocity.x * vector.x + this.velocity.y * vector.y;
        let forceMagnitude = vector.get_magnitude();
        let velocityMagnitude = this.velocity.get_magnitude();
		
		let steering = "none";
		let steer = 0;
		if(dotNeg < 0){
			steering = "right";
			steer = 1;
		} else if(dotNeg > 0){
			steering = "left";
			steer = -1;
		}

        if(forceMagnitude > (velocityMagnitude*20)) {
            this.velocity.mult(1.1);
        } else {
            this.velocity.div(1.1);
        }

		// let force = (steer * 5);
        // this.velocity.turn_deg(force);
        vector.subtract(this.velocity);
        vector.div(50);
        this.velocity.add(vector);
    }

    wrapCoord() {
		if(this.pos.x > CANVAS_WIDTH) {
			this.pos.x = 0;
		}
		if(this.pos.y > CANVAS_HEIGHT) {
			this.pos.y = 0;
		}
		if(this.pos.x < 0) {
			this.pos.x = CANVAS_WIDTH;
		}
		if(this.pos.y < 0) {
			this.pos.y = CANVAS_HEIGHT;
		}
	}
}