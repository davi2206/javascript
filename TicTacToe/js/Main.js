const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 1000;
const LINE_WIDTH = 10;
const TITLE = "Tic-Tac-Toe"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'black';
const PLAYER_X = 'Player1';
const PLAYER_O = 'AI';

var scripts = ["Inputs.js", "UI.js", "Game.js", "ai_mover.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let object_size = 50;
let turn = PLAYER_X;
let board = [
	['','',''],
	['','',''],
	['','','']
];

let winner = '';

let xSpaces = [];
let oSpaces = [];

window.onload = function() {
	var baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR);
	drawLevel();

	registerEvents();
	registerUIEvents();

	turn = PLAYER_X;
	board = [
		['','',''],
		['','',''],
		['','','']
	];
	winner = '';
	drawAll();
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	drawLevel();
}

function drawLevel() {
	hori_top_start = [0,CANVAS_HEIGHT/3];
	hori_top_end = [CANVAS_WIDTH,CANVAS_HEIGHT/3];

	hori_bot_start = [0,CANVAS_HEIGHT/3*2];
	hori_bot_end = [CANVAS_WIDTH,CANVAS_HEIGHT/3*2];

	vert_lft_start = [CANVAS_WIDTH/3,0];
	vert_lft_end = [CANVAS_WIDTH/3,CANVAS_HEIGHT];

	vert_rght_start = [CANVAS_WIDTH/3*2,0];
	vert_rght_end = [CANVAS_WIDTH/3*2,CANVAS_HEIGHT];

	drawLineTo(hori_top_start, hori_top_end, stroke = 'black', width = LINE_WIDTH);
	drawLineTo(hori_bot_start, hori_bot_end, stroke = 'black', width = LINE_WIDTH);
	drawLineTo(vert_lft_start, vert_lft_end, stroke = 'black', width = LINE_WIDTH);
	drawLineTo(vert_rght_start, vert_rght_end, stroke = 'black', width = LINE_WIDTH);

	let w = CANVAS_WIDTH / 3;
	let h = CANVAS_HEIGHT / 3;
	let tokenSize = (Math.min(w, h) * 0.75)/2;
	
	for (let j = 0; j < 3; j++) {
		for (let i = 0; i < 3; i++) {
			let x = w * i + w / 2;
			let y = h * j + h / 2;
			let spot = board[j][i];
			
			if (spot == 'X') {
				// Draw X
				colorX(x, y, tokenSize, 'black');
			} else if (spot == 'O') {
				// Draw O
				colorCircle(x, y, tokenSize, 'blue', false);
			}
		}
	}
}

function onMouseClick(evt) {
	//let mousePos = {x: 600, y:333};
	let mousePos = getMousePos(evt);
	let spot = getSpotFromPos(mousePos);

	if(winner == '') {
		console.log('Spot: ' + spot);
		winner = placeToken(spot);
	}
	if(winner != '') {
		console.log('Winner is: ' + winner);
	}

	if(turn == 'AI' && winner == '') {
		let ai_spot = ai_move(board);
		winner = placeToken(ai_spot);
	}

	drawLevel();
}
