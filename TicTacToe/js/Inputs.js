// Map arrow keys
const KEY_LEFT_ARROW = 37;
const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;

const KEY_SPACE = 32;
const KEY_ENTER = 13;
const KEY_RESET = 82;

let mousePos;

function registerEvents() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	canvas.addEventListener('mousedown', handleMouseClick);
	canvas.addEventListener('mousemove', function(evt) {mousePos = getMousePos(evt);});
	document.addEventListener('keydown', keyPressed);
//	document.addEventListener('keyup', keyReleased);
}

// Mouse stuff
function handleMouseClick(evt) {	
	onMouseClick(evt);
}

function keyChange(keyEvt, value) {
	switch(keyEvt.keyCode) {
		case KEY_SPACE:
			break;
		case KEY_ENTER:
			break;
		default:
			console.log("Pressed key: "+keyEvt.keyCode);
			break;
	}
}

// Keyboard inputs
function keyPressed(evt) {
	keyChange(evt, true);
}

function keyReleased(evt) {
	keyChange(evt, false);
}