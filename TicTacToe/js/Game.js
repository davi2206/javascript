let cellWidth = CANVAS_WIDTH/3;
let cellHeight = CANVAS_HEIGHT/3;

function getSpotFromPos(mousePos) {
    if(mousePos.x < CANVAS_WIDTH/3) {
        spaceX = 0;
    }
    else if(mousePos.x < CANVAS_WIDTH/3*2 && mousePos.x > CANVAS_WIDTH/3) {
        spaceX = 1;
    }
    else if(mousePos.x < CANVAS_WIDTH && mousePos.x > CANVAS_WIDTH/3*2) {
        spaceX = 2;
    }

    if(mousePos.y < CANVAS_HEIGHT/3) {
        spaceY = 0;
    }
    else if(mousePos.y < CANVAS_HEIGHT/3*2 && mousePos.y > CANVAS_HEIGHT/3) {
        spaceY = 1;
    }
    else if(mousePos.y < CANVAS_HEIGHT && mousePos.y > CANVAS_HEIGHT/3*2) {
        spaceY = 2;
    }

    return [spaceX, spaceY];
}

function placeToken(spot) {
    let spaceX = spot[0];
    let spaceY = spot[1];
    
    if(board[spaceY][spaceX] !== '') {
		return;
    }

    if(turn == PLAYER_X) {
        board[spaceY][spaceX] = 'X';
        turn = PLAYER_O;
    }
    else if(turn == PLAYER_O) {
        board[spaceY][spaceX] = 'O';
        turn = PLAYER_X;
    }

    return checkWinner(board);
}

function checkWinner(board) {
    let winner = '';
    let start, end;

    // Horizontal?
    for(let i = 0; i < 3; i++) {
        if(compare3(board[i][0], board[i][1], board[i][2])) {
            winner = board[i][0];
            start = [cellWidth/2, cellHeight/2 + cellHeight * i];
            end = [cellWidth/2+cellWidth*2, cellHeight/2 + cellHeight * i];
        }
    }

    // Vertical?
    for(let i = 0; i < 3; i++) {
        if(compare3(board[0][i], board[1][i], board[2][i])) {
            winner = board[0][i];
            start = [cellWidth/2 + cellWidth * i, cellHeight/2];
            end = [cellWidth/2 + cellWidth * i, cellHeight/2 + cellHeight * 2];
        }
    }

    // Diag L-R
    if(compare3(board[0][0], board[1][1], board[2][2])) {
        winner = board[0][0];
        start = [cellWidth/2, cellHeight/2];
        end = [cellWidth/2 + cellWidth * 2, cellHeight/2 + cellHeight * 2];
    }
    // Diag R-L
    if(compare3(board[0][2], board[1][1], board[2][0])) {
        winner = board[0][2];
        start = [cellWidth/2, cellHeight/2 + cellHeight * 2];
        end = [cellWidth/2 + cellWidth * 2, cellHeight/2];
    }

    if(winner != '') {
        drawLineTo(start, end, stroke = 'lightgreen', width = 15);
    }

    if(checkTie()) winner = 'Tie';

    return winner;
}

function checkTie() {
    for(let i = 0; i < 3; i++) {
        for(let j = 0; j < 3; j++) {
            if(board[j][i] == '') {
                return false;
            }
        }
    }
    return true;
}

function compare3(a, b, c) {
    return (a == b && b == c && a != '');
}