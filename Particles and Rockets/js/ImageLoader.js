// Create elements for images
var imageOne = document.createElement("img");

var pics = [];

var picsToLoad = 0;

function loadImages() {
	const imageMap = new Map();
	//imageMap.set(imageOne, "image.png");
	
	picsToLoad = imageMap.size;
	
	for(var [k,v] of imageMap) {
		beginLoadImg(k, v);
	}
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;
		
		if(picsToLoad == 0) {
			runSim();
		}
	}
	
	var baseDir = 'graphics/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/graphics/';
	}
	imgVar.src = baseDir+fileName;
}