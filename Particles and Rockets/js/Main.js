const LIVE = false;
const CANVAS_COLOR = 'rgba(0,0,0,0.07)';//'darkblue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 1200;
const TITLE = "Rockets"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js", "Explosion.js", "Rocket.js"];
let commonScripts = ["draw.js", "Vector.js", "Particle.js"];
var scriptsToLoad = 0;

let object_size = 50;

let rockets = [];
let particles = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	//loadImages();
	runSim();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	for(let i = 0; i < particles.length; i++) {
		if(particles[i].live) {
			particles[i].move();
		}
		else {
			removeFromSet(particles[i], particles);
		}
	}
	for(let i = 0; i < rockets.length; i++) {
		if(rockets[i].live) {
			rockets[i].move();
		}
		else {
			removeFromSet(rockets[i], rockets);
		}
	}
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	for(let i = 0; i < particles.length; i++) {
		particles[i].draw();
	}
	for(let i = 0; i < rockets.length; i++) {
		rockets[i].draw();
	}
}

function onMouseClick(evt) {
	let mousePos = getMousePos(evt);
	for(let i = 0; i < 3; i++) {
		generateRocket(mousePos);
	}
}

function generateRocket(mousePos) {
	let rocket_x = random(-0.5, 0.5);
	let rocket_y = random(-2, -1.5);
	let rocker_vector = new Vector(rocket_x, rocket_y);
	let detonation_height = random(0, CANVAS_HEIGHT/1.5);
	let power = random(0.15, 2);
	let color = getRandColor();
	this.rocket = new cRocket(mousePos.x, CANVAS_HEIGHT, 5, 2, rocker_vector, detonation_height, power, color);
	rockets.push(rocket);
}

function runTests() {
	// Have tests here
}