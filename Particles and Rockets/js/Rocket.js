function cRocket(x, y, h = 5, w = 2, vector = new Vector(0,-1), max_height = 0, power = 1, color = OBJECT_COLOR) {
	this.x = x;
	this.y  = y;
	this.h = h;
	this.w = w;
	this.vector = vector;
	this.max_height = max_height;
	this.power = power;
	this.color = color;

	this.live = true;
	this.chance = 0;

	this.move = function() {
		// Update 'this.x' and 'this.y' from vector
		this.x += this.vector.x;
		this.y += this.vector.y;

		this.vector.y *= 0.999;

		this.chance = random_floor(0,9);

		if(this.chance < 5) {
			// Trailing particles
			this.explosion = new cExplosion(this.x, this.y, power = 0.1, count = 5, size = [0.01,0.05]);
			particles.push(...this.explosion.explode(["#FFBF00","#FFEA00","#FDDA0D","#FFD700","#801100","#b62203","#d73502","#fc6400","#ff7500","#fac000"]));
		}

		if(this.y < max_height || this.x < 0 || this.x > CANVAS_WIDTH) {
			// Detonate rocket
			this.detonate();
		}
	}
	
	this.draw = function() {
		colorRect(this.x, this.y, this.w, this.h, this.color, this.color);
	}

	this.detonate = function() {
		this.explosion = new cExplosion(this.x, this.y, this.power, count = 50, size = [0.1,1]);
		particles.push(...this.explosion.explode());

		this.live = false;
	}
}