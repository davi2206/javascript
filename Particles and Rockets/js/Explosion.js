function cExplosion(x, y, power = 50, count = 5, size = [0.1,1]) {
	this.speed = power;
	this.x = x;
	this.y  = y;
	this.count = count;

	this.particles = [];
	
	this.explode = function(colors = []) {
		let r;
		let vector;
		let color;
		let fadePct;
		let particle;
		let gravity;

		for(let i = 0; i < count; i++) {
			// Generate a new particle
			r = random(size[0],size[1]);
			vector = new Vector(random(-power, power),random(-power, power));
			color = getRandColor(colors);
			fadePct = random(0.1,1);
			gravity = new Vector(0.001,0.01);

			particle = new Particle(this.x, this.y, r, vector, color, fadePct, gravity, [false, true, true, true], r/10);
			this.particles.push(particle);
		}

		return this.particles;
	}
}