/*
 * Cell Class
*/

class Cell {
    constructor(pos, width, height, val, color) {
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.val = val;
        this.color = color;
    }

    update() {
        this.val++;
        if(this.val >= colors.length) this.val = 0;
    }

    draw() {
		colorRect(this.pos[0]*this.width, this.pos[1]*this.height, this.width, this.height, colors[this.val], 'black');
    }
}