const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 2000;
const CANVAS_HEIGHT = 1000;
const TITLE = "Langton's Ant"
const FPS = 15;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js", "Cell.js", "Ant.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let cols = 500;
let rows = 250;

let cells = [];

let width;
let height;

let steps = 0;
let time;
let started = false;

let audioContext = new (window.AudioContext || window.webkitAudioContext)();
let tempo;
let noteLength;
let notes;
let currentTime;
let oscillator;
let oscillator2;

let colors = ['rgb(255,255,255)', 'rgb(0,0,0)', 'rgb(255,0,0)', 'rgb(0,255,0)', 'rgb(0,0,255)', 'rgb(255,255,0)', 'rgb(255,0,255)', 'rgb(0,255,255)'];

let antCount = 6;
let ants = [];

window.onload = function () {
	window.addEventListener("keydown", function (e) {
		// Prevent window from scroling on arrow key press
		if (["Space", "ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);

	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if (LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	init();
	setupSound();

	setInterval(function () {
		if(started) {
			moveAnt();
			drawAll();
			steps++;
			if(steps % 64 == 0 && ants.length < antCount) {
				ants.push(new Ant(random_floor(0, cols-1), random_floor(0, rows-1), height/2, getRandColor(), steps/64));
			}
		}
	}, 1000 / FPS);
}

function init() {
	cells = [];
	height = CANVAS_HEIGHT / rows;
	width = height;
	for (let y = 0; y < rows; y++) {
		for (let x = 0; x < cols; x++) {
			cells.push(new Cell([x, y], width, height, 0, colors[0]));
		}
	}
	for (let i = 0; i < cells.length; i++) {
		cells[i].draw();
	}

	// for(let i = 0; i < antCount; i++) {
	// 	ants.push(new Ant(random_floor(0, cols-1), random_floor(0, rows-1), height/2, getRandColor(), i));
	// }

	time = Date.now();
}

function moveAnt() {
	ants.forEach(ant => {
		ant.move();
	});
	
	if(steps % 1000 == 0) {
		let t = Date.now();
		let elapsed = t-time;
		let seconds = elapsed/1000;
		titleH1.innerHTML = TITLE + " Steps: " + (steps) + " FPS: " + (Math.floor(steps/seconds));
	}
}

function drawAll() {
	ants.forEach(ant => {
		ant.draw();
	});
}

function getValue(x, y) {
	let index;
	if (y > 0) {
		let sum = 0;
		index = getIndex(x, y - 1);
		let plusCols = (x == 0) ? cols : 0;
		let minusCols = (x == cols - 1) ? cols : 0;

		sum += (cells[index - 1 + plusCols].val * 4);
		sum += (cells[index].val * 2);
		sum += (cells[index + 1 - minusCols].val);

		return new Cell([x, y], width, height, key[sum], colors[sum]);
	}
}

function getIndex(x, y) {
	return (y * cols + x);
}
function getYfromIndex(i) {
	return Math.floor(i / cols);
}
function getXfromIndex(i) {
	return Math.floor(i % cols);
}

function onMouseClick(evt) {
	started = !started;
}

function setupSound() {
	audioContext = new (window.AudioContext || window.webkitAudioContext)();
	tempo = 120; // Beats per minute
	noteLength = 1.5; // Length of each note in beats
	notes = [329.63, 369.99, 392, 440, 493.88, 554.37, 622.25, 659.26]; // Frequencies of the notes in Hz (E melodic minor -> E, F#, G, A, H, C#, D#, E)
}

let AUDIO_CONTEXT;
function playNote(nodeIndex, duration = 0.05) {
	if (AUDIO_CONTEXT == null) {
        AUDIO_CONTEXT = new(AudioContext || 
    webkitAudioContext || window.webkitAudioContext)()
    }

    var osc = AUDIO_CONTEXT.createOscillator();
    var gainNode = AUDIO_CONTEXT.createGain();
    gainNode.gain.setValueAtTime(0, AUDIO_CONTEXT.currentTime);

    gainNode.gain.linearRampToValueAtTime(0.4, AUDIO_CONTEXT.currentTime + 0.05);
    gainNode.gain.linearRampToValueAtTime(0, AUDIO_CONTEXT.currentTime +
        duration);

    osc.type = "triangle";

    osc.frequency.value = notes[nodeIndex];
    osc.start(AUDIO_CONTEXT.currentTime);

    osc.stop(AUDIO_CONTEXT.currentTime + duration);

    osc.connect(gainNode);
    gainNode.connect(AUDIO_CONTEXT.destination);
}