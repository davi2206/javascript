/*
 * Ant Class
*/

class Ant {
    constructor(col, row, size, color, index) {
        this.col = col;
        this.row = row;
        this.size = size;
        this.color = color;
        this.number = index;
        this.direction = random_floor(0,4);
        this.cellIndex = getIndex(this.col, this.row);
        this.cell = cells[this.cellIndex];
        this.sountDuration = (this.number+10)/100;
    }

    move() {
        // Update the current cell
        this.cell.update();
        this.cell.draw();

        if(steps%(this.number+1) == 0) {
            playNote(this.cell.val, this.sountDuration);
        }

        // Move Ant in Direction
        switch(this.direction) {
            case 0: // Up
                this.row--;
                if(this.row < 0) this.row = rows-1;
                break;
            case 1: // Right
                this.col++;
                if(this.col >= cols) this.col = 0;
                break;
            case 2: // Down
                this.row++;
                if(this.row >= rows) this.row = 0;
                break;
            case 3: // Left
                this.col--;
                if(this.col < 0) this.col = cols-1;
                break;
        }

        // Get new cell
        this.cellIndex = getIndex(this.col, this.row);
        this.cell = cells[this.cellIndex];

        if(this.cell.val % 2 == 0) this.turnRight();
        else this.turnLeft();
        this.cell.draw();
    }

    turnRight() {
        this.direction++;
        if(this.direction > 3) this.direction = 0;
    }

    turnLeft() {
        this.direction--;
        if(this.direction < 0) this.direction = 3;
    }

    draw(color = this.color) {
        colorX(this.col*width+width/2, this.row*height+height/2, this.size, color)
    }
}