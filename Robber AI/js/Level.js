// Level parameters
var tileSize;

var levelGrid = [];
var tileMap;
var startTileIndex;
var goalTileIndex;
var startNode;
var goalNode;

// Track tile options
const LEVEL_GROUND = 0;
const LEVEL_WALL = 1;
const LEVEL_START = 2;
const LEVEL_GOAL = 3;
const LEVEL_ART = 4;
const LEVEL_VAN = 5;
const LEVEL_DOOR = 6;


const DIVISOR = 10;
const LEVEL_COLS = 45;
const LEVEL_ROWS = 25;

const WALL_PCT = 0;

function initLevel() {
	tileMap = new Map();
	tileSize = canvas.width / LEVEL_COLS;
	levelGrid = [
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 4, 4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 2, 4, 0, 0, 1, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
	generateLevel();
	drawLevel();
	
	startNode = tileMap.get(startTileIndex);
	goalNode = tileMap.get(goalTileIndex);
	
	return true;
}

function generateLevel() {
	var arrayIndex = 0;
	var drawTileX = 0;
	var drawTileY = 0;
	
	nodes = LEVEL_COLS*LEVEL_ROWS;
	
	for(row = 0; row < LEVEL_ROWS; row++) {
		for(col = 0; col < LEVEL_COLS; col++) {
			levelIndex = getArrayIndex(col, row);
			
			var levelType = levelGrid[levelIndex];
			
			tileMap.set(levelIndex, new cTile(col, row, levelIndex, levelType));
			
			if(levelType == LEVEL_START) {
				startTileIndex = levelIndex;
			}
			if(levelType == LEVEL_GOAL) {
				goalTileIndex = levelIndex;
			}
			
			arrayIndex++;
			drawTileX += tileSize;
		}
		drawTileX = 0;
		drawTileY += tileSize;
	}
	drawTileY = 0;
}

function drawLevel() {
	for(const [k, v] of tileMap) {
		var color = "pink";
		switch(v.tileType) {
			case LEVEL_GROUND:
				color = "gray";
				break;
			case LEVEL_WALL:
				color = "black";
				break;
			case LEVEL_START:
				color = "green";
				break;
			case LEVEL_GOAL:
				color = "red";
				break;
			case LEVEL_ART:
				color = "aqua";
				break;
			case LEVEL_VAN:
				color = "white";
				break;
			case LEVEL_DOOR:
				color = "brown";
				break;
			default:
				color = "black";
				break;
		}
		
		if(v.isOpen) {
			color = "green";
		}
		if(v.isClosed) {
			color = "blue";
		}
		
		v.draw(color);
		v.setValuesHandF();
		//colorText("G: " + v.g, v.col*tileSize+2, v.row*tileSize+tileSize/2, 'yellow', 20);
		//colorText("H: " + v.h, v.col*tileSize+2, v.row*tileSize+tileSize/2+15, 'yellow', 20);
	}
}

function drawCurrent() {
	drawSet(openSet);
	drawSet(closedSet);
	
	if(currentNode) {
		paintTraceBack(currentNode);
	}
}

function drawSet(set) {
	for(var v of set) {
		var color = "pink";
		switch(v.tileType) {
			case LEVEL_GROUND:
				color = "gray";
				break;
			case LEVEL_WALL:
				color = "black";
				break;
			case LEVEL_START:
				color = "green";
				break;
			case LEVEL_GOAL:
				color = "red";
				break;
			default:
				color = "black";
				break;
		}
		
		if(v.isOpen) {
			color = "green";
		}
		if(v.isClosed) {
			color = "blue";
		}
		
		v.draw(color);
		//colorText("G: " + v.g, v.col*tileSize+2, v.row*tileSize+2, 'yellow', 7);
		//colorText("H: " + v.h, v.col*tileSize+2, v.row*tileSize+10, 'yellow', 7);
	}
}

function paintTraceBack(node) {
	node.draw('yellow');
	if(node.parentTile != null && node.parentTile != undefined) {
		paintTraceBack(node.parentTile)
	}
}