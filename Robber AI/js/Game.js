const LIVE = false;

var canvas;
var canvasContext;

const WALK_SPEED = 50;
const FPS = 250;
const LOCATE_MOUSE = false;

var startTime = 0;
var stopTime = 0;
var runTime = 0;
var steps = 0;

var searching = true;

let scripts = [];
let commonScripts = ["draw.js"];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
}

function play() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('This is some sort of loading screen', canvas.width/10, canvas.height/3, 'blue', 80);
	
	startTime = performance.now();
	setInterval(function() {fpsLoop();}, 1000/FPS);

	gameReset();
	registerEvents();
}

function gameReset() {
	initLevel();
	initSim();
	searching = true;
	drawLevel();
}

function fpsLoop() {
	if(searching) {
		steps++;
		
		getCurent();
	
		if(currentNode == goalNode || currentNode == null) {
			stopTime = performance.now();
			console.log("Found it!");
			searching = false;
			drawCurrent();
			runTime = stopTime-startTime;
			
			console.log("Moved " + steps + " in " + runTime + " ms");
			console.log("Speps/sec: " + steps/(runTime/1000));
			// location.reload();
			return;
		}
		currentNode.getNeighbours();
		
		for(var neighbour of currentNode.neighbours) {
			if(!neighbour.isOpen || neighbour.g > (currentNode.g+STEP_VALUE)) { // New node
				neighbour.g = (currentNode.g+neighbour.stepValue);
				neighbour.setValuesHandF();
				neighbour.parentTile = currentNode;
				if(!neighbour.isOpen) {
					neighbour.isOpen = true;
					openSet.push(neighbour);
				}
			}
		}
		
		// Level
		// drawCurrent();
		//drawLevel();
	}
	
	findMouseTile();
	
	// Print Mouse Possition
	if(LOCATE_MOUSE) {
		colorText(mouseTileX+","+mouseTileY+": "+mouseIndex, mouseX, mouseY, 'yellow', 15);
	}
	
	//for(var i=0; i < openSet.length; i++) {
	//	var node = openSet[i];
	//	colorText(node.g, node.col*tileSize+5, node.row*tileSize+20, 'yellow', 15);
	//	colorText(node.h, node.col*tileSize+5, node.row*tileSize+40, 'yellow', 15);
	//}
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

