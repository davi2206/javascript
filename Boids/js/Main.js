const LIVE = false;
const CANVAS_COLOR = 'blue';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const FPS = 250;
const FLOCK_SIZE = 3;
const BOID_SIZE = 5;
const BOID_COLOR = 'white';
const BOID_SPEED = 0.62;
const BOID_PERCEPTION = 50;
const BOID_STEER_FORCE = 0.5;
const BOID_STEER_TOLERANCE = (BOID_STEER_FORCE);

var scripts = ["Boid.js", "Flock.js", "ImageLoader.js", "TestCases/TestVectorMath.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;
var boid = null;
var flock = [];

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	var baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/GoldFish/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
}

function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, 'fishTank', 'fishCanvas');

	loadImages();
}

function runSim() {
	flock = new cFlock(FLOCK_SIZE);
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	flock.move();
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	//drawImgCentRot(cityImg, canvas.width/2, canvas.height/2, 0);
	
	flock.draw();
}

function runTests() {
	// 0 is good, 1 is ERROR
	var res = testVectorTurn();
}