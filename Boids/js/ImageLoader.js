// Create elements for images
var fishStraight = document.createElement("img");
var fishLeft = document.createElement("img");
var fishRight = document.createElement("img");

var pics = [];

var picsToLoad = 0;

function loadImages() {
	const imageMap = new Map();
	imageMap.set(fishStraight, "Fish_Straight.png");
	imageMap.set(fishLeft, "Fish_Left.png");
	imageMap.set(fishRight, "Fish_Right.png");
	
	picsToLoad = imageMap.size;
	
	for(var [k,v] of imageMap) {
		beginLoadImg(k, v);
	}
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;
		
		if(picsToLoad == 0) {
			runSim();
		}
	}
	
	var baseDir = 'graphics/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/GoldFish/graphics/';
	}
	imgVar.src = baseDir+fileName;
}