function cBoid(x, y, ang, vector, color = BOID_COLOR) {
	this.isMarker = false;
	this.steering = "none";
	
	this.speed = BOID_SPEED;
	this.perception = BOID_PERCEPTION + BOID_SIZE;
	this.r  = BOID_SIZE;
	this.x = x;
	this.y  = y;
	
	this.vector = vector;
	this.prevVector = this.vector;
	this.updated = false;
	this.direction = ang;
	this.delta = 0;
	this.newDirection = 0;
	
	this.rad = getRadOfDeg(ang);
	
	this.nearby = [];
	
	this.avg_vector = [];
	
	this.move = function() {
		this.prevVector = this.vector;
		
		this.steer();
		
		this.rad = vector_ang(this.vector);
		this.direction = this.rad * 180 / Math.PI;
		
		this.x += this.vector[0] * this.speed;
		this.y += this.vector[1] * this.speed;
		
		this.coord = this.wrapCoord(this.x, this.y);
		this.x = this.coord[0];
		this.y = this.coord[1];
	}
	
	this.steer = function() {
		this.checkPeers();
		this.align();
	}
		
	this.checkPeers = function() {
		this.nearby = [];
		this.near = flock.flock.filter(boid => (boid.x > this.x-BOID_PERCEPTION && boid.x < this.x+BOID_PERCEPTION));
		this.near = this.near.filter(boid => (boid.y > this.y-BOID_PERCEPTION && boid.y < this.y+BOID_PERCEPTION));
		this.near.forEach((boid) => this.addPeer(boid));
	}
	
	this.addPeer = function(boid) {
		this.xDif = (this.x - boid.x);
		this.yDif = (this.y - boid.y);
		this.cSq = Math.pow(this.xDif, 2) + Math.pow(this.yDif, 2);
		this.dist = Math.sqrt(this.cSq);
		
		if(this.dist < this.perception && this != boid) {
			this.nearby.push(boid);
		}
	}
	
	this.align = function() {
		this.peer_vectors = [];
		// Avg vector
		// move my vector towards average
		
		this.nearby.forEach((boid) => this.pushVector(boid));
		this.peer_vectors.push(this.vector);
		// console.log(this.peer_vectors);
		this.avg_vector = vector_avg(this.peer_vectors);
		
		this.fraction = Math.abs(this.vector[1])/Math.abs(this.vector[0]);
		this.avg_fraction = Math.abs(this.avg_vector[1])/Math.abs(this.avg_vector[0]);
		
		this.dot = this.vector[0]*-this.avg_vector[1] + this.vector[1]*this.avg_vector[0];
		
		
		this.steering = "none";
		steer = 0;
		if(this.dot < 0){
			this.steering = "right";
			steer = 1;
		} else if(this.dot > 0){
			this.steering = "left";
			steer = -1;
		}
		
		this.force = (steer * BOID_STEER_FORCE);
		this.vector = vector_turn(this.vector, this.force);
		
		this.matchMinSpeed();
		this.updated = true;
		
		// if(this.isMarker) {
			// console.log(this.steering);
		// }
	}
	
	this.pushVector = function(boid) {
		if(boid.updated){	
			this.peer_vectors.push(boid.prevVector);
		} else {
			this.peer_vectors.push(boid.vector)
		}
	}
	
	this.matchMinSpeed = function() {
		this.cSq = Math.pow(this.vector[0], 2) + Math.pow(this.vector[1], 2);
		this.c = Math.sqrt(this.cSq);
		
		this.factor = (this.speed/this.c);
		
		this.vector[0] = this.vector[0]*this.factor;
		this.vector[1] = this.vector[1]*this.factor;
	}
	
	this.wrapCoord = function(x, y) {
		if(x > CANVAS_WIDTH) {
			x = 0;
		}
		if(y > CANVAS_HEIGHT) {
			y = 0;
		}
		if(x < 0) {
			x = CANVAS_WIDTH;
		}
		if(y < 0) {
			y = CANVAS_HEIGHT;
		}
		
		return [x,y];
	}
	
	this.draw = function() {
		switch(this.steering) {
			case "none":
				drawImgCentRot(fishStraight, this.x, this.y, this.rad);
				break;
			case "right":
				drawImgCentRot(fishRight, this.x, this.y, this.rad);
				break;
			case "left":
				drawImgCentRot(fishLeft, this.x, this.y, this.rad);
				break;
			default:
				colorCircle(this.x, this.y, this.r, color);
		}
		
		// // FOR DEBUG ONLY
		// this.nx = this.x + this.vector[0]*this.perception;
		// this.ny = this.y + this.vector[1]*this.perception;
		// drawLineTo([this.x,this.y], [this.nx,this.ny]);
		
		// this.vx = this.x + this.avg_vector[0]*this.perception;
		// this.vy = this.y + this.avg_vector[1]*this.perception;
		// drawLineTo([this.x,this.y], [this.vx,this.vy], 'red');
		
		// if(this.isMarker) {
			// colorCircle(this.x, this.y, this.perception, 'rgba(128, 0, 0, 0.3)');
		// }
	}
}