/*
	FUNCTIONS FOR VECTOR MATH
*/

var rad = 0;

var x = 0;
var y = 0;
var xn = 0;
var yn = 0;

function vector_add(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]+v2[0];
	vector[1] = v1[1]+v2[1];
	
	return vector;
}

function vector_sub(v1, v2) {
	var vector = [0,0];
	
	vector[0] = v1[0]-v2[0];
	vector[1] = v1[1]-v2[1];
	
	return vector;
}

function vector_mult(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]*nr;
	vector[1] = v1[1]*nr;
	
	return vector;
}

function vector_div(v1, nr) {
	var vector = [0,0];
	
	vector[0] = v1[0]/nr;
	vector[1] = v1[1]/nr;
	
	return vector;
}

function vector_avg(vectors) {
	var vector = [0,0];
	
	for (var i = 0; i < vectors.length; i++) {
		vector = vector_add(vector, vectors[i]);
	}
		
	vector = vector_div(vector, vectors.length);
	
	return vector;
}

function vector_ang(v) {
	var ang = Math.atan2(v[1],v[0]);
	return ang;
}

function vector_turn(v, deg) {
	if(deg == 0) {
		return v;
	}
	
	rad = deg * Math.PI / 180;
	
	x = v[0];
	y = v[1];
	var cos = round(Math.cos(rad),10);
	var sin = round(Math.sin(rad),10)
	xn = (x * cos) - (y * sin);
	yn = (x * sin) + (y * cos);
	
	var nv = [xn,yn];
	
	return nv;
}

function round(nr, dec) {
	dec = Math.pow(10, dec);
	return Math.floor(nr*dec)/dec;
}

function vector_from_ang(ang) {
	var rad = ang / 180 * Math.PI;
	var vector = [0,0];
	vector[0] = Math.cos(rad);
	vector[1] = Math.sin(rad);
	
	return vector;
}