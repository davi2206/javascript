const LIVE = false;
const CANVAS_COLOR = 'white';
const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 800;
const TITLE = "Project Title"
const FPS = 250;

// A Ball that we can put on the canvas
const OBJECT_COLOR = 'white';

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"]; //["Script_1.js", "Script_2.js"];
let commonScripts = ["draw.js"];
var scriptsToLoad = 0;

let object_size = 50;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	loadImages();
	registerEvents();
	registerUIEvents();
}

function runSim() {
	setInterval(function() {
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	let n = 0;
	
	n = map(5, 0, 10, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 25, 'black', 15);

	n = map(7, 0, 10, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 50, 'black', 15);
	
	n = map(13, 0, 42, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 75, 'black', 15);
	
	n = map(22, 10, 55, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 100, 'black', 15);
	
	n = map(5, 0, 10, 33, 150);
	colorText(n.toString(), CANVAS_WIDTH/2, 125, 'black', 15);

	n = map(7, 5, 15, 30, 50);
	colorText(n.toString(), CANVAS_WIDTH/2, 150, 'black', 15);

	n = map(0, 1, 10, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 175, 'black', 15);
	
	n = map(-5, -10, 10, 0, 100);
	colorText(n.toString(), CANVAS_WIDTH/2, 200, 'black', 15);
}

function drawAll() {
	
}

function onMouseClick(evt) {
	var mousePos = getMousePos(evt);
}

function runTests() {
	// Have tests here
}