// Class tile
class cTile {
	constructor(col, row, index, type) {
		this.col = col;
		this.row = row;
		this.index = index;
		this.tileType = type;
		this.liveNeighbours = 0;
	}
	
	getNeighbours() {
		// Straight
		var left = tileMap.get(this.index-1);
		if(this.checkLive(left)) {
			this.liveNeighbours++;
		}
		
		var right = tileMap.get(this.index+1);
		if(this.checkLive(right)) {
			this.liveNeighbours++;
		}
		
		var above = tileMap.get(this.index-LEVEL_COLS);
		if(this.checkLive(above)) {
			this.liveNeighbours++;
		}
		
		var below = tileMap.get(this.index+LEVEL_COLS);
		if(this.checkLive(below)) {
			this.liveNeighbours++;
		}
		
		var armpit = false;
		
		// Diagonal
		var leftTop = tileMap.get(this.index-LEVEL_COLS-1);
		if(this.checkLive(leftTop)) {
			this.liveNeighbours++;
		}
		
		var rightTop = tileMap.get(this.index-LEVEL_COLS+1);
		if(this.checkLive(rightTop)) {
			this.liveNeighbours++;
		}
		
		var leftBotom = tileMap.get(this.index+LEVEL_COLS-1);
		if(this.checkLive(leftBotom)) {
			this.liveNeighbours++;
		}
		
		var rightBotom = tileMap.get(this.index+LEVEL_COLS+1);
		if(this.checkLive(rightBotom)) {
			this.liveNeighbours++;
		}
	}
	
	checkLive(node) {
		if(node == undefined) return false;
		if(node.tileType == LIVE) {
			return true;
		} else {
			return false;
		}
	}
	
	updateLife() {
		if(this.tileType == LIVE) {
			if(this.liveNeighbours < 2 || this.liveNeighbours > 3) {
				this.tileType = DEAD;
			}
		} else {
			if(this.liveNeighbours == 3) {
				this.tileType = LIVE;
			}
		}
		this.liveNeighbours = 0;
	}
	
	draw(color, strokeColor) {
		var x = this.col*tileSize;
		var y = this.row*tileSize;
		
		colorRect(x, y, tileSize, tileSize, color, strokeColor);
	}
}

/*
G: Score from start to this node. Increse by 10 for horizontal/vertical moves, increse 14 for diagonal (If included)
H: Score for remaining distanceance in straight line (Ignoring obstacles) Go diagonal until you hit row/column of target, then go straight
F: G + H. This is the score we look at to know which note to check next. Always check lowest. If equal, check lowest H
*/