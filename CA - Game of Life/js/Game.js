var canvas;
var canvasContext;

const FPS = 20;
const LOCATE_MOUSE = false;

var startTime = 0;
var stopTime = 0;
var runTime = 0;
var steps = 0;

var searching = true;

var scripts = [];
let commonScripts = ["draw.js"];

window.onload = function() {
	var baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/GoldFish/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);

	startTime = performance.now();
	setInterval(function() {fpsLoop();}, 1000/FPS);
}

function play() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	
	colorRect(0, 0, canvas.width, canvas.height, "white");
	colorText('This is some sort of loading screen', canvas.width/10, canvas.height/3, 'blue', 80);

	gameReset();
	registerEvents();
}

function gameReset() {
	initLevel();
}

function fpsLoop() {
	updateLives();
	drawLevel();
}

// Support functions
// Other support stuff
function getArrayIndex(col, row) {
	var index = LEVEL_COLS * row + col;
	return index;
}

