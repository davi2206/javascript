// Level parameters
var tileSize;

var levelGrid = [];
var tileMap;
var startNode;
var goalNode;

// Level tile options
const LEVEL_GROUND = 0;
const DEAD = 1;
const LIVE = 2;
const LEVEL_GOAL = 3;

const DIVISOR = 10;
const LEVEL_COLS = (1920/DIVISOR);
const LEVEL_ROWS = (1080/DIVISOR);

const LIVE_PCT = 50;

function initLevel() {
	tileMap = new Map();
	tileSize = canvas.width / LEVEL_COLS;
	levelGrid = [];
	generateLevel();
	drawLevel();
	
	return true;
}

function generateLevel() {
	var arrayIndex = 0;
	var drawTileX = 0;
	var drawTileY = 0;
	
	for(row = 0; row < LEVEL_ROWS; row++) {
		for(col = 0; col < LEVEL_COLS; col++) {
			levelIndex = getArrayIndex(col, row);
			
			var levelType = -1;
			
			var r = Math.floor(Math.random() * 100);
			if(r > LIVE_PCT) {
				levelGrid[levelIndex] = LEVEL_GROUND;
				levelType = DEAD;
			} else {
				levelGrid[levelIndex] = LIVE;
				levelType = LIVE;
			}
			
			tileMap.set(levelIndex, new cTile(col, row, levelIndex, levelType));
			
			arrayIndex++;
			drawTileX += tileSize;
		}
		drawTileX = 0;
		drawTileY += tileSize;
	}
	drawTileY = 0;
}

function drawLevel() {
	for(const [k, v] of tileMap) {
		var color = "pink";
		switch(v.tileType) {
			case LEVEL_GROUND:
				color = "gray";
				break;
			case DEAD:
				color = "white";
				break;
			case LIVE:
				color = "green";
				break;
			default:
				color = "blue";
				break;
		}
		
		v.draw(color, "black");
		v.getNeighbours();
		//colorText("G: " + v.g, v.col*tileSize+2, v.row*tileSize+tileSize/2, 'yellow', 20);
		//colorText("H: " + v.h, v.col*tileSize+2, v.row*tileSize+tileSize/2+15, 'yellow', 20);
	}
}

function updateLives() {
	for(const [k, v] of tileMap) {
		v.updateLife();
	}
}