// Mouse pos
var mouseX;
var mouseY;

var mouseTileX = 0;
var mouseTileY = 0;
var mouseIndex = 0;

// Map arrow keys
const KEY_LEFT_ARROW = 37;
const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;

const KEY_SPACE = 32;
const KEY_ENTER = 13;
const KEY_RESET = 82;

function registerEvents() {
	canvas.addEventListener('mousedown', handleMouseClick);
	canvas.addEventListener('mousemove', function(evt) {var mousePos = getMousePos(evt);});
	document.addEventListener('keydown', keyPressed);
//	document.addEventListener('keyup', keyReleased);
}

// Mouse stuff
function handleMouseClick(evt) {	
	levelGrid[mouseIndex]++;
}

function keyChange(keyEvt, value) {
	switch(keyEvt.keyCode) {
		case KEY_SPACE:
			searching = !searching;
			break;
		case KEY_ENTER:
			break;
		default:
			console.log("Pressed key: "+keyEvt.keyCode);
			break;
	}
}

// Keyboard inputs
function keyPressed(evt) {
	keyChange(evt, true);
}

function keyReleased(evt) {
	keyChange(evt, false);
}

function findMouseTile() {
	mouseTileX = Math.floor(mouseX / tileSize);
	mouseTileY = Math.floor(mouseY / tileSize);
	mouseIndex = getArrayIndex(mouseTileX, mouseTileY);
}