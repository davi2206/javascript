// A* implementation, I hope

const STEP_VALUE = 10; // Value to add pr step
const STEP_VALUE_DIAG = 14; // Value to add pr step

var openSet = []; // Known nodes to be evaluated
var closedSet = []; // Known nodes already evaluated
var startNode;
var currentNode;


function initSim() {
	startNode = tileMap.get(startTileIndex);

	startNode.isOpen = false;
	startNode.isClosed = true;
	openSet = [];
	closedSet = [];
	
	openSet.push(startNode);
}

function getCurent() {
	currentNode = null;
	var smallF = 9000000;
	
	for(var i=0; i < openSet.length; i++) {
		var node = openSet[i];
		if(node.f < smallF) {
			smallF = node.f;
			currentNode = node;
		}
	}
	
	if(currentNode != null) {
		closedSet.push(currentNode);
		removeFromSet(currentNode, openSet);
		currentNode.isopen = false;
		currentNode.isClosed = true;
	} else {
		console.log("currentNode: "+currentNode);
	}
}

/*
G: Score from start to this node. Increse by 10 for horizontal/vertical moves, increse 14 for diagonal (If included)
H: Score for remaining distanceance in straight line (Ignoring obstacles) Go diagonal until you hit row/column of target, then go straight
F: G + H. This is the score we look at to know which note to check next. Always check lowest. If equal, check lowest H
*/