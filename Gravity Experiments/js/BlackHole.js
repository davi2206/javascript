function cBlackHole(x, y, r, gravity = 1, direction = [0,0], sun) {
	this.x = x;
	this.y = y;
	this.r = r; // Radius of the object (m)
	this.gravity = gravity * this.r; // Gravity force of the object, based on the size and gravitational properties of the object
	this.direction = direction; // Vector
	this.sun = sun;
	
	this.draw = function() {
		let diam = this.r*2;
		drawImgCentRot(this.sun, this.x, this.y, 0, diam, diam);
	}
}