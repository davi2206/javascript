// Map arrow keys
const KEY_LEFT_ARROW = 37;
const KEY_UP_ARROW = 38;
const KEY_RIGHT_ARROW = 39;
const KEY_DOWN_ARROW = 40;

const KEY_SPACE = 32;
const KEY_ENTER = 13;
const KEY_RESET = 82;

let mouseX = 0;
let mouseY = 0;

function registerEvents() {
	canvas.addEventListener('mousedown', handleMouseClick);
	canvas.addEventListener('mousemove', function(evt) {var mousePos = getMousePos(evt);});
	document.addEventListener('keydown', keyPressed);
//	document.addEventListener('keyup', keyReleased);
}

// Mouse stuff
function handleMouseClick(evt) {	
	onMouseClick(evt);
}

// Keyboard inputs
function keyChange(keyEvt, value) {
	switch(keyEvt.keyCode) {
		case KEY_SPACE:
			onSpaceClick()
			break;
		case KEY_ENTER:
			break;
		default:
			break;
	}
}

function keyPressed(evt) {
	keyChange(evt, true);
}

function keyReleased(evt) {
	keyChange(evt, false);
}