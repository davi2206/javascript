let slider = document.getElementById("size_slider");
let object_button = document.getElementById("object_button");
let planet_x = document.getElementById("planet_x");
let planet_y = document.getElementById("planet_y");
let reset_button = document.getElementById("reset_button");
let pause_button = document.getElementById("pause_button");

function registerUIEvents() {
	object_button.onclick = function() {
		onSpaceClick();
	}
	
	reset_button.onclick = function() { 
		reset_values();
	}
	
	pause_button.onclick = function() { 
		if(paused) {
			pause_button.value = "Pause Simulation";
		} else {
			pause_button.value = "Start Simulation";
			
		}
		paused = !paused;
	}
}