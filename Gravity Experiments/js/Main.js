const LIVE = false;
const CANVAS_COLOR = "rgba(0, 0, 0, 1)";
const TITLE = "Gravity"
const FPS = 120;
const GRAVITY = [0,0.196]; // Earth gravity is 9.8 m/s^2
const OBJECT_SIZE = 25;
const MASS_MODIFIER = 25;

// A Ball that we can put on the canvas
const PLANET_COLOR = 'rgba(0,195,255,0.6)';
const BLACK_HOLE_COLOR = 'rgba(255,255,0,0.6)';

let scripts = [
	"ImageLoader.js", 
	"Ball.js", 
	"Inputs.js", 
	"BlackHole.js",
	"UI.js"];
let commonScripts = ["draw.js", "VectorMath.js", "Vector.js"];
	
let scriptsToLoad = 0;
this.balls = [];
this.black_holes = [];
this.object_color = '';
let canvas_W;
let canvas_H;
let worker;
let paused = false;

window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	// Common Load Scripts
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/Gravity/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	canvas_W = (window.innerWidth*0.8);
	canvas_H = (window.innerHeight*0.7);
	loadCanvas(canvas_W, canvas_H, CANVAS_COLOR, showError = false);
	
	reset_values();
	registerEvents();
	registerUIEvents();
	loadImages();
}

function reset_values() {
	slider.value = OBJECT_SIZE;
	planet_x.value = 1;
	planet_y.value = 1;
}

function runSim() {
	worker = setInterval(function() {
		if(!paused) {
			moveAll();
			drawAll();
		}
	}, 1000/FPS);
}

function moveAll() {
	this.balls.forEach(ball => update_ball(ball, black_holes));
}

function drawAll() {
	// Canvas
	//colorRect(0,0,canvas.width,canvas.height, CANVAS_COLOR);
	drawImgCentRot(sky, canvas_W/2, canvas_H/2, 0, canvas_W, canvas_H);
	this.black_holes.forEach(hole => hole.draw());
	this.balls.forEach(ball => ball.draw());
	
	if(this.mouseCreatePlanet) {
		this.object_color = PLANET_COLOR;
	} else {
		this.object_color = BLACK_HOLE_COLOR;
	}
	
	colorCircle(mouseX, mouseY, slider.value, this.object_color);
}

function update_ball(ball, black_holes) {
	ball.move(this.black_holes);
	if(ball.destroy) {
		let index = this.balls.indexOf(ball);
		if (index > -1) { // only splice array when item is found
			this.balls.splice(index, 1); // 2nd parameter means remove one item only
		}
	}
}

function onMouseClick(evt) {
	let vector;
	let obj;
	
	let mousePos = getMousePos(evt);
	let radius = parseInt(slider.value);
	if(this.mouseCreatePlanet) {
		vector = [parseInt(planet_x.value), -parseInt(planet_y.value)];
		let rotation = random(0,360);
		let newplanet = getRandplanet();
		let mass = radius / MASS_MODIFIER;
		obj = new cBall(mousePos.x, mousePos.y, radius, vector, PLANET_COLOR, rotation, mass, newplanet);
		this.balls.push(obj);
	} else {
		this.black_holes.push(new cBlackHole(mousePos.x, mousePos.y, radius, radius/25, [0,0], sun1));
	}
}

function onSpaceClick() {
	this.mouseCreatePlanet = !this.mouseCreatePlanet;
	
	if(this.mouseCreatePlanet) {
		object_button.value = "Placing Planet";
	} else {
		object_button.value = "Placing Sun";
	}
}

function getRandplanet() {
	let randImg = random_floor(1,7);
	
	switch(randImg) {
		case 1:
			return planet1;
			break;
		case 2:
			return planet2;
			break;
		case 3:
			return planet3;
			break;
		case 4:
			return planet4;
			break;
		case 5:
			return planet5;
			break;
		case 6:
			return planet6;
			break;
	}
}

function runTests() {
	// Have tests here
}