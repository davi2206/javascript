function cBall(x, y, r, direction = [0,0], color = OBJECT_COLOR, rotation, mass = 1, planet) {
	this.speed = 0; // Magnitude of directional vector
	this.r = r; // Radius of the object (m)
	this.x = x;
	this.y  = y;
	this.rotation = rotation;
	this.color = color;
	this.mass = mass; // Mass of the object (kg)
	this.direction  = direction; // Is a Vector
	this.planet = planet;
	
	// Should the planet be destroyed?
	this.destroy = false;
	
	this.move = function(black_holes = []) {
		// Acceleration = Vector from object to black hole
		// Set magnitude of Acceleration to # or cap it at #
		// Add acceleration to velocity (this.direction)
		// Add velocity to position
		
		black_holes.forEach(hole => this.add_black_hole(hole));
		black_holes.forEach(hole => this.crash_black_hole(hole));
		
		this.x += this.direction[0];
		this.y += this.direction[1];
		
		// Wrap around the map
		// this.wrapCoord()
	}
	
	this.add_black_hole = function(hole) {
		let black_vector = this.get_black_vector(hole); // Acceleration
		let distance = vector_magnitude(black_vector);
		let force = ((this.mass * hole.gravity) / (distance * distance));
		black_vector = vector_mult(black_vector, force); // Ajusted magnitude
				
		this.direction = vector_add(this.direction, black_vector);
	}
	
	// Destroy the ball, if it crashes into a black hole
	this.crash_black_hole = function(hole) {
		let vector = this.get_black_vector(hole);
		let dist = vector_magnitude(vector);
		
		let min_dist = (hole.r + this.r);
		
		if(dist < min_dist) {
			this.destroy = true;
		}
	}
	
	this.get_black_vector = function(hole) {
		var xDiff = hole.x-this.x;
		var yDiff = hole.y-this.y;
		return [xDiff, yDiff];
	}
	
	this.draw = function() {
		// Common colorCircle(...)
		let diam = this.r*2;
		
		drawImgCentRot(this.planet, this.x, this.y, this.rotation, diam, diam);
		//colorCircle(this.x, this.y, this.r, this.color);
	}
}
