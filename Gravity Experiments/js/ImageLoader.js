// Create elements for images
let sky = document.createElement("img");
let sun1 = document.createElement("img");
let sun2 = document.createElement("img");
let planet1 = document.createElement("img");
let planet2 = document.createElement("img");
let planet3 = document.createElement("img");
let planet4 = document.createElement("img");
let planet5 = document.createElement("img");
let planet6 = document.createElement("img");

let suns = [];
let planets = [];

let picsToLoad = 0;

function loadImages() {
	const imageMap = new Map();
	//imageMap.set(sky, "sky_one.jpg");
	imageMap.set(sky, "sky_two.jpg");
	
	imageMap.set(sun1, "sun1.png");
	imageMap.set(sun2, "sun2.png");
	
	imageMap.set(planet1, "planet (1).png");
	imageMap.set(planet2, "planet (2).png");
	imageMap.set(planet3, "planet (3).png");
	imageMap.set(planet4, "planet (4).png");
	imageMap.set(planet5, "planet (5).png");
	imageMap.set(planet6, "planet (6).png");
	
	picsToLoad = imageMap.size;
	
	for(var [k,v] of imageMap) {
		beginLoadImg(k, v);
	}
}

function beginLoadImg(imgVar, fileName) {
	imgVar.onload = function() {
		picsToLoad--;
		
		if(picsToLoad == 0) {
			runSim();
		}
	}
	
	var baseDir = 'graphics/';
	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/custom/Gravity/graphics/';
	}
	imgVar.src = baseDir+fileName;
}