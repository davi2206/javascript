const LIVE = false;
const CANVAS_COLOR = 'blue';
const TITLE = "Project Title"
const FPS = 500;
let CANVAS_WIDTH = 2574;
let CANVAS_HEIGHT = 2803;

let scripts = ["ImageLoader.js", "Inputs.js", "UI.js"];
let commonScripts = ["draw.js", "Point.js", "Vector.js"];
var scriptsToLoad = 0;

let point;
let vector;
let start_x = 396;
let start_y = 535;
let start_vector_x = 50;
let start_vector_y = 0;

let turn_angle = 0;
let ang_change = 0.15;


// Debug information
let smallX = CANVAS_WIDTH;
let bigX = 0;
let smallY = CANVAS_HEIGHT;
let bigY = 0;


window.onload = function() {
	window.addEventListener("keydown", function(e) {
		// Prevent window from scroling on arrow key press
		if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
			e.preventDefault();
		}
	}, false);
	
	let baseDir = 'js/';
	let commonBaseDir = '../_common_resources/';

	if(LIVE) {
		baseDir = 'https://theslowloris.com/wp-includes/js/TSL_Custom/__Template/js/';
	}
	loadScripts(scripts, baseDir, commonScripts, commonBaseDir);
	
	titleH1 = document.getElementById('gameTitle');
	titleH1.innerHTML = TITLE;
}

// Automatically called when all scripts are loaded
function play() {
	// Common Load Canvas
	loadCanvas(CANVAS_WIDTH, CANVAS_HEIGHT, color = CANVAS_COLOR, showError = true);

	// loadImages();
	registerEvents();
	registerUIEvents();

	vector = new Vector(start_vector_x, start_vector_y);

	let magnitude = vector.get_magnitude();
	console.log(magnitude);

	point = new Point(start_x,start_y);

	runSim();
}

function runSim() {
	setInterval(function() {
		if(turn_angle > 360) return;
		moveAll();
		drawAll();
	}, 1000/FPS);
}

function moveAll() {
	point.move_vector(vector);
	vector.turn_deg(180-turn_angle);
	turn_angle += ang_change;

	if(point.x > bigX) bigX = point.x;
	if(point.x < smallX) smallX = point.x;
	if(point.y > bigY) bigY = point.y;
	if(point.y < smallY) smallY = point.y;
}

function drawAll() {
	let color = getRandColor();

	vector.draw(point, color, 2);
}
