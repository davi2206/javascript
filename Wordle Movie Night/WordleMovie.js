/*
TO IMPLEMENT:
- What if this was all JS, and not HTML elements? You could draw everything nicely on the canvas, rather than fight the CSS fight...
- Stats with wins, looses and win-loose ratio
- Better Win and Lose screens, with the stats
- Count down till next word
- Cookie to see that player already played today (List of word nrs)

- Share button (Yikes)
*/

var canvas;
var canvasContext;

// Brick parameters
const BRICK_SPACING_PCT = 2;
const BRICK_COLS = 5;
const BRICK_ROWS = 6;
const cWrong = '#c8d0ff'; //'Silver';
const cClose = 'Gold';
const cRight = 'LawnGreen';
const cPassive = 'Aliceblue';

var brickHeight = 20;
var brickSize = 20;

var brickGap = 1;
var brickIndex = 0;

let wordList = ["TWINS","GLORY","PRIME","SIGNS","KOLYA","MARTY","SLEEP","ZELIG","BAMBI","FRIDA","RADIO","FOCUS","LAURA","MAMMY","ROCKY","ALIEN","TOPAZ","RONIN","VIRUS","BRUNO","TOMMY","ANGEL","TAMMY","STONE","CRANK","DOGMA","HOTEL","SHAFT","FRAUD","MANDY","OTLEY","YANKS","WINGS","EVITA","DUETS","AMOUR","SCOOP","SHANE","ANNIE","SPEED","HEIST","MULAN","PINKY","FARGO","SHREK","AKIRA","HOFFA","ALFIE","DRIVE","SALLY","DUMBO","KIPPS","DOUBT","BABEL","ARIEL","GIGLI","KITTY","HONDO","SPAWN","ALIVE","AWAKE","YENTL","FANNY","HOLES","SHINE","PONYO","GILDA","GREED","ANZIO","CLICK","HONEY","HEIDI","GHOST","BLADE","JOKER","NORTH","SEVEN","JULIA","LENNY","GIANT","COBRA","ELCID","LUCAS","LADYL","EARTH","BENJI","NIXON","EQUUS","BUGSY","RAMBO","KOTCH","PROOF","BOBBY","GYPSY","ABYSS","WALLE","OUIJA","VENOM","TAKEN","SELMA","RANGO","CRASH","HANNA","TENET","HITCH","DIGBY","TURBO","BRAVE","KLUTE"];

let buffer = [];
let tiles = [];
let curGuess = 0;
let word = '';
let gameOver = false;

document.addEventListener('keydown', (event) => {
	if(gameOver) return;
	
	const charList = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	const key = event.key.toUpperCase();
	
	// We are only interested in alphanumeric keys
	if (key == "BACKSPACE" && buffer.length > 0) {
		buffer.pop();
		ind = getArrayIndex(buffer.length, curGuess);
		tiles[ind].draw();
		setLetter();
		return;
	} 
	
	if(key == "ENTER") {
		if(buffer.length < 5) {
			// Word to Short!
			var message = "You need to fill all five letters. Count again";
			let color = "red";
			updateHtmlMessage(message, color);
			return;
		}
		
		checkWord(buffer);
		
		return;
	}

	// Reset!
	var message = "[]";
	let color = "black";
	updateHtmlMessage(message, color);
	
	if (charList.indexOf(key) === -1) return;
	if (buffer.length == 5) {
		return;
	}
	else {
		buffer.push(key.toUpperCase());
		setLetter();
	}
}, false);

function setLetter() {
	for(col = 0; col < BRICK_COLS; col++) {
		brickIndex = getArrayIndex(col, curGuess);
		
		var x = (brickSize*col);
		var y = (brickHeight*curGuess);
		var w = brickSize-(brickGap/2);
		var h = w;
		
		if(buffer[col]) {
			colorText(buffer[col], x+(brickSize/2), y+(brickHeight/10*8), 'black', 65);
		}
	}
}

function checkWord(guess) {
	let guessWord = guess.join('');
	
	if(!wordList.includes(guessWord)) {
		// Refuse word!
		var message = "That is not a real movie though...";
		let color = "red";
		updateHtmlMessage(message, color);
		return;
	}
	
	let bgColor = "";
	let color = "black";
	
	for (let i = 0; i < guess.length; i++) {
		index = getArrayIndex(i, curGuess);
		if(guess[i] == word[i]) {
			tiles[index].state = "right";
			tiles[index].draw();
			bgColor = cRight;
			setLetter();
		} else if (word.includes(guess[i])) {
			tiles[index].state = "close";
			tiles[index].draw();
			bgColor = cClose;
			setLetter();
		} else {
			tiles[index].state = "wrong";
			tiles[index].draw();
			bgColor = cWrong;
			setLetter();
		}
		
		let key = document.getElementById(guess[i]);
		key.style.backgroundColor = bgColor;
		key.style.color = color;
	}
	curGuess += 1;
	buffer = [];
	
	if(guessWord.toUpperCase() == word.toUpperCase()) {
		var game = document.getElementById('game');
		var winner = document.getElementById('winner');
		game.style.display = "none";
		winner.style.display = "block";
		gameOver = true;
		return;
	}
	
	if(curGuess > 5) {
		// FAIL
		var game = document.getElementById('game');
		var looser = document.getElementById('looser');
		game.style.display = "none";
		looser.style.display = "block";
		gameOver = true;
	}
}

window.onload = function() {
	canvas = document.getElementById('gameCanvas');
	canvasContext = canvas.getContext('2d');
	pickWord();
	drawAll();
}

function pickWord() {
	// Based on the curent date, pick a word
	let d1 = new Date("01/01/2022 12:00:00");
	let d2 = new Date();
	
	var days = Math.round((d2.getTime() - d1.getTime())/ (1000*3600*24));
	while(days > (wordList.length-1)) {
		days = days - wordList.length;
	}
	
	word = wordList[days];
}

function drawAll() {
	// Canvas
	colorRect(0,0,canvas.width,canvas.height,'white');
	
	brickSize = canvas.width / BRICK_COLS - BRICK_SPACING_PCT;
	brickHeight = canvas.height / BRICK_ROWS;
	brickGap = brickSize / 100 * BRICK_SPACING_PCT;
	
	for(row = 0; row < BRICK_ROWS; row++) {
		for(col = 0; col < BRICK_COLS; col++) {
			brickIndex = getArrayIndex(col, row);
			
			var x = (brickSize*col) + (brickSize/2);
			var y = (brickHeight*row) + (brickHeight/2);
			var w = brickSize - brickGap;
			var h = w;
			
			tiles[brickIndex] = new Tile(row, col, x, y, w, h, 'passive');
			tiles[brickIndex].draw();
		}
	}
}

function updateHtmlMessage(message, color) {
	document.getElementById("gameMessage").innerHTML = message;
	document.getElementById("gameMessage").style.color = color;
}

function keyboardClick() {
	let key = event.srcElement.id;
	document.dispatchEvent(new KeyboardEvent('keydown', {'key': key}));
}

// Support functions
function colorRect(x, y, width, height, color) {
	canvasContext.fillStyle = color;
	canvasContext.fillRect(x, y, width, height);
}

function colorRectCent(x, y, width, height, color) {
	canvasContext.fillStyle = color;
	var nx = x - (width/2);
	var ny = y - (height/2);
	canvasContext.fillRect(nx, ny, width, height);
}

function colorText(words, posX, posY, color, size) {
	canvasContext.font = 'normal '+size+'px Arial';
	canvasContext.fillStyle = color;
	canvasContext.textAlign = 'center';
	canvasContext.fillText(words, posX, posY);
}

function getMousePos(evt) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	mouseX = evt.clientX - rect.left - root.scrollLeft;
	mouseY = evt.clientY - rect.top - root.scrollTop;
	
	return {
		x:mouseX,
		y:mouseY
	};
}

function getArrayIndex(col, row) {
	var index = BRICK_COLS * row + col;
	return index;
}



class Tile {
	constructor(row, col, x, y, w, h, state) {
		this.row = row;
		this.col = col;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.state = state;
	};
	
	draw() {
		// Reset first
		colorRectCent(this.x, this.y, this.w, this.h, 'white');
		
		let color = '';
		this.state = this.state.toLowerCase();
		if(this.state == 'wrong') {
			color = cWrong;
		} else if(this.state == 'close') {
			color = cClose;
		} else if(this.state == 'right') {
			color = cRight;
		} else if(this.state == 'passive') {
			color = cPassive;
		}
		
		colorRectCent(this.x, this.y, this.w, this.h, color);
	}
}